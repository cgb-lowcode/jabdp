/*
 * @(#)Rule.java
 * 2012-11-15 下午08:56:10
 * 
 * Copyright (c) 2018-2028, HangZhou QiYun InfoTech Co.,Ltd. .
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.qyxx.jwp.bean;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;


/**
 *  规则
 *  @author gxj
 *  @version 1.0 2012-11-15 下午08:56:10
 *  @since jdk1.6 
 */
@XmlAccessorType(XmlAccessType.FIELD)
public class Rule {
	/**
	 * 规则类型--sql语句
	 */
	public static final String TYPE_SQL = "sql";
	
	/**
	 * 规则类型--存储过程
	 */
	public static final String TYPE_PROC = "proc";
	
	@XmlAttribute
	private String caption;
	@XmlAttribute
	private String key;
	@XmlAttribute
	private String type;
	
	@XmlElement
	private Sql sql;

	
	/**
	 * @return caption
	 */
	public String getCaption() {
		return caption;
	}

	
	/**
	 * @param caption
	 */
	public void setCaption(String caption) {
		this.caption = caption;
	}

	
	/**
	 * @return key
	 */
	public String getKey() {
		return key;
	}

	
	/**
	 * @param key
	 */
	public void setKey(String key) {
		this.key = key;
	}

	
	/**
	 * @return type
	 */
	public String getType() {
		return type;
	}

	
	/**
	 * @param type
	 */
	public void setType(String type) {
		this.type = type;
	}

	
	/**
	 * @return sql
	 */
	public Sql getSql() {
		return sql;
	}

	
	/**
	 * @param sql
	 */
	public void setSql(Sql sql) {
		this.sql = sql;
	}
	
	

}
