/*
 * @(#)Button.java
 * 2012-11-6 上午11:18:07
 * 
 * Copyright (c) 2018-2028, HangZhou QiYun InfoTech Co.,Ltd. .
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.qyxx.jwp.bean;

import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlTransient;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;


/**
 *  自定义按钮
 *  @author gxj
 *  @version 1.0 2012-11-6 上午11:18:07
 *  @since jdk1.6 
 */
@XmlAccessorType(XmlAccessType.FIELD)
public class Button {
	//按钮类型--普通，即单个按钮
	public static final String BUTTON_TYPE_NORMAL = "normal";

	//按钮类型--菜单，即下拉式按钮，有多个
	public static final String BUTTON_TYPE_MENU = "menu";
	
	//在哪个页面--查询页面、列表页面
	public static final String IN_QUERY_PAGE = "InQueryPage";
	
	//在哪个页面--查看页面、详情页面
	public static final String IN_VIEW_PAGE = "InViewPage";
	
	@XmlAttribute
	private String caption;
	@XmlAttribute
	private String key;// 按钮key
	@XmlAttribute
    private String languageText;
	@XmlAttribute
	private String iconCls;// 图标
	
	@XmlAttribute
	private Integer width;//控件宽度
	@XmlAttribute
	private Integer height;//控件长度
	
	@XmlElement(name="event")
	private List<Event> eventList = new ArrayList<Event>();
	
	@JsonIgnoreProperties
	@XmlTransient
	private String i18nKey;
	
	@XmlAttribute
	private String buttonType; //按钮类型
	
	@XmlElementWrapper(name = "buttons")
	@XmlElement(name="button")
	private List<Button> buttonList = new ArrayList<Button>();
	
	/**
	 * 开启权限检查
	 */
	@XmlAttribute
	private Boolean enableAuthCheck = false; 

	/**
	 * 表单标题
	 */
	@JsonIgnoreProperties
	@XmlTransient
	private String formCaption;
	
	/**
	 * 表单实体名
	 */
	@JsonIgnoreProperties
	@XmlTransient
	private String formEntityName;
	
	/**
	 * 表单Key
	 */
	@JsonIgnoreProperties
	@XmlTransient
	private String formKey;
	
	/**
	 * 在什么页面
	 */
	@JsonIgnoreProperties
	@XmlTransient
	private String inWhichPage;
	
	/**
	 * @return caption
	 */
	public String getCaption() {
		return caption;
	}

	
	/**
	 * @param caption
	 */
	public void setCaption(String caption) {
		this.caption = caption;
	}

	
	/**
	 * @return key
	 */
	public String getKey() {
		return key;
	}

	
	/**
	 * @param key
	 */
	public void setKey(String key) {
		this.key = key;
	}

	
	/**
	 * @return languageText
	 */
	public String getLanguageText() {
		return languageText;
	}

	
	/**
	 * @param languageText
	 */
	public void setLanguageText(String languageText) {
		this.languageText = languageText;
	}

	
	/**
	 * @return width
	 */
	public Integer getWidth() {
		return width;
	}

	
	/**
	 * @param width
	 */
	public void setWidth(Integer width) {
		this.width = width;
	}

	
	/**
	 * @return height
	 */
	public Integer getHeight() {
		return height;
	}

	
	/**
	 * @param height
	 */
	public void setHeight(Integer height) {
		this.height = height;
	}

	
	/**
	 * @return eventList
	 */
	public List<Event> getEventList() {
		return eventList;
	}


	
	/**
	 * @param eventList
	 */
	public void setEventList(List<Event> eventList) {
		this.eventList = eventList;
	}


	/**
	 * @return i18nKey
	 */
	public String getI18nKey() {
		return i18nKey;
	}

	
	/**
	 * @param i18nKey
	 */
	public void setI18nKey(String i18nKey) {
		this.i18nKey = i18nKey;
	}


	
	/**
	 * @return iconCls
	 */
	public String getIconCls() {
		return iconCls;
	}


	
	/**
	 * @param iconCls
	 */
	public void setIconCls(String iconCls) {
		this.iconCls = iconCls;
	}


	/**
	 * @return buttonType
	 */
	public String getButtonType() {
		return buttonType;
	}


	
	/**
	 * @param buttonType
	 */
	public void setButtonType(String buttonType) {
		this.buttonType = buttonType;
	}


	/**
	 * @return buttonList
	 */
	public List<Button> getButtonList() {
		return buttonList;
	}


	
	/**
	 * @param buttonList
	 */
	public void setButtonList(List<Button> buttonList) {
		this.buttonList = buttonList;
	}


	
	/**
	 * @return enableAuthCheck
	 */
	public Boolean getEnableAuthCheck() {
		return enableAuthCheck;
	}


	
	/**
	 * @param enableAuthCheck
	 */
	public void setEnableAuthCheck(Boolean enableAuthCheck) {
		this.enableAuthCheck = enableAuthCheck;
	}


	
	/**
	 * @return formCaption
	 */
	public String getFormCaption() {
		return formCaption;
	}


	
	/**
	 * @param formCaption
	 */
	public void setFormCaption(String formCaption) {
		this.formCaption = formCaption;
	}


	
	/**
	 * @return formEntityName
	 */
	public String getFormEntityName() {
		return formEntityName;
	}


	
	/**
	 * @param formEntityName
	 */
	public void setFormEntityName(String formEntityName) {
		this.formEntityName = formEntityName;
	}


	
	/**
	 * @return formKey
	 */
	public String getFormKey() {
		return formKey;
	}


	
	/**
	 * @param formKey
	 */
	public void setFormKey(String formKey) {
		this.formKey = formKey;
	}


	
	/**
	 * @return inWhichPage
	 */
	public String getInWhichPage() {
		return inWhichPage;
	}


	
	/**
	 * @param inWhichPage
	 */
	public void setInWhichPage(String inWhichPage) {
		this.inWhichPage = inWhichPage;
	}

	

}
