/*
 * @(#)DataProperties.java
 * 2012-5-8 下午06:04:50
 * 
 * Copyright (c) 2018-2028, HangZhou QiYun InfoTech Co.,Ltd. .
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.qyxx.jwp.bean;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;

/**
 *  字段数据属性
 *  
 *  @author gxj
 *  @version 1.0 2012-5-8 下午06:04:50
 *  @since jdk1.6
 */
@XmlAccessorType(XmlAccessType.FIELD)
public class DataProperties {
	
	public static final String DATA_TYPE_STRING = "dtString";
	
	public static final String DATA_TYPE_LONG = "dtLong";
	
	public static final String DATA_TYPE_INT = "dtInteger";
	
	public static final String DATA_TYPE_SHORT = "dtShort";
	
	public static final String DATA_TYPE_DOUBLE = "dtDouble";
	
	public static final String DATA_TYPE_FLOAT = "dtFloat";
	
	public static final String DATA_TYPE_DATETIME = "dtDateTime";
	
	public static final String DATA_TYPE_MEMO = "dtText";
	
	public static final String DATA_TYPE_IMAGE = "dtImage";

	@XmlAttribute
	private String dataType;
	@XmlAttribute
	private Integer size;
	@XmlAttribute
	private Integer scale;
	@XmlAttribute
	private String key;
	@XmlAttribute
	private String colName;
	@XmlAttribute
	private Boolean notNull;
	@XmlAttribute
	private Boolean unique;

	
	/**
	 * @return dataType
	 */
	public String getDataType() {
		return dataType;
	}
	
	/**
	 * @param dataType
	 */
	public void setDataType(String dataType) {
		this.dataType = dataType;
	}
	
	/**
	 * @return size
	 */
	public Integer getSize() {
		return size;
	}
	
	/**
	 * @param size
	 */
	public void setSize(Integer size) {
		this.size = size;
	}
	
	/**
	 * @return scale
	 */
	public Integer getScale() {
		return scale;
	}
	
	/**
	 * @param scale
	 */
	public void setScale(Integer scale) {
		this.scale = scale;
	}
	
	/**
	 * @return key
	 */
	public String getKey() {
		return key;
	}
	
	/**
	 * @param key
	 */
	public void setKey(String key) {
		this.key = key;
	}
	
	
	/**
	 * @return colName
	 */
	public String getColName() {
		return colName;
	}

	
	/**
	 * @param colName
	 */
	public void setColName(String colName) {
		this.colName = colName;
	}

	/**
	 * @return notNull
	 */
	public Boolean getNotNull() {
		return notNull;
	}
	
	/**
	 * @param notNull
	 */
	public void setNotNull(Boolean notNull) {
		this.notNull = notNull;
	}
	
	/**
	 * @return unique
	 */
	public Boolean getUnique() {
		return unique;
	}
	
	/**
	 * @param unique
	 */
	public void setUnique(Boolean unique) {
		this.unique = unique;
	}

}
