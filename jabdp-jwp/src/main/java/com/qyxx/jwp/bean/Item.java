/*
 * @(#)Item.java
 * 2012-5-9 下午02:38:14
 * 
 * Copyright (c) 2018-2028, HangZhou QiYun InfoTech Co.,Ltd. .
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.qyxx.jwp.bean;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;

/**
 *  item属性
 *  
 *  @author gxj
 *  @version 1.0 2012-5-9 下午02:38:14
 *  @since jdk1.6
 */
@XmlAccessorType(XmlAccessType.FIELD)
public class Item {
	//操作符
	public static final String OPERATOR_EQ = "EQ";
	public static final String OPERATOR_NE = "NE";
	public static final String OPERATOR_LT = "LT";
	public static final String OPERATOR_LE = "LE";
	public static final String OPERATOR_GT = "GT";
	public static final String OPERATOR_GE = "GE";
	public static final String OPERATOR_IN = "IN";
	public static final String OPERATOR_LIKE = "LIKE";
	public static final String OPERATOR_LIKEL = "LIKEL";
	public static final String OPERATOR_LIKER = "LIKER";
	public static final String OPERATOR_ISNULL = "ISNULL";
	public static final String OPERATOR_ISNOTNULL = "ISNOTNULL";
	public static final String OPERATOR_FULLTEXT = "FULLTEXT";
	public static final String OPERATOR_NONE = "NONE";
	
	public static final String COLUMN_TYPE_STRING = "string";//字符串
	public static final String COLUMN_TYPE_LONG = "long";//整数
	

	@XmlAttribute
	private String key;
	@XmlAttribute
	private String caption;
	@XmlAttribute
	private String column;
	@XmlAttribute
	private String order;
	@XmlAttribute
	private String operator;
	@XmlAttribute
	private String value;
	@XmlAttribute
	private String width;
	@XmlAttribute
	private String columntype;
	
	public String getWidth() {
		return width;
	}

	public void setWidth(String width) {
		this.width = width;
	}

	public String getColumntype() {
		return columntype;
	}

	public void setColumntype(String columntype) {
		this.columntype = columntype;
	}

	/**
	 * @return key
	 */
	public String getKey() {
		return key;
	}
	
	/**
	 * @param key
	 */
	public void setKey(String key) {
		this.key = key;
	}
	
	/**
	 * @return caption
	 */
	public String getCaption() {
		return caption;
	}
	
	/**
	 * @param caption
	 */
	public void setCaption(String caption) {
		this.caption = caption;
	}
	
	/**
	 * @return column
	 */
	public String getColumn() {
		return column;
	}
	
	/**
	 * @param column
	 */
	public void setColumn(String column) {
		this.column = column;
	}

	/**
	 * @return the order
	 */
	public String getOrder() {
		return order;
	}

	/**
	 * @param order the order to set
	 */
	public void setOrder(String order) {
		this.order = order;
	}

	
	/**
	 * @return operator
	 */
	public String getOperator() {
		return operator;
	}

	
	/**
	 * @param operator
	 */
	public void setOperator(String operator) {
		this.operator = operator;
	}

	/**
	 * @return value
	 */
	public String getValue() {
		return value;
	}

	
	/**
	 * @param value
	 */
	public void setValue(String value) {
		this.value = value;
	}
	
}
