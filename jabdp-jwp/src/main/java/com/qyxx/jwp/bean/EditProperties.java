/*
 * @(#)EditProperties.java
 * 2012-5-8 下午06:05:46
 * 
 * Copyright (c) 2018-2028, HangZhou QiYun InfoTech Co.,Ltd. .
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.qyxx.jwp.bean;

import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;

/**
 *  字段编辑属性
 *  
 *  @author gxj
 *  @version 1.0 2012-5-8 下午06:05:46
 *  @since jdk1.6
 */
@XmlAccessorType(XmlAccessType.FIELD)
public class EditProperties {

	public static final String EDIT_TYPE_TEXT_BOX = "TextBox";
	
	public static final String EDIT_TYPE_RICH_TEXT_BOX = "RichTextBox";
	
	public static final String EDIT_TYPE_COMBO_BOX = "ComboBox";
	
	public static final String EDIT_TYPE_COMBO_TREE = "ComboTree";
	
	public static final String EDIT_TYPE_COMBO_GRID = "ComboGrid";
	
	public static final String EDIT_TYPE_DATE_BOX = "DateBox";
	
	public static final String EDIT_TYPE_RADIO_BOX = "RadioBox";
	
	public static final String EDIT_TYPE_CHECK_BOX = "CheckBox";
	
	public static final String EDIT_TYPE_BUTTON = "Button";
	
	public static final String EDIT_TYPE_HYPER_LINK = "HyperLink";
	
	public static final String EDIT_TYPE_IMAGE_BOX = "ImageBox";
	
	public static final String EDIT_TYPE_BROWSER_BOX = "BrowserBox";
	
	public static final String EDIT_TYPE_DATA_GRID = "DataGrid";
	
	public static final String EDIT_TYPE_TREE_GRID = "TreeGrid";
	
	public static final String EDIT_TYPE_TREE = "Tree";
	
	public static final String EDIT_TYPE_LABEL = "Label";
	
	public static final String EDIT_TYPE_DICT_TREE = "DictTree";
	
	public static final String EDIT_TYPE_COMBO_BOX_SEARCH = "ComboBoxSearch";
	
	public static final String EDIT_TYPE_DIALOGUE_WINDOW = "DialogueWindow";
	
	public static final String EDIT_TYPE_COMBO_RADIO_BOX = "ComboRadioBox";
	
	public static final String EDIT_TYPE_COMBO_CHECK_BOX = "ComboCheckBox";

	public static final String EDIT_TYPE_AREA = "Area";
	
	@XmlAttribute
	private String editType;
	@XmlAttribute
	private Boolean multiple; // 多选
	@XmlAttribute
	private Boolean visible;	
	@XmlAttribute
	private Boolean readOnly;	
	@XmlAttribute
	private String charCase;
	@XmlAttribute
	private String defaultValue;
	@XmlAttribute
	private Boolean isPassword;
	@XmlAttribute
	private String align;
	@XmlAttribute
	private Integer rows;
	@XmlAttribute
	private Integer cols;
	@XmlAttribute
	private Long maxSize;
	@XmlAttribute
	private String dtFormat;
	@XmlElement
	private Formula formula ;//= new Formula();
	@XmlAttribute
	private Integer width;//控件宽度
	@XmlAttribute
	private Integer height;//控件长度
	@XmlAttribute
	private Integer top;//控件位置top值
	@XmlAttribute
	private Integer left;//控件位置left值
	@XmlAttribute
	private Integer layer;//表头层级
	@XmlAttribute
	private Integer rowspan;//表头跨行
	@XmlAttribute
	private Integer colspan;//表头跨列
	
	@XmlElement(name="event")
	private List<Event> eventList = new ArrayList<Event>();
	
	@XmlElement(name="sql")
	private List<Sql> sqlList = new ArrayList<Sql>();
	
	@XmlElement
	private String expCode;//代码表达式
	
	@XmlElement
	private String autoIdRule;//自动编号规则
	
	@XmlAttribute
	private Boolean quickAdd = false;//快速增加功能是否启用
	
	@XmlAttribute
	private String visibleCondition;//字段是否可见条件
	
	@XmlAttribute
	private String fieldSuffix;//字段后缀，用于表示单位等属性

	@XmlAttribute
	private Boolean autoCollapse = false;//树形控件，自动折叠属性
	
	@XmlAttribute
	private Boolean validInView = false;//查看状态可操作，用于控制按钮、链接在查看状态下也可点击操作
	
	@XmlAttribute
	private Boolean autoComplete = false;//自动完成
	
	@XmlAttribute
	private Boolean uniqueFilterMaster = false; //子表唯一性过滤主表记录
	
	@XmlAttribute
	private String minDate; //datebox日期范围开始
	
	@XmlAttribute
	private String maxDate; //datebox日期范围结束
	
	@XmlAttribute
	private Integer limit;//限定行数，用于数据源查询
	
	@XmlAttribute
	private String style; //样式内容
	
	@XmlAttribute
	private String className; //样式名称
	
	public String getMinDate() {
		return minDate;
	}

	public void setMinDate(String minDate) {
		this.minDate = minDate;
	}

	public String getMaxDate() {
		return maxDate;
	}

	public void setMaxDate(String maxDate) {
		this.maxDate = maxDate;
	}

	public Integer getRowspan() {
		return rowspan;
	}

	public void setRowspan(Integer rowspan) {
		this.rowspan = rowspan;
	}

	public Integer getColspan() {
		return colspan;
	}

	public void setColspan(Integer colspan) {
		this.colspan = colspan;
	}
	
	/**
	 * @return editType
	 */
	public String getEditType() {
		return editType;
	}
	
	/**
	 * @param editType
	 */
	public void setEditType(String editType) {
		this.editType = editType;
	}
	
	/**
	 * @return multiple
	 */
	public Boolean getMultiple() {
		return multiple;
	}
	
	/**
	 * @param multiple
	 */
	public void setMultiple(Boolean multiple) {
		this.multiple = multiple;
	}
	
	/**
	 * @return visible
	 */
	public Boolean getVisible() {
		return visible;
	}
	
	/**
	 * @param visible
	 */
	public void setVisible(Boolean visible) {
		this.visible = visible;
	}
	
	/**
	 * @return readOnly
	 */
	public Boolean getReadOnly() {
		return readOnly;
	}
	
	/**
	 * @param readOnly
	 */
	public void setReadOnly(Boolean readOnly) {
		this.readOnly = readOnly;
	}
	
	/**
	 * @return charCase
	 */
	public String getCharCase() {
		return charCase;
	}
	
	/**
	 * @param charCase
	 */
	public void setCharCase(String charCase) {
		this.charCase = charCase;
	}
	
	/**
	 * @return defaultValue
	 */
	public String getDefaultValue() {
		return defaultValue;
	}
	
	/**
	 * @param defaultValue
	 */
	public void setDefaultValue(String defaultValue) {
		this.defaultValue = defaultValue;
	}
	
	/**
	 * @return isPassword
	 */
	public Boolean getIsPassword() {
		return isPassword;
	}
	
	/**
	 * @param isPassword
	 */
	public void setIsPassword(Boolean isPassword) {
		this.isPassword = isPassword;
	}
	
	/**
	 * @return align
	 */
	public String getAlign() {
		return align;
	}
	
	/**
	 * @param align
	 */
	public void setAlign(String align) {
		this.align = align;
	}
	
	/**
	 * @return rows
	 */
	public Integer getRows() {
		return rows;
	}
	
	/**
	 * @param rows
	 */
	public void setRows(Integer rows) {
		this.rows = rows;
	}
	
	/**
	 * @return cols
	 */
	public Integer getCols() {
		return cols;
	}
	
	/**
	 * @param cols
	 */
	public void setCols(Integer cols) {
		this.cols = cols;
	}
	
	/**
	 * @param layer
	 */
	public Integer getLayer() {
		return layer;
	}

	/**
	 * @param layer
	 */
	public void setLayer(Integer layer) {
		this.layer = layer;
	}
	
	/**
	 * @return maxSize
	 */
	public Long getMaxSize() {
		return maxSize;
	}
	
	/**
	 * @param maxSize
	 */
	public void setMaxSize(Long maxSize) {
		this.maxSize = maxSize;
	}
	
	/**
	 * @return the dtFormat
	 */
	public String getDtFormat() {
		return dtFormat;
	}

	/**
	 * @param dtFormat the dtFormat to set
	 */
	public void setDtFormat(String dtFormat) {
		this.dtFormat = dtFormat;
	}

	/**
	 * @return formula
	 */
	public Formula getFormula() {
		return formula;
	}
	
	/**
	 * @param formula
	 */
	public void setFormula(Formula formula) {
		this.formula = formula;
	}

	
	/**
	 * @return width
	 */
	public Integer getWidth() {
		return width;
	}

	
	/**
	 * @param width
	 */
	public void setWidth(Integer width) {
		this.width = width;
	}

	
	/**
	 * @return height
	 */
	public Integer getHeight() {
		return height;
	}

	
	/**
	 * @param height
	 */
	public void setHeight(Integer height) {
		this.height = height;
	}

	
	/**
	 * @return top
	 */
	public Integer getTop() {
		return top;
	}

	
	/**
	 * @param top
	 */
	public void setTop(Integer top) {
		this.top = top;
	}

	
	/**
	 * @return left
	 */
	public Integer getLeft() {
		return left;
	}

	
	/**
	 * @param left
	 */
	public void setLeft(Integer left) {
		this.left = left;
	}

	
	/**
	 * @return eventList
	 */
	public List<Event> getEventList() {
		return eventList;
	}

	
	/**
	 * @param eventList
	 */
	public void setEventList(List<Event> eventList) {
		this.eventList = eventList;
	}

	
	/**
	 * @return sqlList
	 */
	public List<Sql> getSqlList() {
		return sqlList;
	}

	
	/**
	 * @param sqlList
	 */
	public void setSqlList(List<Sql> sqlList) {
		this.sqlList = sqlList;
	}

	
	/**
	 * @return expCode
	 */
	public String getExpCode() {
		return expCode;
	}

	
	/**
	 * @param expCode
	 */
	public void setExpCode(String expCode) {
		this.expCode = expCode;
	}

	
	/**
	 * @return autoIdRule
	 */
	public String getAutoIdRule() {
		return autoIdRule;
	}

	
	/**
	 * @param autoIdRule
	 */
	public void setAutoIdRule(String autoIdRule) {
		this.autoIdRule = autoIdRule;
	}

	
	/**
	 * @return quickAdd
	 */
	public Boolean getQuickAdd() {
		return quickAdd;
	}

	
	/**
	 * @param quickAdd
	 */
	public void setQuickAdd(Boolean quickAdd) {
		this.quickAdd = quickAdd;
	}

	/**
	 * @return visibleCondition
	 */
	public String getVisibleCondition() {
		return visibleCondition;
	}

	
	/**
	 * @param visibleCondition
	 */
	public void setVisibleCondition(String visibleCondition) {
		this.visibleCondition = visibleCondition;
	}

	
	/**
	 * @return fieldSuffix
	 */
	public String getFieldSuffix() {
		return fieldSuffix;
	}

	
	/**
	 * @param fieldSuffix
	 */
	public void setFieldSuffix(String fieldSuffix) {
		this.fieldSuffix = fieldSuffix;
	}

	
	/**
	 * @return autoCollapse
	 */
	public Boolean getAutoCollapse() {
		return autoCollapse;
	}

	
	/**
	 * @param autoCollapse
	 */
	public void setAutoCollapse(Boolean autoCollapse) {
		this.autoCollapse = autoCollapse;
	}

	
	/**
	 * @return validInView
	 */
	public Boolean getValidInView() {
		return validInView;
	}

	
	/**
	 * @param validInView
	 */
	public void setValidInView(Boolean validInView) {
		this.validInView = validInView;
	}

	
	/**
	 * @return autoComplete
	 */
	public Boolean getAutoComplete() {
		return autoComplete;
	}

	
	/**
	 * @param autoComplete
	 */
	public void setAutoComplete(Boolean autoComplete) {
		this.autoComplete = autoComplete;
	}

	
	/**
	 * @return uniqueFilterMaster
	 */
	public Boolean getUniqueFilterMaster() {
		return uniqueFilterMaster;
	}

	
	/**
	 * @param uniqueFilterMaster
	 */
	public void setUniqueFilterMaster(Boolean uniqueFilterMaster) {
		this.uniqueFilterMaster = uniqueFilterMaster;
	}

	
	/**
	 * @return limit
	 */
	public Integer getLimit() {
		return limit;
	}

	
	/**
	 * @param limit
	 */
	public void setLimit(Integer limit) {
		this.limit = limit;
	}

	
	/**
	 * @return style
	 */
	public String getStyle() {
		return style;
	}

	
	/**
	 * @param style
	 */
	public void setStyle(String style) {
		this.style = style;
	}

	
	/**
	 * @return className
	 */
	public String getClassName() {
		return className;
	}

	
	/**
	 * @param className
	 */
	public void setClassName(String className) {
		this.className = className;
	}
	
}
