/*
 * @(#)TableEntityCache.java
 * 2011-9-28 下午11:35:59
 *
 * Copyright (c) 2018-2028, HangZhou QiYun InfoTech Co.,Ltd. .
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.qyxx.platform.gsc.cache;

import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;

import org.apache.commons.lang.StringUtils;

import com.qyxx.jwp.datasource.TableEntity;
import com.qyxx.jwp.datasource.TeAttr;
import com.qyxx.platform.gsc.utils.Constants;


/**
 *  实体类缓存
 *  @author gxj
 *  @version 1.0
 *  @since 1.6 2011-9-28 下午11:35:59
 */

public final class TableEntityCache {
	
	private static TableEntityCache tec;
	
	//存放实体key及实体对象
	private ConcurrentMap<String, TableEntity> teMap;
	
	//存放模块key、名称
	private ConcurrentMap<String, String> tdMap;
	
	//检查导入时缓存
	private ConcurrentMap<String, List<String>> importCheckUniqueAttrMap;
	
	//作为共享用户字段属性集合
	private ConcurrentMap<String, List<TeAttr>> useAsShareUserAttrMap;
	
	//自动编号规则
	private ConcurrentMap<String, Map<String, String>> autoIdRuleAttrMap;
	
	//作为权限控制字段属性集合
	private ConcurrentMap<String, List<TeAttr>> useAsAuthFieldAttrMap;
	
	private TableEntityCache() {
		teMap = new ConcurrentHashMap<String, TableEntity>();
		tdMap = new ConcurrentHashMap<String, String>();
		importCheckUniqueAttrMap = new ConcurrentHashMap<String, List<String>>();
		useAsShareUserAttrMap = new ConcurrentHashMap<String, List<TeAttr>>();
		autoIdRuleAttrMap = new ConcurrentHashMap<String, Map<String, String>>();
		useAsAuthFieldAttrMap = new ConcurrentHashMap<String, List<TeAttr>>();
	}
	
	public static synchronized TableEntityCache getInstance() {
		if(null==tec) {
			tec = new TableEntityCache();
		}
		return tec;
	}
	
	public void put(String entityName, TableEntity te) {
		this.teMap.put(entityName, te);
		this.importCheckUniqueAttrMap.put(entityName, te.getCheckImportUniqueAttrs());
		this.useAsShareUserAttrMap.put(entityName, te.getUseAsShareUserAttrs());
		this.autoIdRuleAttrMap.put(entityName, te.getAutoIdRules());
		this.useAsAuthFieldAttrMap.put(entityName, te.getUseAsAuthFieldAttrs());
	}
	
	/**
	 * 根据实体名获取实体对象
	 * 
	 * @param entityName
	 * @return
	 */
	public TableEntity get(String entityName) {
		return this.teMap.get(entityName);
	}
	
	public void putModuleName(String key, String value) {
		if(null!=key && null!=value) {
			this.tdMap.put(key, value);
		}
	}
	
	/**
	 * 根据模块Key获取模块名称
	 * 
	 * @param key
	 * @return
	 */
	public String getModuleName(String key) {
		return this.tdMap.get(key);
	}
	
	/**
	 * 根据实体名获取模块名称
	 * 
	 * @param entityName
	 * @return
	 */
	public String getModuleNameByEntityName(String entityName) {
		String[] ens = StringUtils.split(entityName, Constants.NAME_SPLIT_SYMBOL);
		return getModuleName(ens[0]);
	}
	
	/**
	 * 根据实体名获取导入唯一性检查属性
	 * 
	 * @param entityName
	 * @return
	 */
	public List<String> getImportCheckUniqueAttrs(String entityName) {
		return this.importCheckUniqueAttrMap.get(entityName);
	}
	
	/**
	 * 根据实体名获取作为用户共享字段属性列表
	 * 
	 * @param entityName
	 * @return
	 */
	public List<TeAttr> getUseAsShareUserAttrMap(String entityName) {
		return this.useAsShareUserAttrMap.get(entityName);
	}
	
	/**
	 * @param useAsShareUserAttrMap
	 */
	public void setUseAsShareUserAttrMap(
			ConcurrentMap<String, List<TeAttr>> useAsShareUserAttrMap) {
		this.useAsShareUserAttrMap = useAsShareUserAttrMap;
	}
	
	/**
	 * 返回实体自动编号规则配置
	 * 
	 * @param entityName
	 * @return
	 */
	public Map<String, String> getAutoIdRules(String entityName) {
		return this.autoIdRuleAttrMap.get(entityName);
	}

	/**
	 * 根据实体名获取权限控制字段列表
	 * @return useAsAuthFieldAttrMap
	 */
	public List<TeAttr> getUseAsAuthFieldAttrMap(String entityName) {
		return this.useAsAuthFieldAttrMap.get(entityName);
	}

	public void destroy() {
		this.teMap.clear();
		this.tdMap.clear();
		this.importCheckUniqueAttrMap.clear();
		this.useAsShareUserAttrMap.clear();
		this.autoIdRuleAttrMap.clear();
		this.useAsAuthFieldAttrMap.clear();
	}

}
