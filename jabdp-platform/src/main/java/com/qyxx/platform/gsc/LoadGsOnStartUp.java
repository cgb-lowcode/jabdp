/*
 * @(#)LoadGsOnStartUp.java
 * 2011-11-14 下午10:44:45
 *
 * Copyright (c) 2018-2028, HangZhou QiYun InfoTech Co.,Ltd. .
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.qyxx.platform.gsc;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.qyxx.platform.sysmng.logmng.service.LogManager;


/**
 *  加载GS缓存文件，应用启动时调用
 *  @author gxj
 *  @version 1.0
 *  @since 1.6 2011-11-14 下午10:44:45
 */

public class LoadGsOnStartUp {
	
	private static Logger logger = LoggerFactory.getLogger(LoadGsOnStartUp.class);
	
	public void init() {
		logger.info("加载GS缓存操作开始...");
		GsParseUtils.loadCacheOnStartup();
		logger.info("加载GS缓存操作结束...");
	}

}
