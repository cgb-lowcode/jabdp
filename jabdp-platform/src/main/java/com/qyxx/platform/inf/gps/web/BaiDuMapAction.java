/*
 * @(#)BaiDuMapAction.java
 * 2017年12月29日 下午5:40:41
 * 
 *
 * Copyright (c) 2018-2028, HangZhou QiYun InfoTech Co.,Ltd. .
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.qyxx.platform.inf.gps.web;

import java.util.Map;

import org.apache.struts2.convention.annotation.Namespace;
import org.apache.struts2.convention.annotation.Results;

import com.qyxx.platform.common.json.JsonOutput;
import com.qyxx.platform.common.module.web.CrudActionSupport;
import com.qyxx.platform.common.utils.web.struts2.Struts2Utils;
import com.qyxx.platform.inf.gps.service.BaiDuMapService;

/**
 *  百度地图API web服务
 *  @author gxj
 *  @version 1.0 2017年12月29日 下午5:40:41
 *  @since jdk1.6 
 */
@Namespace("/inf/gps/baidu")
@Results({})
public class BaiDuMapAction extends CrudActionSupport <Map<String, Object>> {

	/**
	 * long
	 */
	private static final long serialVersionUID = -6644516999435089732L;
	
	private String address;//解析地址
	
	private String coordtype;//坐标系类型
	
	/**
	 * @return address
	 */
	public String getAddress() {
		return address;
	}

	
	/**
	 * @param address
	 */
	public void setAddress(String address) {
		this.address = address;
	}

	
	/**
	 * @return coordtype
	 */
	public String getCoordtype() {
		return coordtype;
	}

	/**
	 * @param coordtype
	 */
	public void setCoordtype(String coordtype) {
		this.coordtype = coordtype;
	}

	/**
	 * 
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub

	}

	@Override
	public String list() throws Exception {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String input() throws Exception {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String save() throws Exception {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String view() throws Exception {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String delete() throws Exception {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	protected void prepareModel() throws Exception {
		// TODO Auto-generated method stub
		
	}
	
	/**
	 * 根据地址获取经纬度
	 * 
	 * @return
	 * @throws Exception
	 */
	@JsonOutput
	public String getGpsByAddress() throws Exception {
		Map<String, Object> result = BaiDuMapService.getGpsByAddress(address, coordtype);
		formatMessage.setMsg(result);
		Struts2Utils.renderJson(formatMessage);
		return null;
	}

}
