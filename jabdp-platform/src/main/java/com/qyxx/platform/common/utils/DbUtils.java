/*
 * @(#)DbUtils.java
 * 2013-6-26 下午03:23:18
 * 
 *
 * Copyright (c) 2018-2028, HangZhou QiYun InfoTech Co.,Ltd. .
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.qyxx.platform.common.utils;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

import org.apache.tools.ant.Project;
import org.apache.tools.ant.taskdefs.SQLExec;
import org.apache.tools.ant.taskdefs.SQLExec.DelimiterType;
import org.apache.tools.ant.types.EnumeratedAttribute;
import org.apache.tools.ant.types.FileSet;


/**
 *  数据库脚本初始化类
 *  @author gxj
 *  @version 1.0 2013-6-26 下午03:23:18
 *  @since jdk1.6 
 */

public class DbUtils {
	
	/**
	 * 读取jdbc配置属性文件
	 * 
	 * @param filePath
	 * @return
	 * @throws IOException
	 */
	public static Properties loadProp(String filePath) throws IOException {
		Properties prop = new Properties();// 属性集合对象
		InputStream fis = new FileInputStream(filePath + "/classes/application.properties");
		prop.load(fis);// 将属性文件流装载到Properties对象中
		fis.close();// 关闭流
		return prop;
	}
	
	public static void execSql(Properties prop, String filePath) {
		SQLExec sqlExec = new SQLExec();
		String jdbcUrl = prop.getProperty("jdbc.url");
		String sqlPath = "mssql";
		if(jdbcUrl.indexOf(":sqlserver:") > 0) {
			sqlPath = "mssql";
			//sqlExec.setDelimiter("go");
			//sqlExec.setDelimiterType((SQLExec.DelimiterType) (EnumeratedAttribute
			//		.getInstance(SQLExec.DelimiterType.class, "row")));
		} else if(jdbcUrl.indexOf(":mysql:") > 0) {
			sqlPath = "mysql";
		} else if(jdbcUrl.indexOf(":oracle:") > 0) {
			sqlPath = "oracle";
		} else {
			System.out.println("不支持的数据库");
			return;
		}
		String sqlFilePath = filePath + "/classes/sql/" + sqlPath + "/";
		// 设置数据库参数
		sqlExec.setDriver(prop.getProperty("jdbc.driver"));
		sqlExec.setUrl(prop.getProperty("jdbc.url"));
		sqlExec.setUserid(prop.getProperty("jdbc.username"));
		sqlExec.setPassword(prop.getProperty("jdbc.password"));
		// 要执行的脚本
		sqlExec.setEncoding("UTF-8");
		
		// 有出错的语句该如何处理
		sqlExec.setOnerror((SQLExec.OnError) (EnumeratedAttribute
				.getInstance(SQLExec.OnError.class, "abort")));
		sqlExec.setPrint(true); // 设置是否输出
		// 输出到文件 sql.out 中；不设置该属性，默认输出到控制台
		sqlExec.setOutput(new File(filePath + "/init-db-log.out"));
		sqlExec.setProject(new Project()); // 要指定这个属性，不然会出错
		System.out.println("1、系统表初始化");
		System.out.println("2、数据初始化");
		FileSet fs = new FileSet();
		fs.setProject(new Project());
		fs.setDir(new File(sqlFilePath));
		fs.setIncludes("01-schema.sql");
		fs.setIncludes("02-data.sql");
		sqlExec.add(fs);		
		sqlExec.execute();
		System.out.println("3、数据库初始化成功");
	}

	/**
	 * 
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		//String filePath = "E:/jiawasoft/jwpf/jwpf-mysql/webapps/jwpf/WEB-INF";
		String filePath = args[0];
		try {
			Properties prop = loadProp(filePath);
			execSql(prop, filePath);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}

}
