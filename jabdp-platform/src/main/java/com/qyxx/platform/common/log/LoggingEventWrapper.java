/*
 * @(#)LoggingEventWrapper.java
 * 2011-5-29 下午10:11:26
 *
 * Copyright (c) 2018-2028, HangZhou QiYun InfoTech Co.,Ltd. .
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.qyxx.platform.common.log;

import java.util.Date;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.PatternLayout;
import org.apache.log4j.spi.LoggingEvent;

/**
 *  Log4j LoggingEvent的包装类, 提供默认的toString函数及更直观的属性访问方法.
 *  
 *  @author gxj
 *  @version 1.0
 *  @since 1.6 2011-5-29 下午10:11:26
 */
public class LoggingEventWrapper {
	public static final PatternLayout DEFAULT_PATTERN_LAYOUT = new PatternLayout("%d [%t] %-5p %c - %m");

	private final LoggingEvent event;

	public LoggingEventWrapper(LoggingEvent event) {
		this.event = event;
	}

	/**
	 * 使用默认的layoutPattern转换事件到日志字符串.
	 */
	public String convertToString() {
		return DEFAULT_PATTERN_LAYOUT.format(event);
	}

	/**
	 * 根据参数中的layoutPattern转换事件到日志字符串.
	 */
	public String convertToString(String layoutPattern) {
		return new PatternLayout(layoutPattern).format(event);
	}

	public long getTimeStamp() {
		return event.getTimeStamp();
	}

	public Date getDate() {
		return new Date(event.getTimeStamp());
	}

	public String getThreadName() {
		return event.getThreadName();
	}

	public String getLoggerName() {
		return event.getLoggerName();
	}

	public String getLevel() {
		return event.getLevel().toString();
	}

	public String getMessage() {
		StringBuffer sb = new StringBuffer();
		sb.append((String) event.getMessage());
		sb.append("\r\n");
		sb.append(getThrowableInfo());
		return sb.toString();
	}
	
	public String getThrowableInfo() {
		String[] info = event.getThrowableStrRep();
		return StringUtils.join(info, "\r\n");
	}

	/**
	 * 影响性能,慎用.
	 */
	public String getClassName() {
		return event.getLocationInformation().getClassName();
	}

	/**
	 * 影响性能,慎用.
	 */
	public String getMethodName() {
		return event.getLocationInformation().getMethodName();
	}
	
	public static void main(String[] args) {
		String[] info = {"2222","33333"};
		System.out.println(StringUtils.join(info, "\r\n"));
	}
}
