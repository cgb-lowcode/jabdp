/*
 * @(#)DefinitionSource.java
 * 2011-5-24 下午09:21:09
 *
 * Copyright (c) 2018-2028, HangZhou QiYun InfoTech Co.,Ltd. .
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.qyxx.platform.common.definition;

import java.util.List;

import com.qyxx.platform.common.definition.Definition.DefinitionEntity;

/**
 *  数据字典访问接口
 *  
 *  @author gxj
 *  @version 1.0
 *  @since 1.6 2011-5-24 下午09:21:09
 */
public interface DefinitionSource {
	
	/**
	 * 根据数据字典类别关键字获取数据字典列表
	 * 
	 * @param keyWord 数据字典类别关键字
	 * @return
	 */
	List<DefinitionEntity> getDefinitions(String keyWord);
	
	/**
	 * 根据数据字典类别关键字及语言类别获取数据字典列表
	 * 
	 * @param keyWord
	 * @param locale
	 * @return
	 */
	List<DefinitionEntity> getDefinitions(String keyWord, String locale);
	
}
