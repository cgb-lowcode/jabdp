/*
 * @(#)NewSimpleDateFormat.java
 * 2013-4-17 下午01:52:22
 * 
 *
 * Copyright (c) 2018-2028, HangZhou QiYun InfoTech Co.,Ltd. .
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.qyxx.platform.common.utils.jxls;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;


/**
 *  日期格式化类
 *  @author gxj
 *  @version 1.0 2013-4-17 下午01:52:22
 *  @since jdk1.6 
 */

public class NewSimpleDateFormat {

	private static final long serialVersionUID = -6054302449412625608L;

	private static final String DEFAULT_DATE_FORMAT = "yyyy-MM-dd HH:mm:ss";
	
	private DateFormat df;
	
	public NewSimpleDateFormat() {
		df = new SimpleDateFormat(DEFAULT_DATE_FORMAT);
	}
	
	public NewSimpleDateFormat(String dateFormatStr) {
		df = new SimpleDateFormat(dateFormatStr);
	}
	
	public String format(Date date) {
		if(null!=date) {
			return df.format(date);
		} else {
			return "";
		}
	}

	public String format(Date date, String dateFormatStr) {
		if(null!=date) {
			DateFormat df1 = new SimpleDateFormat(dateFormatStr);
			return df1.format(date);
		} else {
			return "";
		}
	}
	
}
