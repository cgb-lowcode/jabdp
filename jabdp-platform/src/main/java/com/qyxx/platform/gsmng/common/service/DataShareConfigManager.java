/*
 * @(#)DataShareConfigManager.java
 * 2015-3-11 下午02:38:12
 * 
 *
 * Copyright (c) 2018-2028, HangZhou QiYun InfoTech Co.,Ltd. .
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.qyxx.platform.gsmng.common.service;

import java.util.Date;
import java.util.List;

import org.apache.commons.lang.time.DateFormatUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.qyxx.platform.gsmng.common.dao.DataShareConfigDao;
import com.qyxx.platform.gsmng.common.entity.DataShareConfig;


/**
 *  数据共享设置
 *  @author gxj
 *  @version 1.0 2015-3-11 下午02:38:12
 *  @since jdk1.6 
 */
@Service
@Transactional
public class DataShareConfigManager {
	
	/**
	 * 根据实体名和实体ID查找共享用户ID
	 */
	private static final String HQL1 = "select a.shareUserId from DataShareConfig a WHERE a.entityName = ? and a.entityId = ?";

	/**
	 * 根据实体名和共享用户ID查找实体ID
	 */
	private static final String HQL2 = "select a.entityId from DataShareConfig a WHERE a.entityName = ? and a.shareUserId = ?";
	
	/**
	 * 根据实体名和实体ID删除数据
	 */
	private static final String HQL_DEL = "delete from DataShareConfig a WHERE a.entityName = ? and a.entityId = ?";

	
	private DataShareConfigDao dataShareConfigDao;

	
	/**
	 * @param dataShareConfigDao
	 */
	@Autowired
	public void setDataShareConfigDao(
			DataShareConfigDao dataShareConfigDao) {
		this.dataShareConfigDao = dataShareConfigDao;
	}
	
	/**
	 * 根据实体名和实体ID查找共享用户ID
	 * 
	 * @param entityName
	 * @param entityId
	 * @return
	 */
	public List<Long> find(String entityName, String entityId) {
		return dataShareConfigDao.find(HQL1, entityName, entityId);
	}
	
	/**
	 * 根据实体名和共享用户ID查找实体ID
	 * 
	 * @param entityName
	 * @param shareUserId
	 * @return
	 */
	public List<String> find(String entityName, Long shareUserId) {
		return dataShareConfigDao.find(HQL2, entityName, shareUserId);
	}
	
	/**
	 * 保存数据共享设置
	 * 
	 * @param dsc
	 */
	public void saveDataShareConfig(String entityName, Long operUserId, 
							String[] entityIds, Long[] shareUserIds, String log) {
		if(null!=entityIds) {
			for(String entityId : entityIds) {
				//先根据entityName和entityId删除数据共享设置
				dataShareConfigDao.batchExecute(HQL_DEL, entityName, entityId);
				//再重新保存设置
				if(null!=shareUserIds) {
					for(Long shareUserId : shareUserIds) {
						DataShareConfig dsc = new DataShareConfig();
						dsc.setEntityName(entityName);
						dsc.setEntityId(entityId);
						dsc.setShareUserId(shareUserId);
						dsc.setOperUserId(operUserId);
						dsc.setOperTime(new Date());
						dataShareConfigDao.save(dsc);
					}
				}
				//保存数据共享日志,追加服务器时间
				log = log + "[" + DateFormatUtils.format(System.currentTimeMillis(), "yyyy-MM-dd HH:mm:ss") + "]";
				dataShareConfigDao.saveDataShareLog(entityName, entityId, log);
			}
		}
	}
	
}
