package com.qyxx.platform.gsmng.common.dao;

import org.springframework.stereotype.Component;

import com.qyxx.platform.common.orm.hibernate.HibernateDao;
import com.qyxx.platform.gsmng.common.entity.ProcessDefinitionInstance;

/**
 * 流程定义类
 */
@Component
public class ProcessDefDao extends HibernateDao<ProcessDefinitionInstance, Long> {

}
