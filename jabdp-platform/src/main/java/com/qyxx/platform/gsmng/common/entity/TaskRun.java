package com.qyxx.platform.gsmng.common.entity;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

/**
 *  流程任务历史表
 *  
 *  @author xk
 *  @version 1.0 2012-12-28 下午15:25:18
 *  @since jdk1.6
 */
@Entity
@Table(name = "ACT_RU_TASK")
@JsonIgnoreProperties({ "hibernateLazyInitializer", "handler" })
public class TaskRun implements Serializable {

	/**
	 * long
	 */
	private static final long serialVersionUID = -8116983001959424024L;
	private String id;
	private Integer rev;
	private String processDefinitionId;
	private String processInstanceId;
	private String taskDefinitionKey;          
	private String executionId;
	private String name;
	private String parentTaskId;
	private String description;
	private String owner;
	private String assignee;
	private Date createTime;
	private Date dueDate;
	private String delegation;
	private Integer priority;
	private Integer suspensionState;
	
	private String moduleName;
	private String field;
	private String startUser;
	
	private String startUserName;
	private String assigneeName;
	
	@Id
	@Column(name="ID_",length=64,nullable=false)
	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}
	
	@Column(name="PROC_DEF_ID_", length=64)
	public String getProcessDefinitionId() {
		return processDefinitionId;
	}
	
	public void setProcessDefinitionId(String processDefinitionId) {
		this.processDefinitionId = processDefinitionId;
	}
	@Column(name="PROC_INST_ID_",nullable=true)
	public String getProcessInstanceId() {
		return processInstanceId;
	}
	
	public void setProcessInstanceId(String processInstanceId) {
		this.processInstanceId = processInstanceId;
	}
	@Column(name="TASK_DEF_KEY_",nullable=true)
	public String getTaskDefinitionKey() {
		return taskDefinitionKey;
	}
	
	public void setTaskDefinitionKey(String taskDefinitionKey) {
		this.taskDefinitionKey = taskDefinitionKey;
	}
	@Column(name="EXECUTION_ID_",nullable=true)
	public String getExecutionId() {
		return executionId;
	}
	
	public void setExecutionId(String executionId) {
		this.executionId = executionId;
	}
	@Column(name="NAME_")
	public String getName() {
		return name;
	}
	
	public void setName(String name) {
		this.name = name;
	}
	@Column(name="PARENT_TASK_ID_" ,nullable=true,insertable=false,updatable=false)
	public String getParentTaskId() {
		return parentTaskId;
	}
	
	public void setParentTaskId(String parentTaskId) {
		this.parentTaskId = parentTaskId;
	}
	@Column(name="DESCRIPTION_",nullable=true)
	public String getDescription() {
		return description;
	}
	
	public void setDescription(String description) {
		this.description = description;
	}
	@Column(name="OWNER_",nullable=true)
	public String getOwner() {
		return owner;
	}
	
	public void setOwner(String owner) {
		this.owner = owner;
	}
	@Column(name="ASSIGNEE_",nullable=true)
	public String getAssignee() {
		return assignee;
	}
	
	public void setAssignee(String assignee) {
		this.assignee = assignee;
	}

	@Column(name="PRIORITY_",nullable=true)
	public int getPriority() {
		return priority;
	}
	
	public void setPriority(int priority) {
		this.priority = priority;
	}
	@Column(name="CREATE_TIME_",nullable=true)
	@Temporal(value = TemporalType.TIMESTAMP)
	public Date getCreateTime() {
		return createTime;
	}
	
	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}
	
	@Column(name="DUE_DATE_",nullable=true)
	@Temporal(value = TemporalType.TIMESTAMP)
	public Date getDueDate() {
		return dueDate;
	}
	
	public void setDueDate(Date dueDate) {
		this.dueDate = dueDate;
	}
	
	@Column(name="REV_")
	public Integer getRev() {
		return rev;
	}

	
	public void setRev(Integer rev) {
		this.rev = rev;
	}

	@Column(name="DELEGATION_")
	public String getDelegation() {
		return delegation;
	}

	
	public void setDelegation(String delegation) {
		this.delegation = delegation;
	}

	@Column(name="SUSPENSION_STATE_")
	public Integer getSuspensionState() {
		return suspensionState;
	}

	
	public void setSuspensionState(Integer suspensionState) {
		this.suspensionState = suspensionState;
	}

	@Transient
	public String getModuleName() {
		return moduleName;
	}

	
	public void setModuleName(String moduleName) {
		this.moduleName = moduleName;
	}

	
	/**
	 * @return field
	 */
	@Transient
	public String getField() {
		return field;
	}

	
	/**
	 * @param field
	 */
	public void setField(String field) {
		this.field = field;
	}

	
	/**
	 * @return startUser
	 */
	@Transient
	public String getStartUser() {
		return startUser;
	}

	
	/**
	 * @param startUser
	 */
	public void setStartUser(String startUser) {
		this.startUser = startUser;
	}

	
	/**
	 * @return startUserName
	 */
	@Transient
	public String getStartUserName() {
		return startUserName;
	}

	
	/**
	 * @param startUserName
	 */
	public void setStartUserName(String startUserName) {
		this.startUserName = startUserName;
	}

	
	/**
	 * @return assigneeName
	 */
	@Transient
	public String getAssigneeName() {
		return assigneeName;
	}

	
	/**
	 * @param assigneeName
	 */
	public void setAssigneeName(String assigneeName) {
		this.assigneeName = assigneeName;
	}
	
	

}


