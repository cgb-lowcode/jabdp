/*
 * @(#)ProcessInfo.java
 * 2012-9-13 上午11:01:38
 * 
 *
 * Copyright (c) 2018-2028, HangZhou QiYun InfoTech Co.,Ltd. .
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.qyxx.platform.gsmng.common.entity;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;


/**
 *  流程流转历史信息
 *  
 *  @author xk
 *  @version 1.0 2012-9-13 上午11:01:38
 *  @since jdk1.6
 */
@Entity
@Table(name = "SYS_PROCESS_INFO")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
@JsonIgnoreProperties({ "hibernateLazyInitializer", "handler" })
public class ProcessInfo implements Serializable {

	/**
	 * long
	 */
	private static final long serialVersionUID = -1010247935451714904L;
	private Long id;
	private String processInstanceId;// 流程实例ID
	private String executionId;//执行ID
	private Date dealTime;// 处理时间
	private Long userId;//用户ID
	private String dealName; //操作用户
	private String taskId;//环节ID
	private String taskName;//环节名称
	private String isApprove;//是否同意
	private Long businessId;//合同ID
	private String businessName;//合同名称
	private String reason;// 原因
	private String activitiId;//活动ID
	private Long countVal;//同样环节执行次数
	
	private String userName;
	 
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO, generator = "SEQ_SYS_PROCESS_INFO_ID")
	@SequenceGenerator(name = "SEQ_SYS_PROCESS_INFO_ID", sequenceName = "SEQ_SYS_PROCESS_INFO_ID")
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}
	@Column(name="PROCESS_INSTANCE_ID", length=100)
	public String getProcessInstanceId() {
		return processInstanceId;
	}
	
	public void setProcessInstanceId(String processInstanceId) {
		this.processInstanceId = processInstanceId;
	}

	@Column(name="IS_APPROVE", length=50)
	public String getIsApprove() {
		return isApprove;
	}

	
	public void setIsApprove(String isApprove) {
		this.isApprove = isApprove;
	}

	@Column(name="REASON", length=2000)
	public String getReason() {
		return reason;
	}
	
	public void setReason(String reason) {
		this.reason = reason;
	}
	
	@Column(name="EXECUTION_ID", length=100)
	public String getExecutionId() {
		return executionId;
	}

	
	public void setExecutionId(String executionId) {
		this.executionId = executionId;
	}

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="DEAL_TIME")
	public Date getDealTime() {
		return dealTime;
	}

	
	public void setDealTime(Date dealTime) {
		this.dealTime = dealTime;
	}

	@Column(name="USER_ID")
	public Long getUserId() {
		return userId;
	}

	
	public void setUserId(Long userId) {
		this.userId = userId;
	}

	@Column(name="DEAL_NAME", length=100)
	public String getDealName() {
		return dealName;
	}

	
	public void setDealName(String dealName) {
		this.dealName = dealName;
	}

	@Column(name="TASK_ID", length=100)
	public String getTaskId() {
		return taskId;
	}

	
	public void setTaskId(String taskId) {
		this.taskId = taskId;
	}

	@Column(name="TASK_NAME", length=1000)
	public String getTaskName() {
		return taskName;
	}

	
	public void setTaskName(String taskName) {
		this.taskName = taskName;
	}


	@Column(name="BUSINESS_ID")
	public Long getBusinessId() {
		return businessId;
	}

	
	public void setBusinessId(Long businessId) {
		this.businessId = businessId;
	}

	@Column(name="BUSINESS_NAME",length=200)
	public String getBusinessName() {
		return businessName;
	}

	
	public void setBusinessName(String businessName) {
		this.businessName = businessName;
	}

	@Column(name="ACTIVITI_ID", length=200)
	public String getActivitiId() {
		return activitiId;
	}
	public void setActivitiId(String activitiId) {
		this.activitiId = activitiId;
	}
	
	@Column(name="COUNT_VAL")
		
	public Long getCountVal() {
		return countVal;
	}

	
	public void setCountVal(Long countVal) {
		this.countVal = countVal;
	}
	

	/**
	 * @return userName
	 */
	@Transient
	public String getUserName() {
		return userName;
	}

	
	/**
	 * @param userName
	 */
	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String  setBusinessKey(String name,Long id ){
		 String s = name +"|" +id.toString();
			 return s ; 
	 }

	public String []  splitBusinessKey(String key ){	 
		 String s[] = key.split("\\|");
			 return s ; 
	 }
}
