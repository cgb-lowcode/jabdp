/*
 * @(#)BsField.java
 * 2016年10月12日 下午4:13:05
 * 
 * Copyright (c) 2016 QiYunInfoTech - All Rights Reserved.
 * ====================================================================
 * The QiYunInfoTech License, Version 1.0
 *
 * This software is the confidential and proprietary information of QiYunInfoTech.
 * You shall not disclose such Confidential Information and shall use it only in
 * accordance with the terms of the license agreement you entered into with QiYunInfoTech.
 */
package com.qyxx.platform.sysmng.userconfig.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.qyxx.jwp.bean.DataProperties;
import com.qyxx.jwp.bean.EditProperties;
import com.qyxx.platform.common.module.entity.BaseEntity;
import com.qyxx.platform.sysmng.utils.Constants;

/**
 *  业务字段
 *  @author bobgao
 *  @version 1.0 2016年10月12日 下午4:13:05
 *  @since jdk1.7 
 */
@Entity
//表名与类名不相同时重新定义表名.
@Table(name = "SYS_BS_FIELD")
//默认的缓存策略.
//@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
public class BsField extends BaseEntity {
	
	//系统定义
	public static final String FIELD_TYPE_SYSTEM = "system";
	//用户定义
	public static final String FIELD_TYPE_USER = "user";

	private Long id;//主键
	
	private Long masterId;//关联table id
	
	private String tableKey;//编码
	
	private String entityName;//实体名
	
	private String fieldKey;//字段编码
	
	private String caption;//字段名称
	
	private String remark;//备注描述
	
	private String dataType = DataProperties.DATA_TYPE_STRING;//数据类型，默认字符串
	
	private String editType = EditProperties.EDIT_TYPE_TEXT_BOX;//编辑类型/控件类型，默认文本框
	
	private String editDs;//编辑数据源
	
	private String status = Constants.STATUS_DRAFT;//状态 -- 默认草稿
	
	private String format;//显示格式
	
	private String expRule;//公式规则
	
	private String defaultVal;//默认值
	
	private String fieldType = FIELD_TYPE_USER;//属性分类  用户自定义、系统，默认用户
	
	private String moduleKey; //模块key
	
	private String fieldIndex; //字段索引/唯一序号
	
	private String isTextField = Constants.BOOL_NO; //是否显示值字段
	
	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO, generator="SEQ_SYS_BS_FIELD_ID")
	@SequenceGenerator(name="SEQ_SYS_BS_FIELD_ID", sequenceName="SEQ_SYS_BS_FIELD_ID")
    public Long getId() {
		return id;
	}
	
	public void setId(Long id) {
		this.id = id;
	}

	
	/**
	 * @return masterId
	 */
	@Column(name="MASTER_ID")
	public Long getMasterId() {
		return masterId;
	}

	
	/**
	 * @param masterId
	 */
	public void setMasterId(Long masterId) {
		this.masterId = masterId;
	}

	
	/**
	 * @return tableKey
	 */
	@Column(name="TABLE_KEY", length=200)
	public String getTableKey() {
		return tableKey;
	}

	
	/**
	 * @param tableKey
	 */
	public void setTableKey(String tableKey) {
		this.tableKey = tableKey;
	}

	
	/**
	 * @return entityName
	 */
	@Column(name="ENTITY_NAME", length=400)
	public String getEntityName() {
		return entityName;
	}

	
	/**
	 * @param entityName
	 */
	public void setEntityName(String entityName) {
		this.entityName = entityName;
	}

	
	/**
	 * @return fieldKey
	 */
	@Column(name="FIELD_KEY", length=200)
	public String getFieldKey() {
		return fieldKey;
	}

	
	/**
	 * @param fieldKey
	 */
	public void setFieldKey(String fieldKey) {
		this.fieldKey = fieldKey;
	}

	
	/**
	 * @return caption
	 */
	@Column(name="CAPTION", length=200)
	public String getCaption() {
		return caption;
	}

	
	/**
	 * @param caption
	 */
	public void setCaption(String caption) {
		this.caption = caption;
	}

	
	/**
	 * @return remark
	 */
	@Column(name="REMARK", length=1000)
	public String getRemark() {
		return remark;
	}

	
	/**
	 * @param remark
	 */
	public void setRemark(String remark) {
		this.remark = remark;
	}

	
	/**
	 * @return dataType
	 */
	@Column(name="DATA_TYPE", length=100)
	public String getDataType() {
		return dataType;
	}

	
	/**
	 * @param dataType
	 */
	public void setDataType(String dataType) {
		this.dataType = dataType;
	}

	
	/**
	 * @return editType
	 */
	@Column(name="EDIT_TYPE", length=100)
	public String getEditType() {
		return editType;
	}

	
	/**
	 * @param editType
	 */
	public void setEditType(String editType) {
		this.editType = editType;
	}

	
	/**
	 * @return editDs
	 */
	@Lob
	@Column(name="EDIT_DS", columnDefinition = "ntext")
	public String getEditDs() {
		return editDs;
	}

	
	/**
	 * @param editDs
	 */
	public void setEditDs(String editDs) {
		this.editDs = editDs;
	}

	
	/**
	 * @return status
	 */
	@Column(name="STATUS", length=100)
	public String getStatus() {
		return status;
	}

	
	/**
	 * @param status
	 */
	public void setStatus(String status) {
		this.status = status;
	}

	
	/**
	 * @return format
	 */
	@Column(name="FORMAT", length=100)
	public String getFormat() {
		return format;
	}

	
	/**
	 * @param format
	 */
	public void setFormat(String format) {
		this.format = format;
	}

	
	/**
	 * @return expRule
	 */
	@Column(name="EXP_RULE", length=1000)
	public String getExpRule() {
		return expRule;
	}

	
	/**
	 * @param expRule
	 */
	public void setExpRule(String expRule) {
		this.expRule = expRule;
	}

	
	/**
	 * @return defaultVal
	 */
	@Column(name="DEFAULT_VAL", length=200)
	public String getDefaultVal() {
		return defaultVal;
	}

	
	/**
	 * @param defaultVal
	 */
	public void setDefaultVal(String defaultVal) {
		this.defaultVal = defaultVal;
	}

	
	/**
	 * @return fieldType
	 */
	@Column(name="FIELD_TYPE", length=100)
	public String getFieldType() {
		return fieldType;
	}

	
	/**
	 * @param fieldType
	 */
	public void setFieldType(String fieldType) {
		this.fieldType = fieldType;
	}

	
	/**
	 * @return moduleKey
	 */
	@Column(name="MODULE_KEY", length=200)
	public String getModuleKey() {
		return moduleKey;
	}

	
	/**
	 * @param moduleKey
	 */
	public void setModuleKey(String moduleKey) {
		this.moduleKey = moduleKey;
	}

	
	/**
	 * @return fieldIndex
	 */
	@Column(name="FIELD_INDEX", length=100)
	public String getFieldIndex() {
		return fieldIndex;
	}

	
	/**
	 * @param fieldIndex
	 */
	public void setFieldIndex(String fieldIndex) {
		this.fieldIndex = fieldIndex;
	}

	
	/**
	 * @return isTextField
	 */
	@Column(name="IS_TEXT_FIELD", length=10)
	public String getIsTextField() {
		return isTextField;
	}

	
	/**
	 * @param isTextField
	 */
	public void setIsTextField(String isTextField) {
		this.isTextField = isTextField;
	}
	
	
	
}
