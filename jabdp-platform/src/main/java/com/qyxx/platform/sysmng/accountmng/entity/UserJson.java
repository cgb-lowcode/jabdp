/*
 * @(#)UserJson.java
 * 2011-6-11 下午10:58:42
 *
 * Copyright (c) 2018-2028, HangZhou QiYun InfoTech Co.,Ltd. .
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.qyxx.platform.sysmng.accountmng.entity;

import java.io.Serializable;

import org.apache.commons.lang3.StringUtils;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

/**
 * 
 * @author YB
 * @version 1.0
 * @since 1.6 2011-6-11 下午10:58:42
 */
@JsonIgnoreProperties({ "hibernateLazyInitializer", "handler" })
public class UserJson implements Serializable {

	/**
	 * long
	 */
	private static final long serialVersionUID = -8081011870542617483L;
	private String id;
	private String pId;
	private String text;
	private String name;
	private Boolean open;
	private Boolean nocheck;
	private Long userId;
	private Boolean checked;
    private String iconSkin;
    

	public UserJson() {
		super();
		// TODO Auto-generated constructor stub
	}

	public UserJson(User u) {
		super();
		this.id = "user_" + u.getId();
		this.pId = "org_" + u.getOrganization().getId();
		this.name = u.getRealName() + "(" + u.getLoginName() + ")[" + u.getOrganizationName() + "]";
		if(StringUtils.isNotBlank(u.getNickname())) {
			this.name = this.name + "-" + u.getNickname();
		}
		this.iconSkin = "iconUser";
		this.userId = u.getId();
		this.text = u.getNickname();
	}

	public UserJson(Long id, Organization org, String name,
			Boolean open, Boolean nocheck) {
		super();
		this.id = "org_" + id;
		if (org.getOrganization() != null) {
			this.pId = "org_" + org.getOrganization().getId();
		}
		this.name = name;
		this.open = open;
		this.nocheck = nocheck;
		this.iconSkin = "iconGroup";
		this.text = name;
	}

	/**
	 * @return id
	 */
	public String getId() {
		return id;
	}

	/**
	 * @param id
	 */
	public void setId(Long id) {
		this.id = "user_" + id;
	}

	/**
	 * @return text
	 */
	public String getText() {
		return text;
	}

	/**
	 * @param text
	 */
	public void setText(String text) {
		this.text = text;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getpId() {
		return pId;
	}

	public void setpId(String pId) {
		this.pId = pId;
	}

	public Boolean getOpen() {
		return open;
	}

	public void setOpen(Boolean open) {
		this.open = open;
	}

	public Boolean getNocheck() {
		return nocheck;
	}

	public void setNocheck(Boolean nocheck) {
		this.nocheck = nocheck;
	}

	public Long getUserId() {
		return userId;
	}

	public void setUserId(Long userId) {
		this.userId = userId;
	}

	public Boolean getChecked() {
		return checked;
	}

	public void setChecked(Boolean checked) {
		this.checked = checked;
	}
	public String getIconSkin() {
		return iconSkin;
	}
	
	public void setIconSkin(String iconSkin) {
		this.iconSkin = iconSkin;
	}
}
