/*
 * @(#)Dictionary.java
 * 2011-5-22 下午07:59:17
 *
 * Copyright (c) 2018-2028, HangZhou QiYun InfoTech Co.,Ltd. .
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.qyxx.platform.sysmng.dictmng.entity;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import com.qyxx.platform.common.module.entity.BaseEntity;


/**
 *  数据字典
 *  @author gxj
 *  @version 1.0
 *  @since 1.6 2011-5-22 下午07:59:17
 */
@Entity
@Table(name = "SYS_DICTIONARY")
//@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
public class Dictionary extends BaseEntity {
	
	private Long id;
	
	private String dictName;
	
	private String dictValue;
	
	private String status;
	
	private String dictDesc;
	
	private String dictLocale;
	
	private DictionaryType dictType;

	
	/**
	 * @return id
	 */
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO, generator="SEQ_SYS_DICTIONARY_ID")
	@SequenceGenerator(name="SEQ_SYS_DICTIONARY_ID", sequenceName="SEQ_SYS_DICTIONARY_ID")
	public Long getId() {
		return id;
	}

	
	/**
	 * @param id
	 */
	public void setId(Long id) {
		this.id = id;
	}

	
	/**
	 * @return dictName
	 */
	public String getDictName() {
		return dictName;
	}

	
	/**
	 * @param dictName
	 */
	public void setDictName(String dictName) {
		this.dictName = dictName;
	}

	
	/**
	 * @return dictValue
	 */
	public String getDictValue() {
		return dictValue;
	}

	
	/**
	 * @param dictValue
	 */
	public void setDictValue(String dictValue) {
		this.dictValue = dictValue;
	}

	
	/**
	 * @return status
	 */
	public String getStatus() {
		return status;
	}

	
	/**
	 * @param status
	 */
	public void setStatus(String status) {
		this.status = status;
	}

	
	/**
	 * @return dictDesc
	 */
	public String getDictDesc() {
		return dictDesc;
	}

	
	/**
	 * @param dictDesc
	 */
	public void setDictDesc(String dictDesc) {
		this.dictDesc = dictDesc;
	}

	
	/**
	 * @return dictLocale
	 */
	public String getDictLocale() {
		return dictLocale;
	}

	
	/**
	 * @param dictLocale
	 */
	public void setDictLocale(String dictLocale) {
		this.dictLocale = dictLocale;
	}


	
	/**
	 * @return dictType
	 */
	@ManyToOne
	@JoinColumn(name="DICT_TYPE_ID")
	public DictionaryType getDictType() {
		return dictType;
	}


	
	/**
	 * @param dictType
	 */
	public void setDictType(DictionaryType dictType) {
		this.dictType = dictType;
	}

}
