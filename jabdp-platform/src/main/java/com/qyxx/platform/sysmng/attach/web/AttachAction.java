package com.qyxx.platform.sysmng.attach.web;


import java.util.ArrayList;
import java.util.List;

import org.apache.struts2.convention.annotation.Namespace;
import org.springframework.beans.factory.annotation.Autowired;

import com.qyxx.platform.common.json.JsonOutput;
import com.qyxx.platform.common.module.service.ServiceException;
import com.qyxx.platform.common.module.web.CrudActionSupport;
import com.qyxx.platform.common.orm.PropertyFilter;
import com.qyxx.platform.common.utils.web.struts2.Struts2Utils;
import com.qyxx.platform.gsc.utils.SystemParam;
import com.qyxx.platform.sysmng.accountmng.entity.User;
import com.qyxx.platform.sysmng.attach.entity.Attach;
import com.qyxx.platform.sysmng.attach.entity.AttachList;
import com.qyxx.platform.sysmng.attach.service.AttachManager;
import com.qyxx.platform.sysmng.utils.Constants;
/**
 * 附件列表处理类
 */
@Namespace("/sys/attach")
public class AttachAction extends CrudActionSupport<AttachList> {

	private static final long serialVersionUID = 112L;

	private AttachManager attachManager;
	//页面参数
	private AttachList attachList;
	private Long attachId;
	private Attach attach;
	private Long id;
	private Long[] ids;
	
	public Long[] getIds() {
		return ids;
	}
	public void setIds(Long[] ids) {
		this.ids = ids;
	}
	//系统配置参数
	private SystemParam systemParam;
	
	public SystemParam getSystemParam() {
		return systemParam;
	}
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getAttachId() {
		return attachId;
	}

	public void setAttachId(Long attachId) {
		this.attachId = attachId;
	}

	public Attach getAttach() {
		return attach;
	}

	public void setAttach(Attach attach) {
		this.attach = attach;
	}

	public AttachList getAttachList() {
		return attachList;
	}

	public void setAttachList(AttachList attachList) {
		this.attachList = attachList;
	}

	public void setAttachManager(AttachManager attachManager) {
		this.attachManager = attachManager;
	}
	//查询附件列表
	@JsonOutput
	public String queryList() throws Exception {
		if(attachId != null) {
			List<PropertyFilter> filters = PropertyFilter.buildFromHttpRequest(Struts2Utils.getRequest());
			PropertyFilter filter = new PropertyFilter("EQL_attachId", String.valueOf(attachId));
			filters.add(filter);
			pager = attachManager.findAttachList(pager, filters);
		} else {
			pager.setTotalCount(0L);
		}
		Struts2Utils.renderJson(pager);
		return null;
	}

	@Override
	public String input() throws Exception {
		// TODO Auto-generated method stub
		return null;
	}
	//保存附件
	@Override
	@JsonOutput
	public String save() throws Exception {
		User user = (User)Struts2Utils.getSessionAttribute(Constants.USER_SESSION_ID);
		Long userId = 1L;
		if(null!=user) userId = user.getId();
		String attId = attachManager.saveAttach(userId);
		formatMessage.setMsg(attId);
		Struts2Utils.renderJson(formatMessage);
		return null;
	}

	@Override
	public String view() throws Exception {
		// TODO Auto-generated method stub
		return null;
	}
	//删除附件
	@Override
	@JsonOutput
	public String delete() throws Exception {
		if (ids==null) {
			throw new ServiceException("请选择你要删除的附件", new Exception(
					"请选择你要删除的附件"));
		}
		
		boolean flag = attachManager.deleteAttachs(ids);
		if(flag){
		formatMessage.setMsg(true);
		Struts2Utils.renderJson(formatMessage);
		}else{
		formatMessage.setMsg(false);
		Struts2Utils.renderJson(formatMessage);
		}
		return null;
	}

	@Override
	protected void prepareModel() throws Exception {
		// TODO Auto-generated method stub

	}

	@Override
	public String list() throws Exception {
		return SUCCESS;
	}
	
	@Autowired
	public void setSystemParam(SystemParam systemParam) {
		this.systemParam = systemParam;
	}
	
	
}
