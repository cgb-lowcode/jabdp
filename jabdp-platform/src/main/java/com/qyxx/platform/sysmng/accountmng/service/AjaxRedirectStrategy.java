/*
 * @(#)AjaxRedirectStrategy.java
 * 2015年11月27日 下午5:07:08
 * 
 *
 * Copyright (c) 2018-2028, HangZhou QiYun InfoTech Co.,Ltd. .
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.qyxx.platform.sysmng.accountmng.service;

import java.io.IOException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.security.web.DefaultRedirectStrategy;

import com.qyxx.platform.common.module.web.FormatMessage;
import com.qyxx.platform.common.utils.web.struts2.Struts2Utils;


/**
 *  判断是否ajax请求，重定向url或输出json数据
 *  @author gxj
 *  @version 1.0 2015年11月27日 下午5:07:08
 *  @since jdk1.6 
 */

public class AjaxRedirectStrategy extends DefaultRedirectStrategy {
	
	private FormatMessage fm;
	
	/**
	 * @return fm
	 */
	public FormatMessage getFm() {
		return fm;
	}

	/**
	 * @param fm
	 */
	public void setFm(FormatMessage fm) {
		this.fm = fm;
	}


	/** 
	 * @see org.springframework.security.web.DefaultRedirectStrategy#sendRedirect(javax.servlet.http.HttpServletRequest, javax.servlet.http.HttpServletResponse, java.lang.String)
	 */
	@Override
	public void sendRedirect(HttpServletRequest request,
			HttpServletResponse response,
			String url) throws IOException {
		boolean isAjax = "XMLHttpRequest".equals(request.getHeader("X-Requested-With"));
		if(isAjax) { //ajax请求返回
			Struts2Utils.renderJson(fm, response);
		} else { //url重定向
			super.sendRedirect(request, response, url);
		}
	}
	
	

}
