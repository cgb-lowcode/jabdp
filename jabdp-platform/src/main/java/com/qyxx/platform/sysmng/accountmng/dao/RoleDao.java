/*
 * @(#)RoleDao.java
 * 2011-5-9 下午09:08:33
 *
 * Copyright (c) 2018-2028, HangZhou QiYun InfoTech Co.,Ltd. .
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.qyxx.platform.sysmng.accountmng.dao;

import java.util.List;

import org.springframework.stereotype.Component;

import com.qyxx.platform.common.orm.hibernate.HibernateDao;
import com.qyxx.platform.sysmng.accountmng.entity.Role;
import com.qyxx.platform.sysmng.accountmng.entity.User;

/**
 *  角色对象的泛型DAO
 *  
 *  @author gxj
 *  @version 1.0
 *  @since 1.6 2011-5-9 下午09:08:33
 */
@Component
public class RoleDao extends HibernateDao<Role, Long> {

	private static final String QUERY_USER_BY_ROLEID = "select u from User u left join u.roleList r where r.id=?";
	private static final String INSERT = "INSERT INTO SYS_USER_TO_ROLE(USER_ID, ROLE_ID) VALUES(?, ?)";
	private static final String DELETE_USER_ID = "DELETE FROM SYS_USER_TO_ROLE WHERE USER_ID=?";
	private static final String DELETE_ROLE_ID = "DELETE FROM SYS_USER_TO_ROLE WHERE ROLE_ID=?";
	
	private static final String QUERY_USER_ID_BY_ROLE_NAMES = "select distinct(u.id) from User u inner join u.roleList r where r.roleName in (:roleNames)";
	
	/**
	 * 根据用户编号查询角色对应的首页地址与门户地址
	 */
	private static final String QUERY_ROLE_URL_BY_USER_ID = "select new Role(r.id, r.roleName, r.indexUrl, r.portalUrl) from Role r inner join r.userList u where u.id =? " +
			" and r.roleStatus = ? ";
	
	/**
	 * 根据角色名和组织编码查询用户
	 */
	private static final String QUERY_USER_BY_ROLE_NAME_AND_ORG_CODE = "select distinct u from User u inner join u.roleList r where u.status='1' and r.roleName = :rn and u.organization.organizationCode in (:orgCodes)";
	
	/**
	 * 重载函数,因为Role中没有建立与User的关联,因此需要以较低效率的方式进行删除User与Role的多对多中间表.
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void delete(Long id) {
		Role role = get(id);
		//查询出拥有该角色的用户,并删除该用户的角色.
		List<User> users = createQuery(QUERY_USER_BY_ROLEID, role.getId()).list();
		for (User u : users) {
			u.getRoleList().remove(role);
		}
		super.delete(role);
	}
	
	public void save(Long u_id,Long r_id){
		 getSession().createSQLQuery(INSERT).setLong(0, u_id).setLong(1,r_id).executeUpdate();
	}
	
	public void deleteUserList(Long id){
		 getSession().createSQLQuery(DELETE_USER_ID).setLong(0, id).executeUpdate();
		 
	}
	public void deleteRoleList(Long id){
		 getSession().createSQLQuery(DELETE_ROLE_ID).setLong(0, id).executeUpdate();
	}
	
	/**
	 * 根据角色名称列表获取用户编号
	 * 
	 * @param roleNames
	 * @return
	 */
	@SuppressWarnings("unchecked")
	public List<Long> findUserIdByRoleNames(String[] roleNames) {
		return (List<Long>)getSession().createQuery(QUERY_USER_ID_BY_ROLE_NAMES).setParameterList("roleNames", roleNames).list();
	}
	
	/**
	 * 根据用户编号获取首页地址与门户地址
	 * 
	 * @param userId
	 * @param roleStatus
	 * @return
	 */
	public List<Role> findRoleUrlByUserId(Long userId, String roleStatus) {
		return this.find(QUERY_ROLE_URL_BY_USER_ID, userId, roleStatus);
	}
	
	/**
	 * 根据角色名和组织编码查找当前组织下拥有该角色的人
	 * 
	 * @param roleName
	 * @param orgCodeList
	 * @return
	 */
	@SuppressWarnings("unchecked")
	public List<User> findUserByRoleNameAndOrgCode(String roleName, List<String> orgCodeList) {
		return (List<User>)getSession().createQuery(QUERY_USER_BY_ROLE_NAME_AND_ORG_CODE)
					.setString("rn", roleName)
					.setParameterList("orgCodes", orgCodeList).list();
	}
	
	
}
