/*
 * @(#)Notice.java
 * 2012-9-4 下午05:40:39
 * 
 *
 * Copyright (c) 2018-2028, HangZhou QiYun InfoTech Co.,Ltd. .
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.qyxx.platform.sysmng.notice.entity;

import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.OneToMany;
import javax.persistence.OrderBy;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.google.common.collect.Lists;
import com.qyxx.platform.common.module.entity.BaseEntity;

/**
 *  
 *  @author ly
 *  @version 1.0 2012-9-4 下午05:40:39
 *  @since jdk1.6 
 */

/**
 * 系统通知表
 */
@Entity
@Table(name = "SYS_NOTICE")
// 默认的缓存策略.
//@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
@JsonIgnoreProperties({ "hibernateLazyInitializer", "handler" })
public class Notice extends BaseEntity {

	public static final String STATUS_0 = "0";// 0.草稿 1.已发布
	public static final String STATUS_1 = "1";
	public static final String STYLE_01="01";
	public static final String STYLE_02="02";
	public static final String STYLE_03="03";
	public static final String STYLE_04="04";
	public static final String STYLE_05="05";
	public static final String STYLE_06="06";
	private Long id;
	private String title;
	private String keyworlds;
	private String content;
	private String status;
	//private String type; // user 用户 system系统
	private Long type;
	 private Long atmId;//附件id
	private List<NoticeToUser> noticeToUserList = Lists
			.newArrayList();
	private String style;/*01---普通通知  02---预警通知 03---待办事宜通知 04---业务确认通知  05---提醒通知 ...06---业务办理通知 ...*/
	private String level;/*普通通知级别有：0---普通通知；1---紧急通知；预警通知级别有：100--警告；200--重大；300---严重；待办事宜通知有：0---普通通知；1---紧急通知；*/
	private List<Long> userIds = Lists.newArrayList();
	private String noticeToUserIds;
	private String readStatus;
	private Date readTime;
    private Long ntuId;
	private Long num;
	
	private String moduleKey;
	
	private String moduleDataId;
    
	private String moduleFields;
	
	/**
	 * @return the atmId
	 */
    @Column(name = "ATM_ID")
	public Long getAtmId() {
		return atmId;
	}

	/**
	 * @param atmId the atmId to set
	 */
	public void setAtmId(Long atmId) {
		this.atmId = atmId;
	}

	public Notice(Notice notice, NoticeToUser ntu) {
		this.id = notice.getId();
		this.title =notice.getTitle();
		this.keyworlds = notice.getKeyworlds();
		this.content = notice.getContent();
		this.status = notice.getStatus();
		this.type = notice.getType();
		this.style=notice.getStyle();
		this.level=notice.getLevel();
		this.userIds = notice.getUserIds();
		this.readStatus = ntu.getStatus();
		this.readTime = ntu.getReadTime();
		this.ntuId=ntu.getId();
		this.createUser=notice.getCreateUser();
		this.createTime=notice.getCreateTime();
		this.lastUpdateUser=notice.getLastUpdateUser();
		this.lastUpdateTime=notice.getLastUpdateTime();
		this.atmId=notice.getAtmId();
		this.num=notice.getNum();
		this.moduleKey = notice.getModuleKey();
		this.moduleDataId = notice.getModuleDataId();
		this.moduleFields = notice.getModuleFields();
	}

	public Notice() {
		
	}

	/**
	 * @return id
	 */
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO, generator = "SEQ_SYS_NOTICE_ID")
	@SequenceGenerator(name = "SEQ_SYS_NOTICE_ID", sequenceName = "SEQ_SYS_NOTICE_ID")
	public Long getId() {
		return id;
	}

	/**
	 * @param id
	 */
	public void setId(Long id) {
		this.id = id;
	}

	/**
	 * @return title
	 */
	@Column(name = "TITLE", length = 200)
	public String getTitle() {
		return title;
	}

	/**
	 * @param title
	 */
	public void setTitle(String title) {
		this.title = title;
	}

	/**
	 * @return keyworlds
	 */
	@Column(name = "KEY_VAL", length = 100)
	public String getKeyworlds() {
		return keyworlds;
	}

	/**
	 * @param keyworlds
	 */
	public void setKeyworlds(String keyworlds) {
		this.keyworlds = keyworlds;
	}

	/**
	 * @return content
	 */
	@Lob
	@Column(name = "CONTENT_VAL", columnDefinition = "ntext")
	public String getContent() {
		return content;
	}

	/**
	 * @param content
	 */
	public void setContent(String content) {
		this.content = content;
	}

	/**
	 * @return status
	 */
	@Column(name = "STATUS", length = 1)
	public String getStatus() {
		return status;
	}

	/**
	 * @param status
	 */
	public void setStatus(String status) {
		this.status = status;
	}

	
	
	/**
	 * @return type
	 *//*
	@Column(name = "TYPE", length = 50)
	public String getType() {
		return type;
	}

	*//**
	 * @param type
	 *//*
	public void setType(String type) {
		this.type = type;
	}*/
	
	

	/**
	 * @return the type
	 */
	@Column(name = "TYPE")
	public Long getType() {
		return type;
	}

	/**
	 * @param type the type to set
	 */
	public void setType(Long type) {
		this.type = type;
	}

	/**
	 * @return noticeToUserList
	 */
	@OneToMany(mappedBy = "notice")
	@OrderBy("id")
	@JsonIgnore
	public List<NoticeToUser> getNoticeToUserList() {
		return noticeToUserList;
	}

	/**
	 * @param noticeToUserList
	 */
	public void setNoticeToUserList(
			List<NoticeToUser> noticeToUserList) {
		this.noticeToUserList = noticeToUserList;
	}

	
	
	/**
	 * @return the style
	 */
	@Column(name = "STYLE")
	public String getStyle() {
		return style;
	}

	/**
	 * @param style the style to set
	 */
	public void setStyle(String style) {
		this.style = style;
	}

	/**
	 * @return the level
	 */
	@Column(name = "LEVEL_VAL")
	public String getLevel() {
		return level;
	}

	/**
	 * @param level the level to set
	 */
	public void setLevel(String level) {
		this.level = level;
	}

	/**
	 * @return noticeToUserIds
	 */
	@Transient
	public String getNoticeToUserIds() {
		return noticeToUserIds;
	}

	/**
	 * @param noticeToUserIds
	 */
	public void setNoticeToUserIds(String noticeToUserIds) {
		this.noticeToUserIds = noticeToUserIds;
	}

	/**
	 * @return userIds
	 */
	@Transient
	public List<Long> getUserIds() {
		return userIds;
	}

	/**
	 * @param userIds
	 */
	public void setUserIds(List<Long> userIds) {
		this.userIds = userIds;
	}

	/**
	 * @return readStatus
	 */
	@Transient
	public String getReadStatus() {
		return readStatus;
	}

	/**
	 * @param readStatus
	 */
	public void setReadStatus(String readStatus) {
		this.readStatus = readStatus;
	}

	/**
	 * @return readTime
	 */
	@Transient
	public Date getReadTime() {
		return readTime;
	}

	/**
	 * @param readTime
	 */
	public void setReadTime(Date readTime) {
		this.readTime = readTime;
	}

	
	/**
	 * @return ntuId
	 */
	@Transient
	public Long getNtuId() {
		return ntuId;
	}

	
	/**
	 * @param ntuId
	 */
	public void setNtuId(Long ntuId) {
		this.ntuId = ntuId;
	}

	/**
	 * @return the num
	 */
	@Transient
	public Long getNum() {
		return num;
	}

	/**
	 * @param num the num to set
	 */
	public void setNum(Long num) {
		this.num = num;
	}

	
	/**
	 * @return moduleKey
	 */
	@Column(name = "MODULE_KEY", length = 200)
	public String getModuleKey() {
		return moduleKey;
	}

	
	/**
	 * @param moduleKey
	 */
	public void setModuleKey(String moduleKey) {
		this.moduleKey = moduleKey;
	}

	
	/**
	 * @return moduleDataId
	 */
	@Column(name = "MODULE_DATA_ID", length = 100)
	public String getModuleDataId() {
		return moduleDataId;
	}

	
	/**
	 * @param moduleDataId
	 */
	public void setModuleDataId(String moduleDataId) {
		this.moduleDataId = moduleDataId;
	}

	@Column(name = "MODULE_FIELDS", length = 2000)
	public String getModuleFields() {
		return moduleFields;
	}

	
	public void setModuleFields(String moduleFields) {
		this.moduleFields = moduleFields;
	}

	
}
