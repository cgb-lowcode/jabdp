/*
 * @(#)OnlineUserManager.java
 * 2011-5-17 下午10:38:32
 *
 * Copyright (c) 2018-2028, HangZhou QiYun InfoTech Co.,Ltd. .
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.qyxx.platform.sysmng.accountmng.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import com.qyxx.platform.common.orm.Page;
import com.qyxx.platform.common.orm.PropertyFilter;
import com.qyxx.platform.sysmng.accountmng.dao.OnlineUserDao;
import com.qyxx.platform.sysmng.accountmng.entity.OnlineUser;


/**
 *  在线用户管理
 *  @author gxj
 *  @version 1.0
 *  @since 1.6 2011-5-17 下午10:38:32
 */
@Component
@Transactional
public class OnlineUserManager {
	
	private static final String DEL_HQL_BY_USER_NAME = "delete from OnlineUser where loginUsername = ?";
	
	private static final String COUNT_ONLINEUSER_HQL = "select count(id) from OnlineUser";
	
	private OnlineUserDao onlineUserDao; 
	
	/**
	 * @param onlineUserDao
	 */
	@Autowired
	public void setOnlineUserDao(OnlineUserDao onlineUserDao) {
		this.onlineUserDao = onlineUserDao;
	}
	
	/**
	 * 根据ID获取对象
	 * 
	 * @param id
	 * @return
	 */
	@Transactional(readOnly = true)
	public OnlineUser getOnlineUser(Long id) {
		return onlineUserDao.get(id);
	}
	
	/**
	 * 保存在线用户
	 * 
	 * @param entity
	 */
	public void saveOnlineUser(OnlineUser entity) {
		onlineUserDao.save(entity);
	}
	
	/**
	 * 删除在线用户
	 * 
	 * @param id
	 */
	public void removeOnlineUser(Long id) {
		onlineUserDao.delete(id);
	}
	
	/**
	 * 批量删除在线用户
	 * 
	 * @param id
	 */
	public void removeOnlineUsers(Long[] ids) {
		if(null!=ids) {
			for(Long id : ids) {
				onlineUserDao.delete(id);
			}
		}
	}
	
	/**
	 * 根据过滤条件查询列表
	 * 
	 * @param page
	 * @param filters
	 * @return
	 */
	public Page<OnlineUser> searchOnlineUser(final Page<OnlineUser> page, final List<PropertyFilter> filters) {
		return onlineUserDao.findPage(page, filters);
	}
	
	/**
	 * 根据属性名+属性值查询在线用户列表
	 * 
	 * @param propertyName
	 * @param value
	 * @return
	 */
	public List<OnlineUser> searchOnlineUser(String propertyName, String value) {
		return onlineUserDao.findBy(propertyName, value);
	}
	
	/**
	 * 根据sessionId删除在线用户记录
	 * 
	 * @param sessionId
	 */
	public void removeOnlineUser(String sessionId) {
		OnlineUser ou = onlineUserDao.findUniqueBy("sessionId", sessionId);
		if(ou != null) {
			onlineUserDao.delete(ou);
		}
	}
	
	/**
	 * 根据用户名删除在线用户
	 * 
	 * @param userName
	 */
	public void removeOnlineUserByUserName(String userName) {
		onlineUserDao.batchExecute(DEL_HQL_BY_USER_NAME, userName);
	}
	
	/**
	 * 获取在线用户数
	 * 
	 * @return
	 */
	public Integer getOnlineUserCount() {
		Long uc = onlineUserDao.findUnique(COUNT_ONLINEUSER_HQL);
		return uc.intValue();
	}

}
