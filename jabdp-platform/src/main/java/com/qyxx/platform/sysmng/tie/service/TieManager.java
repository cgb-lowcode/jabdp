package com.qyxx.platform.sysmng.tie.service;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import com.qyxx.platform.common.orm.Page;
import com.qyxx.platform.sysmng.accountmng.entity.User;
import com.qyxx.platform.sysmng.tie.dao.TieDao;
import com.qyxx.platform.sysmng.tie.entity.Tie;

@Component
@Transactional
public class TieManager {
	
	private TieDao tieDao;
	
	private String selectTieById = "SELECT * FROM (SELECT a.ID id,a.CONTENT content,a.PARENT_ID parentId,b.NICKNAME nickname,b.LOGIN_NAME loginName FROM SYS_TIE a LEFT JOIN sys_user b ON a.USER_ID=b.ID WHERE a.ID=:pid) a";
	private String selectTie = "SELECT * FROM (SELECT a.USER_ID userId,a.ID id,a.CONTENT content,a.PARENT_ID parentId,b.NICKNAME nickname,b.LOGIN_NAME loginName,a.TIME time FROM SYS_TIE a LEFT JOIN sys_user b ON a.USER_ID=b.ID WHERE a.MODULE_NAME=:moduleName AND a.RELEVANCE_ID=:relevanceId) a ORDER BY a.ID desc";
	private String selectCount = "SELECT count(*) count FROM SYS_TIE a LEFT JOIN sys_user b ON a.USER_ID=b.ID WHERE a.MODULE_NAME=:moduleName AND a.RELEVANCE_ID=:relevanceId";
	
	@Autowired
	public void setTieDao(TieDao tieDao) {
		this.tieDao = tieDao;
	}

	//保存单条帖子
	public Long saveTie(Tie tie, User user, Date time) {
		tie.setUser(user.getId());
		tie.setTime(new Date());
		tieDao.save(tie);
		return tie.getId();
	}
	
	//删除单条帖子
	public boolean deleteTie(Long id, Long userid) {
		Tie tie = tieDao.findBy("id", id).get(0);
		if(userid != null && userid.equals(tie.getUser())) {
			deleteHuiTie(id);//删除帖子的所有评论回帖
			tieDao.delete(id);
			return true;
		} else {
			return false;
		}
	}
	
	//递归删除帖子的所有评论回帖
	public void deleteHuiTie(Long id) {
		List<Tie> ties = tieDao.findBy("parentId", id);
		if(ties != null) {
			for(Tie tie : ties) {
				deleteHuiTie(tie.getId());
				tieDao.delete(tie.getId());
			}
		}
	}
	
	//分页获取帖子列表
	public List<Map<String, Object>> selectTies(Long relevanceId, String moduleName, int index, int size) {
		Map<String, Object> param = new HashMap<String, Object>();
		param.put("moduleName", moduleName);
		param.put("relevanceId", relevanceId);
		Page<Object> page = new Page<Object>();
		page.setPageNo(index);
		page.setPageSize(size);
		List<Map<String, Object>> tieList = tieDao.findDataMapListBySql(selectTie, param, page);
		if(index == 1) {
			List<Map<String, Object>> m = tieDao.findDataMapListBySql(selectCount, param);
			tieList.add(m.get(0));
		}
		return tieList;
	}
	
	//获取父级帖子列表
	public List<Map<String, Object>> selectParentTies(Long id) {
		List<Map<String, Object>> tieList = new ArrayList<Map<String, Object>>();
		while(true) {
			Map<String, Object> map = selectTieById(id);
			tieList.add(map);
			if(map.get("parentId") == null) {
				break;
			} else {
				id = ((Number)map.get("parentId")).longValue();
			}
		}
		return tieList;
	}
	
	//根据id获取单条帖子
	public Map<String, Object> selectTieById(Long id) {
		Map<String, Object> param = new HashMap<String, Object>();
		param.put("pid", id);
		List<Map<String, Object>> tieList = tieDao.findDataMapListBySql(selectTieById, param);
		return tieList.get(0);
	}

}
