/*
 * @(#)SmsFactory.java
 * 2014-1-7 下午11:49:51
 * 
 *
 * Copyright (c) 2018-2028, HangZhou QiYun InfoTech Co.,Ltd. .
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.qyxx.platform.sysmng.msg.service;

import com.qyxx.platform.inf.NeiAdapterFactory;
import com.qyxx.platform.inf.adapter.AdapterException;
import com.qyxx.platform.inf.adapter.AdapterProperties;
import com.qyxx.platform.inf.adapter.INeiAdapter;
import com.qyxx.platform.inf.adapter.NeiTask;
import com.qyxx.platform.sysmng.dictmng.web.DefinitionCache;


/**
 *  短信工厂类，负责获取服务短信接口
 *  @author gxj
 *  @version 1.0 2014-1-7 下午11:49:51
 *  @since jdk1.6 
 */

public class SmsFactory {
	
	public static final String DICT_SMS = "SYS_SMS_DICT";//数据字典类型--短信
	
	public static final String SERVICE_SMS_CODE = "serviceSMS.code";//服务短信接口编码
	
	public static final String SERVICE_SMS_CLASS = "serviceSMS.class";//服务短信接口类
	
	public static final String SERVICE_SMS_URL = "serviceSMS.url";//服务短信URL地址
	
	public static final String SERVICE_SMS_NAME = "serviceSMS.name";//服务短信名称
	
	public static final String SERVICE_SMS_PASSWORD = "serviceSMS.password";//服务短信密码
	
	public static final String SERVICE_SMS_PRODUCTID = "serviceSMS.productid";//产品ID

	private static AdapterProperties ap = new AdapterProperties(); //短信接口参数

	static {
		initServiceSms();
	}
	
	/**
	 * 发送短信
	 * 
	 * @param target
	 * @param content
	 * @param serviceSms
	 * @return
	 * @throws AdapterException 
	 */
	public static NeiTask sendSms(String target, String content, String serviceSms) throws AdapterException {
		INeiAdapter na = NeiAdapterFactory.getInstance().getNeiApater(ap);
		NeiTask task = new NeiTask();
		task.setTargetCode(target);
		task.setCmd(content);
		na.handle(task);
		return task;
	}

	
	/**
	 * 默认初始化10个服务短信
	 */
	public static void initServiceSms() {
		ap.setAdapterCode(DefinitionCache.getEntityByValue(DICT_SMS, SERVICE_SMS_CODE).getDisplayName());
		ap.setAdapterClass(DefinitionCache.getEntityByValue(DICT_SMS, SERVICE_SMS_CLASS).getDisplayName());
		ap.setIpAddress(DefinitionCache.getEntityByValue(DICT_SMS, SERVICE_SMS_URL).getDisplayName());
		ap.setUserName(DefinitionCache.getEntityByValue(DICT_SMS, SERVICE_SMS_NAME).getDisplayName());
		ap.setPassword(DefinitionCache.getEntityByValue(DICT_SMS, SERVICE_SMS_PASSWORD).getDisplayName());
		ap.setParamValue("productid",DefinitionCache.getEntityByValue(DICT_SMS, SERVICE_SMS_PRODUCTID).getDisplayName());
	}
	
	/**
	 * 重置短信设置
	 */
	public static void resetServiceSms() {
		NeiAdapterFactory.getInstance().removeNeiApater(ap);
		DefinitionCache.removeDefinition(DICT_SMS);
		initServiceSms();
	}
}
