/*
 * @(#)Authority.java
 * 2011-5-9 下午09:05:17
 *
 * Copyright (c) 2018-2028, HangZhou QiYun InfoTech Co.,Ltd. .
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.qyxx.platform.sysmng.accountmng.entity;


import java.io.Serializable;
import java.util.List;
import java.util.Map;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OrderBy;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.apache.commons.lang.builder.ToStringBuilder;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.qyxx.platform.common.module.entity.BaseEntity;

/**
 *  权限
 *  
 *  @author gxj
 *  @version 1.0
 *  @since 1.6 2011-5-9 下午09:05:17
 */
@Entity
@Table(name = "SYS_RESOURCE")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
public class Authority extends BaseEntity implements Serializable {

	/**
	 * long
	 */
	private static final long serialVersionUID = 5420443136907028418L;

	/**
	 * SpringSecurity中默认的角色/授权名前缀.
	 */
	public static final String AUTHORITY_PREFIX = "ROLE_";
	
	public static final String RESOURCE_PREFIX = "RES_";
	
	public static final String TYPE_APP = "app";
	
   
	private Long id;
	/**
	 * 资源标识
	 */
	private String name;
	private String resourceName;
    private String resourceType;
	private Authority authority;
	private String resourceIcon;
	private String openType;
	private String resourceStatus;
	private String resourceUrl;
    private String resourceLevel;
    private String remark;
    private Long resourceNo;
    private String logRule;
    private Boolean appShow =false;


	private Map<String,AuthorityLocale> localeMap = Maps.newHashMap();
    private List<Role> roleList = Lists.newArrayList();
    
	public Authority() {
	}
	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO, generator="SEQ_SYS_RESOURCE_ID")
	@SequenceGenerator(name="SEQ_SYS_RESOURCE_ID", sequenceName="SEQ_SYS_RESOURCE_ID")
	public Long getId() {
		return id;
	}
	
	public void setId(Long id) {
		this.id = id;
	}

	@Column(name="RESOURCE_SIGN", nullable = false, unique = true)
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	
	public String getResourceUrl() {
		return resourceUrl;
	}

	
	public void setResourceUrl(String resourceUrl) {
		this.resourceUrl = resourceUrl;
	}

	
	public String getResourceName() {
		return resourceName;
	}

	
	public void setResourceName(String resourceName) {
		this.resourceName = resourceName;
	}

	
	public String getResourceType() {
		return resourceType;
	}

	
	public void setResourceType(String resourceType) {
		this.resourceType = resourceType;
	}

	@ManyToOne
	@JoinColumn(name = "PARENT_ID")
	@JsonIgnore
	public Authority getAuthority() {
		return authority;
	}

	
	public void setAuthority(Authority authority) {
		this.authority = authority;
	}

	
	public String getResourceIcon() {
		return resourceIcon;
	}

	
	public void setResourceIcon(String resourceIcon) {
		this.resourceIcon = resourceIcon;
	}

	
	public String getOpenType() {
		return openType;
	}

	
	public void setOpenType(String openType) {
		this.openType = openType;
	}

	
	public String getResourceStatus() {
		return resourceStatus;
	}

	
	public void setResourceStatus(String resourceStatus) {
		this.resourceStatus = resourceStatus;
	}

	
	public String getResourceLevel() {
		return resourceLevel;
	}

	
	public void setResourceLevel(String resourceLevel) {
		this.resourceLevel = resourceLevel;
	}

	
	public String getRemark() {
		return remark;
	}

	
	public void setRemark(String remark) {
		this.remark = remark;
	}

	
	public Long getResourceNo() {
		return resourceNo;
	}

	
	public void setResourceNo(Long resourceNo) {
		this.resourceNo = resourceNo;
	}

	
	
	/**
	 * @return logRule
	 */
	public String getLogRule() {
		return logRule;
	}

	
	/**
	 * @param logRule
	 */
	public void setLogRule(String logRule) {
		this.logRule = logRule;
	}

	public static String getAuthorityPrefix() {
		return AUTHORITY_PREFIX;
	}
       
	@ManyToMany
	@JoinTable(name = "SYS_RESOURCE_TO_ROLE", joinColumns = { @JoinColumn(name = "RESOURCE_ID") }, inverseJoinColumns = { @JoinColumn(name = "ROLE_ID") })
	@Fetch(FetchMode.SUBSELECT)
	@OrderBy("id")
	@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
	@JsonIgnore
	public List<Role> getRoleList() {
		return roleList;
	}

	
	public void setRoleList(List<Role> roleList) {
		this.roleList = roleList;
	}

	@Transient
	public String getPrefixedName() {
		return AUTHORITY_PREFIX + name;
	}
	
	@Column(name="APP_SHOW", length=1)
	public Boolean getAppShow() {
		return appShow;
	}

	public void setAppShow(Boolean appShow) {
		this.appShow = appShow;
	}
	 
	
	@Override
	public String toString() {
		return ToStringBuilder.reflectionToString(this);
	}

	@Transient
	public Map<String, AuthorityLocale> getLocaleMap() {
		return localeMap;
	}

	
	public void setLocaleMap(Map<String, AuthorityLocale> localeMap) {
		this.localeMap = localeMap;
	}

	





}
