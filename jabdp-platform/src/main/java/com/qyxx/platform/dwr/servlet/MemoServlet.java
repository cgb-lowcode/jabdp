package com.qyxx.platform.dwr.servlet;

import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

import javax.servlet.ServletException;

import org.directwebremoting.servlet.DwrServlet;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.qyxx.platform.common.utils.spring.SpringContextHolder;
import com.qyxx.platform.sysmng.memo.service.MemoManager;

public class MemoServlet extends DwrServlet {
	
	private static Logger logger = LoggerFactory.getLogger(MemoServlet.class);
	
	@Override
	public void init() throws ServletException {
		super.init();
		// 创建线程池
		ScheduledExecutorService service = Executors.newScheduledThreadPool(10);
        // 从现在开始10秒钟之后，每隔1分钟执行一次，备忘录提醒状态检查
		service.scheduleAtFixedRate(
	        new Runnable() {
				@Override
				public void run() {
					// TODO Auto-generated method stub
					try {
						MemoManager memoManager = SpringContextHolder.getBean("memoManager");
						memoManager.updataMemoToUserStatusByNow();
					} catch (Exception e) {
						// TODO: handle exception
						logger.error("备忘录推送异常", e);
					}
				}
			}, 10, 60, TimeUnit.SECONDS);
		logger.info("启动备忘录推送");
	}

}
