package com.qyxx.platform.dwr.session;

import javax.servlet.http.HttpSession;

import org.directwebremoting.ScriptSession;
import org.directwebremoting.WebContextFactory;
import org.directwebremoting.event.ScriptSessionEvent;
import org.directwebremoting.event.ScriptSessionListener;
import org.directwebremoting.impl.DefaultScriptSessionManager;

import com.qyxx.platform.sysmng.accountmng.entity.User;
import com.qyxx.platform.sysmng.utils.Constants;

public class ScriptSessionConf extends DefaultScriptSessionManager {
	public ScriptSessionConf() {
		addScriptSessionListener(new ScriptSessionListener() {
		    
		    public void sessionDestroyed(ScriptSessionEvent event) {}
		    
		    public void sessionCreated(ScriptSessionEvent event) {
		    	ScriptSession scriptSession = event.getSession();
		    	HttpSession httpSession = WebContextFactory.get().getSession();
		    	User user = (User)WebContextFactory.get().getSession().getAttribute(Constants.USER_SESSION_ID);
		     //如果当前用户已经退出系统，然后销毁这个scriptsession

//		     if(user==null)
//		     {
//		      scriptSession.invalidate();  
//		      httpSession.invalidate();  
//		      return;
//		     }
//		     String ssId = (String) httpSession.getAttribute(SS_ID);//查找SS_ID
//		     if(ssId!=null)
//		     {
//		      //说明已经存在旧的scriptSession.注销这个旧的scriptSession
//		      DefaultScriptSession oldScriptSession = sessionMap.get(ssId);
//		      if(oldScriptSession!=null)
//		      {
//		       invalidate(oldScriptSession);
//		      }
//		     }
//		     httpSession.setAttribute(SS_ID, scriptSession.getId());
//		     scriptSession.setAttribute(Constant.LG_USER_ID, user.getId());//绑定用户ID到ScriptSession上
		    }
		   });
	}
	
}
