(function($){
	$.fn.span_tip=function(setting){
			var option=$.extend({text:'请输入...',hide:false},setting);
			return this.each(function(){
				var self=$(this);
				if(self.parent(".tip_out_wrap").length == 0) {
					self.wrap("<span class='tip_out_wrap' style='width:"+self.outerWidth()+"px;height:"+self.outerHeight()+"px'></span>");
				}
				var tips = self.nextAll(".span_tip");
				if(option.hide) {
					tips.hide();
				} else {
					if(tips.length) {
						tips.html(option.text);
						tips.show();
					} else {
						var tip=$("<span class='span_tip'></span>");
						var namecss=self.attr('name')?self.attr('name')+"_tip_style":"custom_tips_style";
						tip.addClass(namecss).html(option.text).appendTo(self.parents('.tip_out_wrap'));
						self.blur(function(){
							if(self.val()=="")
							{
								tip.show();
							}
						});
						self.focus(function(){
							tip.hide();
						});
						self.change(function(){
							tip.hide();
						});
						tip.bind('click',function(){
							$(this).hide();
							self.focus();
						});
					}
				}
			});
		};
})(jQuery);
