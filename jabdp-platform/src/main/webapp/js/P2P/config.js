var version = '20140414';

var versionJS = '20140414';

versionJS = versionJS === '' ? version : versionJS;

seajs.config({
  alias: {
    'asyncSlider': 'lib/asyncSlider/asyncSlider.js',
    'city': 'lib/city/city.js',
    'new-city': 'lib/city/new-city.js',
    'counter': 'lib/counter/jquery.counter.js',
    'easing': 'lib/easing/easing.js',
    'flash': 'lib/flash/jquery.flash.js',
    'handlebars': 'lib/handlebars/handlebars.js',
    'highcharts': 'lib/highcharts/highcharts.js',
    'jquery': 'lib/jquery/jquery.js',
    'queue': 'lib/plupload/jquery.plupload.queue',
    'simplePagination': 'lib/simplePagination/simplePagination.js',
    'validate': 'lib/validation/jquery.validate.js',
    'mailSuggest':'lib/mailSuggest/mailSuggest.js',
    'rsa': 'rsa/index.js',

    'base': 'arale/base/base.js',
    'class': 'arale/class/class.js',
    'events': 'arale/events/events',
    'widget': 'arale/widget/widget.js',
    'popup': 'arale/popup/popup.js',
    'tip': 'arale/tip/tip.js',
    'overlay': 'arale/overlay/overlay.js',
    'mask': 'arale/overlay/mask.js',
    'iframe-shim': 'arale/shim/iframe-shim.js',
    'position': 'arale/position/position.js',
    'dialog': 'arale/dialog/dialog.js',
    'confirmbox': 'arale/dialog/confirmbox.js',
    'templatable': 'arale/templatable/templatable.js',
    'calendar': 'arale/calendar/calendar.js',
    'moment': 'arale/moment/moment.js',
    'sticky':'arale/sticky/sticky.js',

    'ui-counter': 'lib/counter/jquery.counter-analog.css',
    'ui-poptip': 'alice/poptip/poptip.css'
  },
  
  map: [
     [/^(.*(widgets|components|pages|common|page|rsa|protocol).*\.js)(.*)$/i, '$1?v=' + versionJS]
  ]
  
});
