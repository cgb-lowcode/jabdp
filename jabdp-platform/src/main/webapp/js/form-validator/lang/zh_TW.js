/**
 * jQuery Form Validator
 * ------------------------------------------
 *
 * Spanish language package
 *
 * @website http://formvalidator.net/
 * @license MIT
 */
!(function (root, factory) {
  if (typeof define === 'function' && define.amd) {
    // AMD. Register as an anonymous module unless amdModuleId is set
    define(["jquery"], function (a0) {
      return (factory(a0));
    });
  } else if (typeof exports === 'object') {
    // Node. Does not work with strict CommonJS, but
    // only CommonJS-like environments that support module.exports,
    // like Node.
    module.exports = factory(require("jquery"));
  } else {
    factory(jQuery);
  }
}(this, function (jQuery) { 
(function($, window) {

  'use strict';


	  	$.formUtils.LANG = {
	  			  errorTitle:"表單提交失敗！",
	  		      requiredField:"這是必填字段",
	  		      requiredFields:"你沒有回答所有必填字段",
	  		      badTime:"你沒有給出正確的時間",
	  		      badEmail:"你沒有給出正確的電子郵件地址",
	  		      badTelephone:"你沒有給出正確的電話號碼",
	  		      badSecurityAnswer:"你沒有給出正確答案的安全問題",
	  		      badDate:"你沒有給出正確的日期",
	  		      lengthBadStart:"輸入值必須在",
	  		      lengthBadEnd:"字符",
	  		      lengthTooLongStart:"輸入值長於",
	  		      lengthTooShortStart:"輸入值短於",
	  		      notConfirm:"輸入值無法確認",
	  		      badDomain:"域名不正確",
	  		      badUrl:"輸入值不正確的URL",
	  		      badCustomVal:"輸入值不正確",
	  			  andSpaces:"和空格",
	  		      badInt:"輸入的值格式不正確，必須為整數",
	  		      badSecurityNumber:"你的社會保險號碼不正確",
	  		      badUKVatAnswer:"英國增值稅號不正確",
	  		      badUKNin:"不正確的英國NIN",
	  		      badUKUtr:"不正確的英國編號",
	  		      badStrength:"密碼不夠強",
	  		      badNumberOfSelectedOptionsStart:"你必須至少選擇",
	  		      badNumberOfSelectedOptionsEnd:"答案",
	  		      badAlphaNumeric:"輸入值只能包含字母數字字符",
	  		      badAlphaNumericExtra:"和",
	  		      wrongFileSize:"您嘗試上傳的文件太大（最多％s）",
	  		      wrongFileType:"只允許類型為％s的文件",
	  		      groupCheckedRangeStart:"請選擇",
	  		      groupCheckedTooFewStart:"請至少選擇",
	  		      groupCheckedTooManyStart:"請選擇最大",
	  		      groupCheckedEnd:"項目",
	  		      badCreditCard:"信用卡號不正確",
	  		      badCVV:"CVV號碼不正確",
	  		      wrongFileDim:"圖像尺寸不正確",
	  		      imageTooTall:"圖像不能高於",
	  		      imageTooWide:"圖像不能寬於",
	  		      imageTooSmall:"圖像太小",
	  		      min:"最小值",
	  		      max:"最大值",
	  		      imageRatioNotAccepted:"圖像比例不被接受",
	  		      badBrazilTelephoneAnswer:"輸入的電話號碼無效",
	  		      badBrazilCEPAnswer:"輸入的CEP無效",
	  		      badBrazilCPFAnswer:"CPF輸入無效",
	  		      badPlPesel:"輸入的PESEL無效",
	  		      badPlNip:"輸入的NIP無效",
	  		      badPlRegon:"輸入的REGON無效",
	  		      badreCaptcha:"請確認你不是機器人",
	  			  passwordComplexityStart:'密碼必須至少包含',
	  		       passwordComplexitySeparator:',',
	  		       passwordComplexityUppercaseInfo:'大寫字母',
	  		       passwordComplexityLowercaseInfo:'小寫字母',
	  		       passwordComplexitySpecialCharsInfo:'特殊字符（S）',
	  		       passwordComplexityNumericCharsInfo:'數字字符（多個）',
	  		       passwordComplexityEnd:'.'
		};


})(jQuery, window);

}));
