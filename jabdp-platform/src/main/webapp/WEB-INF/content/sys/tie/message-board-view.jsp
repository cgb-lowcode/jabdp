<%@ page contentType="text/html;charset=UTF-8"%>
<%@ include file="/common/taglibs.jsp"%>
<script type="text/javascript">
	var newsId = ${param.id};
	var moduleKey = "${param.moduleKey}";
	var pageindex = 1;
	var pageSize = 5;
	var oneTieList = [];//跟当前评论有关的父贴列表
	
	//加载留言贴列表
	function initTieList() {
		$.ajax({
			url:'${ctx}/sys/tie/tie!list.action',
			data:{
				relevanceId:newsId,
				moduleName:moduleKey,
				pageindex:pageindex,
				pageSize:pageSize
				},
			success:function(data) {
				var num;
				if(data.length < 5) {
					$(".tie-more").hide();
				}
				if(data[data.length-1].count!==null) {
					num = data.length - 1;
					$('i.js-actCount').text(data[num].count);
				} else {
					num = data.length;
				}
				console.log(data);
				for(var i = 0; i < num; i++) {
					afterInsertTie(data[i]);
				}
				pageindex++;
			}
		});
	}
	
	//向后插入单个帖子内容
	function afterInsertTie(option) {
		//获取单个帖子的html字符串
		var tieStr = "";
		if(option.userId==${session.USER.id}) {
			tieStr = getTieHtmlStr(option,true);
		} else {
			tieStr = getTieHtmlStr(option,false);
		}
		$('#tie-bodys').append(tieStr);
		_tieBody(option.parentId,option.content,option.id);//插入帖子内容
	}
	
	//删除帖子
	function _shanTie(id) {
		$.messager.confirm('提示','确定删除该评论',function(r){
			if(r) {
				$.ajax({
					url:'${ctx}/sys/tie/tie!delete.action',
					data:{
						tieId:id
						},
					success:function(data) {
						if(data) {
							shanTiepinglun(id);
							$(".tie-body[jid="+id+"]").parents(".tie-reply").remove();
							var js_actCount = $('i.js-actCount');//留言条数减一
							js_actCount.text(parseInt(js_actCount.text())-1);
						} else {
							$.messager.alert(
								'<s:text name="system.javascript.alertinfo.title"/>',
								'只能删除自己的评论',
								'info');
						}
					}
				});
			}
		});
	}
	
	//递归删除帖子评论
	function shanTiepinglun(id) {
		$(".tie-body[pid="+id+"]").each(function() {
			shanTiepinglun($(this).attr('jid'));
			$(this).parents(".tie-reply").remove();
			var js_actCount = $('i.js-actCount');//留言条数减一
			js_actCount.text(parseInt(js_actCount.text())-1);
		});
	}
	
	//获取的html字符串
	function getTieBodyHtmlStr(parentId,count) {
		if(parentId != null) {
			var str = "";
			for(var i in oneTieList) {
				if(oneTieList[i].id==parentId) {
					var opt = oneTieList[i];
					var ss = "的原帖";
					if(opt.parentId) {
						ss = '评论';
					}
					str = '<div class="tie-frontOfFloor layerBgcolor">'
						+ getTieBodyHtmlStr(opt.parentId, count+1)//用递归插入父级帖
				        + '<div class="tie-floorInner"><span class="tie-floor-author layerUserName">员工姓名:'
				        + opt.nickname + '&nbsp;&nbsp;[账户名称:' + opt.loginName + '] '
				        + ss
				        + ':</span><span class="tie-floor-index">'
				        + count
				        + '</span></div>'
				        + '<p class="tie-floor-content layerFontColor">'
				        + opt.content
				        + '</p></div>';
					break;
				}
			}
		    return str;
		} else {
			return "";
		}
	}
	
	//处理回复型帖子的层级
	function _tieBody(pid,content,id) {
		if(pid != null) {
			$.ajax({
				url:'${ctx}/sys/tie/tie!view.action',
				data:{
					  parentId:pid,//如果是回复贴，关联被评论父贴的ID，否则留null
					},
				success:function(data) {
					//向前插入单个帖子内容
					oneTieList = data;
					var str = getTieBodyHtmlStr(pid,1);
					$(".tie-body[jid="+id+"]").append(str + '<p class="tie-floor-content">' + content + '</p>');
				}
			});
		} else {
			$(".tie-body[jid="+id+"]").append('<div class="tie-oneLayer">' + content + '</div>');
		}
	}
	
	//点击回帖
	function _huiTie(id) {
		$('#tie-dialogue').window('open').data("parentId",id);
	}
	
	//获取单个帖子的html字符串
	function getTieHtmlStr(option, deleteable) {
		var deletestr = "";
		var pidstr = "";
		if(deleteable) {
			deletestr = '<li><a rel="nofollow" href="javascript:void(0);" onclick="_shanTie('
			    + option.id
			    + ')">删除</a></li>';
		}
		if(option.parentId) {
			pidstr = " pid=" + option.parentId;
		}
		var str = '<div class="tie-reply" ><div class="tie-inner">'
		    + '<span class="tie-sum-author"><span class="tie-from layerUserName">员工姓名:'
		    + option.nickname
		    + '</span><span>&nbsp;&nbsp;[账户名称:'
		    + option.loginName
		    + ']</span></span><span class="tie-postTime">'
		    + option.time
		    + '</span>'
		    + '<div jid='
			+ option.id
			+ pidstr
			+ ' class="tie-body"></div>'
		    + '<ul class="tie-operations">'
		    + deletestr
		    + '<li><a rel="nofollow" href="javascript:void(0);" onclick="_huiTie('
		    + option.id
		    + ')">回复</a></li>'
		    + '</ul></div></div>';
		 return str;
	}
	
	//向前插入单个帖子内容
	function beforInsertTie(option) {
		//获取单个帖子的html字符串
		var tieStr = getTieHtmlStr(option,true);
		var tie_body = $('.tie-reply:first');
		if(tie_body.length>0) {
			tie_body.before(tieStr);
		} else {
			$('#tie-bodys').append(tieStr);
		}
		_tieBody($('#tie-dialogue').data("parentId"),option.content,option.id);//插入帖子内容
		var js_actCount = $('i.js-actCount');//留言条数加一
		js_actCount.text(parseInt(js_actCount.text())+1);
	}
	
	//提交发帖
	function doSubmitTie() {
		var content = $('#tie-dialogue textarea').val();
		var parentId = $('#tie-dialogue').data("parentId");
		if(content){
			$.ajax({
				url:'${ctx}/sys/tie/tie!save.action',
				data:{
					  content:content,//内容
					  parentId:parentId,//如果是回复贴，关联被评论父贴的ID，否则留null
					  relevanceId:newsId,//关联留言贴ID
					  moduleName:moduleKey//关联模块名
					},
				success:function(data) {
					$('#tie-dialogue').window('close');
					//向前插入单个帖子内容
					var option = {
						content:content,//内容
						parentId:parentId,//如果是回复贴，关联被评论父贴的ID，否则留null
					}
					option = $.extend(option,data);
					beforInsertTie(option);
				}
			});
		} else {
			$.messager.alert(
				'<s:text name="system.javascript.alertinfo.title"/>',
				'评论内容不能为空',
				'info');
		}
	}
	
	//初始化输入帖子内容对话窗
	function initTieDialogue() {
		var w = {
			closed:true,
			onBeforeOpen:function() {
				$(this).removeData("parentId");
				$(this).find('textarea').val("");
			},
			onResize:function(width, height) {
				$(this).find('textarea').css({width:width-32,height:height-88});
			}
		};
		var tie_dialogue = $('#tie-dialogue').window(w);
		/*tie_dialogue.find('textarea').click(function(){
			if($(this).val() == '请输入评论内容' || $(this).val() == null){
				$(this).val("");
			}
		}).blur(function(){
			if($(this).val() == '') {
				$(this).val("请输入评论内容");
			}
		});*/
		tie_dialogue.find('.easyui-layout').layout();
		$('.easyui-linkbutton').linkbutton();
	}
	
	//初始化
	$(function() {
		//加载留言贴列表
		initTieList();
		//初始化输入帖子内容对话窗
		initTieDialogue();
	});
</script>
<!-- 跟贴内容css -->
<style type="text/css">
.tie-reply {
  text-align: left;
  padding: 20px 0 20px 0;
  border-bottom: 1px dotted #ddd;
  position: relative;
  overflow: hidden;
  background-color: #fff;
}
.tie-inner {
  zoom: 1;
  overflow: hidden;
  padding-left: 30px;
}
.tie-sum-author {
  float: left;
  display: block;
  color: #006b9c;
  height: 18px;
  line-height: 19px;
  zoom: 1;
}
.layerUserName {
  color: #006b9c;
}
.tie-postTime {
  color: #888;
  float: right;
  display: block;
  line-height: 19px;
  padding-right: 10px;
}
.tie-operations {
  color: #1e50a2;
  float: right;
  list-style: none;
  padding-right: 10px;
  height: 14px;
  line-height: 14px;
  vertical-align: middle;
  overflow: hidden;
}
.tie-operations li {
  float: left;
  padding-left: 10px;
}
.tie-operations a {
  color: #006b9c;
  vertical-align: bottom;
}
.tie-floor-content {
  margin: 8px 3px;
  line-height: 20px;
  clear: both;
  font-size: 14px;
}
.tie-body {
  font-size: 14px;
  padding: 15px 10px 10px 0;
  clear: both;
}
.tie-oneLayer {
  line-height: 21px;
  margin-bottom: 3px;
  zoom: 1;
  word-wrap: break-word;
}
.tie-frontOfFloor {
  padding: 3px;
  border: 1px solid #aaa;
  overflow: hidden;
  margin-bottom: 5px;
  word-wrap: break-word;
  zoom: 1;
}
.layerBgcolor {
  background: #ffe;
}
.tie-floorInner {
  margin: 0 3px;
  overflow: hidden;
  line-height: 20px;
  height: 20px;
  font-size: 12px;
  clear: both;
  zoom: 1;
  padding-top: 7px;
}
.tie-floor-author {
  float: left;
}
.tie-floor-index {
  color: #666;
  float: right;
  margin-top: -5px;
  margin-right: 4px;
}
.tie-floor-content {
  margin: 8px 3px;
  line-height: 20px;
  clear: both;
  font-size: 14px;
}
</style>

<!-- 跟帖模块css -->
<style type="text/css">
.tie-show {
  display:block;
  text-align: left;
  margin-top: 5px;
  padding-top: 10px;
  border-top: 1px solid #d8d8d8;
}
.tie-show a {
  color: #3c6496;
}
.tie-show .tie-titlebar {
  height: 30px;
  line-height: 30px;
  clear: both;
  overflow: hidden;
  text-align: left;
  font-size: 14px;
  padding-bottom: 20px;
}
.tie-show .tie-titlebar strong {
  float: left;
  font-size: 16px;
  background: ;
  width: 31px;
  height: 31px;
}
.j-tie-header {
  float: left;
  margin: 0 30px 0 10px;
  font-size: 14px;
}
.j-tie-header span {
  border-bottom: 3px solid #dd4232;
  text-decoration: none;
  color: #444;
  font-size: 18px;
  padding-bottom: 5px;
}
.j-tie-header i {
  font-style: normal;
  color: red;
}
.tie-titlebar .tie-info {
  float: right;
  font-size: 12px;
}
.tie-info .tie-actCount {
  font-size: 14px;
  margin-right: 50px;
  position: relative;
  top: 7px;
}
.tie-info .tie-alert {
  font-size: 14px;
  padding-right: 80px;
  position: relative;
  top: 10px;
  color:red;
}
.tie-show .tie-reply {
  overflow: visible;
}
.tie-show .tie-last {
  border-bottom-width: 0;
}
.tie-more {
  display: block;
  height: 36px;
  cursor: pointer;
  line-height: 36px;
  text-align: center;
  background-color: #eef6ff;
  border: 1px solid #c9d5e2;
  margin-top: 20px;
  text-decoration: none;
}
</style>
<!-- 跟帖模块html -->
<div class="tie-show">
  <!-- 跟帖头部 -->
  <div class="tie-titlebar">
    <strong></strong>
    <span class="j-tie-header"><span>评论</span>(&nbsp;<i class="js-actCount">0</i>&nbsp;个)</span>
    <span class="tie-info">
      <a href="javascript:;" class="tie-actCount" plain="true" onclick="$('#tie-dialogue').window('open');">发表评论</a>
      <span class="tie-alert">*发表评论</span>
    </span>
  </div>
  <!-- 跟帖主体 -->
  <div id='tie-bodys' class="j-tie-body js-tie-data active"></div>
  <a class="tie-more" href="javascript:void(0);" onclick="initTieList();">查看更多评论<em>&gt;&gt;</em></a>
</div>
<!-- 输入帖子内容对话窗 -->
<div id="tie-dialogue" title='&nbsp;&nbsp;内容' style="width:407px;height:216px;padding:5px;">
  <div class="easyui-layout" fit="true">
	<div region='center' border="false" style="background:#fff;border:1px solid #ccc;">
	  <textarea style='width:375px;height:128px' style='color:#999' placeholder="请输入评论内容"></textarea>
	</div>
	<div region='south' border="false" style="height:35px;padding-top:7px;text-align:right;">
	  <a class="easyui-linkbutton" href="javascript:void(0);" onclick="doSubmitTie();">提交</a>
	  <a class="easyui-linkbutton" href="javascript:void(0);" onclick="$('#tie-dialogue').window('close');">取消</a>
	</div>
  </div>
</div>