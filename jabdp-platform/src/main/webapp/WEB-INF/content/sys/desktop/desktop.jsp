<%@ page contentType="text/html;charset=UTF-8"%>
<%@ include file="/common/taglibs.jsp"%>
<!DOCTYPE html>
<html>
<head>
	<title><s:text name="system.index.title"/></title>
	<%@ include file="/common/meta-gs.jsp" %>
	<%-- <link href="${ctx}/js/easyui/${themeColor}/panel.css" rel="stylesheet" type="text/css"/>--%>
    <script type="text/javascript" src="${ctx}/js/easyui-1.4/easyui.xpPanel.js"></script>
            <script type="text/javascript">
            var _userList={};
           
            function getOption() {
                return {
                    width : 'auto',
                    height : 'auto',
                    nowrap : false,
                    striped : true,
                    fit : true,
                    url :'${ctx}/sys/desktop/desktop!queryList.action',
                    sortName : 'createTime',
                    sortOrder : 'desc',
                    frozenColumns : [
                        [
                            {
                                field : 'ck',
                                checkbox : true
                            }
                        ]
                    ],
                    columns : [
                        [

	                                      /*   {
	                                            field : 'id',
	                                            title : '<s:text name="编号"/>',
	                                            width : 80,
	                                            sortable : true
	                                        } , */
	                                        {
	                                            field : 'title',
	                                            title : '<s:text name="system.sysmng.desktop.modelTitle.title"/>',
	                                            width : 80,
	                                            sortable : true
	                                        } ,
	                                        {
	                                            field : 'keyworlds',
	                                            title : '<s:text name="system.sysmng.desktop.key.title"/>',
	                                            width : 80,
	                                            sortable : true
	                                        } ,
	                                        {
	                                            field : 'type',
	                                            title : '<s:text name="system.sysmng.desktop.desktopType.title"/>',
	                                            width : 80,
	                                            sortable : true,
	                                            formatter:function(value,rowData,rowIndex){
	                                            	switch(value){
	                                            	case "toDoList":return "待办事宜";break;
	                                            	case "datetime":return "时间类型";break;
	                                            	case "weather":return "天气类型";break;
	                                            	case "datagrid":return "表格类型";break;
	                                            	case "picTextList":return "图文类型";break;
	                                            	case "statistic2":return "报表类型2";break;
	                                            	case "statistic1":return "报表类型1";break;
	                                            	case "picList":return "图片类型";break;
	                                            	case "textList":return "文字列表";break;
	                                            	case "iframe":return "iframe类型";break;
	                                            	case "textSlides":return "文字滚动";break;
	                                            	}
	                                            }
	                                        } ,
	                                        {
	                                            field : 'layoutType',
	                                            title : '<s:text name="system.sysmng.desktop.layoutType.title"/>',
	                                            width : 80,
	                                            sortable : true
	                                        } ,
	                                        
	                                        {
	                                            field : 'layoutVal',
	                                            title : '<s:text name="system.sysmng.desktop.layoutVal.title"/>',
	                                            width : 80,
	                                            sortable : true
	                                        }  /*,
	                                        {
	                                            field : 'level',
	                                            title : '<s:text name="system.sysmng.desktop.desktopLevel.title"/>',
	                                            width : 80,
	                                            sortable : true
	                                        } */ ,
	                                        {
	                                            field : 'displayNum',
	                                            title : '<s:text name="system.sysmng.desktop.displayNum.title"/>',
	                                            width : 80,
	                                            sortable : true
	                                        } ,
	                                        {
	                                            field : 'moduleUrl',
	                                            title : '<s:text name="system.sysmng.desktop.moduleUrl.title"/>',
	                                            width : 80,
	                                            sortable : true
	                                        } ,
                                            {
	                                            field : 'dataUrl',
	                                            title : '<s:text name="system.sysmng.desktop.dataUrl.title"/>',
	                                            width : 80,
	                                            sortable : true
	                                        } ,
	                                        {
								                field : 'sourceType',
								                title : '<s:text name="system.sysmng.desktop.sourceType.title"/>',
								                width : 80,
								                sortable : true ,
								                formatter:function(value,rowData,rowIndex){
								                	if(value=="fixed"){
								                		return "固定值";
								                	}else if(value=="sql"){
								                		return "sql语句";
								                	}else if(value=="proc"){
								                		return "存储过程";
								                	}else if(value=="java"){
								                		return "java代码";
								                	}else {
								                		return value;
								                	}
								                		
								                } 
								            }/*  ,
								            {
									            field : 'source',
									            title : '<s:text name="内容"/>',
									            width : 80,
									            sortable : true
									        }  */,
									        {
									            field : 'height',
									            title : '<s:text name="system.sysmng.desktop.height.title"/>',
									            width : 80,
									            sortable : true
									        } ,
									        {
									            field : 'fontNum',
									            title : '<s:text name="system.sysmng.desktop.fontNum.title"/>',
									            width : 80,
									            sortable : true
									        } ,
	                                        {
	                                            field : 'createUser',
	                                            title : '<s:text name="system.sysmng.desktop.createUser.title"/>',
	                                            width : 80,
	                                            sortable : true,
	                                            formatter:function(value, rowData, rowIndex){
                                                    /*var uObj=_userList[value];
                                                    if(uObj){
                                                        return uObj.realName;
                                                    }else{
                                                        return value;
                                                    }*/
                                                    return rowData["createUserCaption"];
                                                 }
	                                        } ,
	                                        {
	                                            field : 'createTime',
	                                            title : '<s:text name="system.sysmng.desktop.createTime.title"/>',
	                                            width : 80,
	                                            sortable : true
	                                              
	                                        } /* ,
	                                        {
	                                            field : 'lastUpdateUser',
	                                            title : '<s:text name="最后修改人"/>',
	                                            width : 80,
	                                            sortable : true,
	                                            formatter:function(value, rowData, rowIndex){
                                                    var uObj=_userList[value];
                                                    if(uObj){
                                                        return uObj.realName;
                                                    }else{
                                                        return value;
                                                    }
                                                    
                                                  }
	                                        } ,
	                                        {
	                                            field : 'lastUpdateTime',
	                                            title : '<s:text name="最后修改时间"/>',
	                                            width : 80,
	                                            sortable : true,
	                                            formatter: function(value,row,index){
	                    							if(value){
	                    								return fnFormatDate(value, 'yyyy-MM-dd');
	                    							}
	                    						}
	                                             
	                                        } */ /* ,
	                                        {
	                                            field : 'version',
	                                            title : '<s:text name="版本号"/>',
	                                            width : 80,
	                                            sortable : true
	                                            
	                                        } */ 
	                                      
                        ]
                    ],
                    onDblClickRow:function(rowIndex, rowData){
                    	doDblView(rowData.id);
                    	$('#queryList').datagrid('unselectRow',rowIndex);
                    },
                    
    				onRowContextMenu  : function(e, rowIndex, rowData){
    					e.preventDefault();
    					$('#mm').data("id", rowData.id);
    					$('#mm').menu('show', {
    						left: e.pageX,
    						top: e.pageY
    					});
    				},
                    pagination : true,
                    rownumbers : true,
                    toolbar :[
                        {
                            id : 'bt_add',
                            text : '<s:text name="system.button.add.title"/>',
                            iconCls : 'icon-add',
                            handler : function() {
                                doAdd();
                            }
                        },
                        '-',
                        {
                            id : 'bt_del',
                            text : '<s:text name="system.button.delete.title"/>',
                            iconCls : 'icon-remove',
                            handler : function() {
                                doDelete();
                            }                           
                        } ,
                        '-',
                        {
                            id : 'bt_view',
                            text : '<s:text name="system.button.view.title"/>',
                            iconCls : "icon-search",
                            handler : function() {
                                doView();
                            }
                        },
                        '-',
                        {
                            id : 'bt_copy',
                            text : '<s:text name="system.button.copy.title"/>',
                            iconCls : "icon-copy",
                            handler : function() {
                            	doCopyTbar();
                            }
                        }/* ,
                        {
                            id : 'bt_accessory',
                            text : '<s:text name="system.button.accessory.title"/>',
                            iconCls : "icon-attach",
                            handler : function() {
                                doAccessory();
                            }
                        }  */
                    ]
                };
            }
               
            //右键复制一条记录 
            function doCopy(){
             var id = $("#mm").data("id");
             if(id==null){
            	 $.messager.alert('<s:text name="system.javascript.alertinfo.title"/>', '<s:text name="system.javascript.alertinfo.copy"/>', 'info');
                  return ;
             }else{
            	  parent.addTab('<s:text name="system.sysmng.desktop.copy.title"/>-'+id, '${ctx}/sys/desktop/desktop!edit.action?id=' + id);
             }
             
          }
           //工具条复制一条记录
           function doCopyTbar(){
        	   var id=0;
             	 var rows = $('#queryList').datagrid('getSelections');
             	 if(rows.length!=1){
             		 $.messager.alert('<s:text name="system.javascript.alertinfo.title"/>', '<s:text name="system.javascript.alertinfo.copy"/>', 'info');
             	     return ;
             	 } else{
             		 id=rows[0].id;
             		 parent.addTab('<s:text name="system.sysmng.desktop.copy.title"/>-'+id, '${ctx}/sys/desktop/desktop!edit.action?id=' + id);
             	 }
           }
           //删除一条记录
            function doDelete() {
                var ids = [];
                    var rows = $('#queryList').datagrid('getSelections');
                    for (var i = 0; i < rows.length; i++) {
                        ids.push(rows[i].id);
                    }
                   

                if (ids != null && ids.length > 0) {
                    $.messager.confirm('<s:text name="system.javascript.alertinfo.title"/>', '<s:text name="system.javascript.alertinfo.info"/>', function(r) {
                        if (r) {
                            var options = {
                                url : '${ctx}/sys/desktop/desktop!delete.action',
                                data : {
                                    "ids" : ids
                                },
                                success : function(data) {
                                    if (data.msg) {
                                        $('#queryList').datagrid('clearSelections');
                                        //doQuery();
                                        doRefreshDataGrid();
                                    }
                                },
                                traditional:true
                            };
                            fnFormAjaxWithJson(options);
                        }
                    })

                } else {
                    $.messager.alert('<s:text name="system.javascript.alertinfo.title"/>', '<s:text name="system.javascript.alertinfo.question"/>', 'info');
                }
            }
            //查询
            function doQuery() {
                var param = $("#queryForm").serializeArrayToParam();
                $("#queryList").datagrid("load", param);
            }

          //重定义尺寸
            function doResizeDataGrid() {
            	$("#queryList").datagrid("resize");
            	$("#queryList").datagrid("fixColumnSize","ck");
            }
          
           //新增
            function doAdd() {
                parent.addTab('<s:text name="system.sysmng.desktop.add.title"/>', '${ctx}/sys/desktop/desktop!add.action');
            }
            
            function doView() {
            	 var id=0;
            	 var rows = $('#queryList').datagrid('getSelections');
            	  if(rows.length!=1){
            		 $.messager.alert('<s:text name="system.javascript.alertinfo.title"/>', '<s:text name="system.javascript.alertinfo.view"/>', 'info');
            	     return ;
            	 } else{
            		 id=rows[0].id;
            	 }
            	 doDblView(id); 
            }
           //查看tab
            function doDblView(id) {
            	parent.addTab('<s:text name="system.sysmng.desktop.view.title"/>-'+id, '${ctx}/sys/desktop/desktop!view.action?id=' + id);
           }
            
            //右键查看
            function doCmView() {
            	var id = $("#mm").data("id");
            	doDblView(id);
            }
            
           //刷新列表
            function doRefreshDataGrid() {
    			$("#queryList").datagrid("load");
    		}
            $(document).ready(function() {
            	 //_userList=findAllUser();
                 //回车查询
                 doQuseryAction("queryForm");
                $("#queryList").datagrid(getOption());
                $("#bt_query").click(doQuery);
                $("#bt_reset").click(function() {
                    $("#queryForm")[0].reset();
                    doQuery();
                });
            });

            </script>
</head>            
         <body class="easyui-layout" fit="true">
          <div region="west" title="<s:text name='system.search.title'/>" border="false"
                 split="true" style="width:215px;padding:0px;"
                 iconCls="icon-search" tools="#pl_tt" >
			            <form action=""
	                      name="queryForm" id="queryForm">
	                       <div class="easyui-accordion" data-options="multiple:true" style="width:98%;" id="queryFormId">
								                                               <div class="xpstyle" title="<s:text name="system.sysmng.desktop.modelTitle.title"/>" collapsible="true" collapsed="true">
								                                                    <input type="text"
								                                                           name="filter_LIKES_title"
								                                                           id="title" class="Itext" />
								
								                                                </div> 
							                                    
																		     <div class="xpstyle" title="<s:text name="system.sysmng.desktop.key.title"/>" collapsible="true" collapsed="true">
								                                                  <input type="text"
								                                                           name="filter_LIKES_keyworlds"
								                                                           id="key" class="Itext" />
								
								                                             </div>
								                                             <div class="xpstyle" title="<s:text name="system.sysmng.desktop.desktopType.title"/>" collapsible="true" collapsed="true">
								                                                  <select class="easyui-combobox"
								                                                           name="filter_LIKES_type"
								                                                           id="type" style="width: 170px;">
								                                                            <option value="">所有</option>
								                                                            <option value="textList">文字列表</option>
																							<option value="picTextList">图文类型</option>
																							<option value="picList">图片类型</option>
																							<option value="datagrid">datagrid列表类型</option>
																							<option value="statistic1">统计分析类型1</option>
																							<option value="statistic2">统计分析类型2</option>
																							<option value="weather">天气预报类型</option>
																							<option value="datetime">日期时间类型</option></select>
																								
								                                             </div>
								                                             
<!-- 				 <td> <select id="type" class="easyui-combobox" name="type" 
						style="width: 200px;" >
							<option value="textList" selected="selected">文字列表</option>
							<option value="picTextList">图文类型</option>
							<option value="picList">图片类型</option>
							<option value="datagrid">datagrid列表类型</option>
							<option value="statistic1">统计分析类型1</option>
							<option value="statistic2">统计分析类型2</option>
							<option value="weather">天气预报类型</option>
							<option value="datetime">日期时间类型</option>
					</select></td> -->
					</div>
	                        <div style="text-align:center;padding:8px 8px;">
					                              <button type="button" id="bt_query" class="button_small">
								                                    <s:text name="system.search.button.title"/>
								                                </button>
								                                &nbsp;&nbsp;
								                                <button type="button" id="bt_reset" class="button_small">
								                                    <s:text name="system.search.reset.title"/>
								                                </button> 
							</div>
	                    </div>
	                </form>           
      
            </div>
            <div region="center" title="" border="false">
                <table id="queryList" border="false"></table>

            </div>
             <div id="mm" class="easyui-menu" style="width:120px;">
					<div onclick="doAdd()" iconCls="icon-add"><s:text name="system.button.add.title"/></div>
					<div onclick="doDelete()" iconCls="icon-remove"><s:text name="system.button.delete.title"/></div>
					<div onclick="doCmView()" iconCls="icon-search"><s:text name="system.button.view.title"/></div>
					<div onclick="doCopy()" iconCls="icon-copy"><s:text name="system.button.copy.title"/></div>
					<!--  <div onclick="doAccessory()" iconCls = "icon-attach">附件</div>  -->
		    </div>
		    
		     <div id="pl_tt">
				<a href="#" id="a_switch_query" class="icon-menu" title="<s:text name="system.button.switchQueryType.title"/>" style="display:none;"></a>
				<a href="#" id="a_exp_clp" class="accordion-expand" title="<s:text name="system.button.expand.title"/>"></a>
			 </div>
			
			
            </body>
</html>