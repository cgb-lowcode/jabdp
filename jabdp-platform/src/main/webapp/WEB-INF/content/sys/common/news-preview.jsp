<%@ page contentType="text/html;charset=UTF-8"%>
<%@ include file="/common/taglibs.jsp"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title><s:text name="system.index.title"/></title>
<link href="${ctx}/js/picNews/style/css.css" rel="stylesheet" type="text/css" />
<%@ include file="/common/meta.jsp" %>
<script type="text/javascript">
		          
   
	//定义一个全局变量保存操作状态
	var status = "";
	var _userList = {};
	function doInitForm(data,callFun) {
		if (data.msg) {
			var jsonData = data.msg;
			$("#showTitle").html(jsonData["${param.title}"]);
			$("#showContent").html(jsonData["${param.content}"]);
			var dtTime="";
			if(jsonData["lastUpdateTime"]){
				dtTime=jsonData["lastUpdateTime"];
			}else{
				dtTime=jsonData["createTime"];
			}
			$("#lastUpdateTime")
					.html(
							"发布时间："
									+ dtTime);
			var uId = jsonData["createUser"];
			var uObj = _userList[uId];
			if (uObj) {
				$("#showCreateUser").html("编辑：" + uObj.realName);
			} else {
				$("#showCreateUser").html("编辑：未知者");
			}

		}
		if(callFun){
			callFun();
		}

	}

	$(document).ready(function() {
		_userList = findAllUser();
		doModify("${param.id}","${param.entityName}");

	});
	   function doModify(id,entityName,callFunc) {
	       	var options = {
	             url : '${ctx}/gs/gs-mng!queryEntity.action',
	             data : {
	                 "id" : id,
	                 "entityName" :entityName
	             },
	             success : function(data) {
	                 doInitForm(data, callFunc);
	             }
	        };
	        fnFormAjaxWithJson(options);  
	   }
</script>
</head>


<body>
<div id="wrapper" style="margin-top:10px;">
	<div id="picSlideWrap" class="clearfix">
		         <div><h3 class="titleh3" id="showTitle"></h3></div>
		         <div><h4 class="titleh4" id="lastUpdateTime"></h4></div> 
	             <div id="content"><p id="showContent"></p></div>
    　　                              <div style="padding-right: 30px;"><p align="right" id="showCreateUser"></p></div>
        </div> 
    </div>
</body> 
</html>
