<%@ page contentType="text/html;charset=UTF-8"%>
<%@ include file="/common/taglibs.jsp"%>
<script type="text/javascript">

	$(document).ready(function() {
		_initUserMenu_();
	});


	//初始化消息要发送的用户树
 function _initUserMenu_(){
		var options = {
				url : '${ctx}/sys/account/user!getUserList.action',
				data : {
					"type":"${param.type}"
				},
				success : function(data) {
				if (data.msg) {
					var setting = {
							check: {
								enable: true
							},
							data: {
								simpleData: {
									enable: true
								}
							}
						};
					$.fn.zTree.init($("#m_users_"), setting, data.msg);
					zTree = $.fn.zTree.getZTreeObj("m_users_");
					zTree.expandAll(true);
					}
				}
			};
			fnFormAjaxWithJson(options,true);
	} 
	
</script>
<div class="easyui-layout" fit="true">
	
			<form action="" name="sel_usersForm" id="sel_usersForm" method="post">
					<table align="center">
						<tbody >
							<tr >
							<th><label ><s:text name="选择用户"/>:</label>
								</th>
								<td >
							<ul  style="text-align: center; width: 230px;height:200px;" id="m_users_" class="ztree"></ul>	
							</td>			
							</tr>
						</tbody>	
					</table>
			</form>	

</div>




