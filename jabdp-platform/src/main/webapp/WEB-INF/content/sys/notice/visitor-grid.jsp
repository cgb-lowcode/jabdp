<%@ page contentType="text/html;charset=UTF-8"%>
<%@ include file="/common/taglibs.jsp"%>
<!-- 访问者datagrid -->
<table id="visitorGrid"></table>
<script type="text/javascript">
var _relevanceId = "${param.id}"; // 关联ID
_userList = {};
function initVisitorGrid() { // 页面Documemt初始化
	$("#visitorGrid").datagrid({
		height : 365,
		striped : true,
		fitColumns : true,
		rownumbers : true,
		pagination : true,
		url : "${ctx}/sys/visitor/visitor!queryVisitors.action",
		queryParams : {id : _relevanceId},
        columns : [[
			{
				field : 'id',
				hidden : true
			},
	        {
                field : 'userId',
                title : '访问者',
                width : 200,
                sortable : true,
                formatter : function(value, rowData, rowIndex) {
                 	/*var uObj = _userList[value];
					if(uObj) {
                        var nickName = (uObj.nickName) ? ("-" + uObj.nickName) : "";
						return uObj.realName + "[" + uObj.loginName + "]" + nickName;
					} else {
                        return value;
					}*/
					return rowData["userName"];
             	}
            },
	        {
            	field : 'visitTime',
            	title : '访问时间',
            	width : 200
	        }
        ]]
	});
}
$(function() {
	//_userList = findAllUser();
	initVisitorGrid();
});
</script>
