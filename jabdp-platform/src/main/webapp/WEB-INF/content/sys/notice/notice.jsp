<%@ page contentType="text/html;charset=UTF-8"%>
<%@ include file="/common/taglibs.jsp"%>
<!DOCTYPE html>
<html
<head>
<title><s:text name="system.index.title" />
</title>
<%@ include file="/common/meta-gs.jsp"%>
	<%-- <link href="${ctx}/js/easyui/${themeColor}/panel.css" rel="stylesheet" type="text/css"/>--%>
    <script type="text/javascript" src="${ctx}/js/easyui-1.4/easyui.xpPanel.js"></script>
<script type="text/javascript">
            var _userList={};

            function getOption() {
                return {
                    width : 'auto',
                    height : 'auto',
                    nowrap : false,
                    striped : true,
                    fit : true,
                    url :'${ctx}/sys/notice/notice!queryList.action',
                    sortName : 'createTime',
                    sortOrder : 'desc',
                    frozenColumns : [
                        [
                            {
                                field : 'ck',
                                checkbox : true
                            } , 
                            {
								field : "atmId",
								title : "<s:text name='system.button.accessory.title'/>",
								width : 35,
								formatter:function(value, rowData, rowIndex) {
									if(value) {
										var astr = [];
										astr.push('<a class="easyui-linkbutton l-btn l-btn-plain" href="javascript:void(0);"');
										<security:authorize url="/sys/attach/attach.action"> 
										astr.push(' onclick="doAccessory(');
										astr.push(value);
										astr.push(',');
										astr.push(rowData["id"]);
										astr.push(');" ');
										</security:authorize> 
										astr.push('>');
										astr.push('<span class="l-btn-left"><span class="l-btn-text"><span class="l-btn-empty icon-attach">&nbsp;</span></span></span></a>');
										return astr.join("");
									} else {
										return "<span></span>";
									}
								}
                            } 
                        ]
                    ],
                    columns : [
                        [
	                                        /* {
	                                            field : 'id',
	                                            title : '<s:text name="编号"/>',
	                                            width : 80,
	                                            sortable : true
	                                        } , */
	                                        {
	                                            field : 'title',
	                                            title : '<s:text name="system.sysmng.desktop.modelTitle.title"/>',
	                                            width : 250,
	                                            sortable : true
	                                        } ,
	                                        {
	                                            field : 'keyworlds',
	                                            title : '<s:text name="system.sysmng.desktop.key.title"/>',
	                                            width : 80,
	                                            sortable : true
	                                        } /* ,
	                                        {
	                                            field : 'content',
	                                            title : '<s:text name="正文"/>',
	                                            width : 80,
	                                            sortable : true
	                                        }  */,
	                                        {
	                                            field : 'createUser',
	                                            title : '<s:text name="system.sysmng.desktop.createUser.title"/>',
	                                            width : 100,
	                                            sortable : true,
	                                            formatter:function(value, rowData, rowIndex){
	                                            	/*var uObj=_userList[value];
                                                    if(uObj){
                                                        var nickName = (uObj.nickName)?("-" + uObj.nickName):"";
														return uObj.realName + "[" + uObj.loginName + "]" + nickName;
                                                    }else{
                                                        return value;
                                                    }*/
                                                    return rowData["createUserCaption"];
                                                 }
	                                        } ,
	                                        {
	                                            field : 'createTime',
	                                            title : '<s:text name="system.sysmng.desktop.createTime.title"/>',
	                                            width : 130,
	                                            sortable : true/* ,
	                                            formatter: function(value,row,index){
	                    							if(value){
	                    								return fnFormatDate(value, 'yyyy-MM-dd');
	                    							}
	                    						} */
	                                              
	                                        }/*  ,
	                                        {
	                                            field : 'lastUpdateUser',
	                                            title : '<s:text name="最后修改人"/>',
	                                            width : 80,
	                                            sortable : true,
	                                            formatter:function(value, rowData, rowIndex){
                                                    var uObj=_userList[value];
                                                    if(uObj){
                                                        return uObj.realName;
                                                    }else{
                                                        return value;
                                                    }
                                                    
                                                  }
	                                        } ,
	                                        {
	                                            field : 'lastUpdateTime',
	                                            title : '<s:text name="最后修改时间"/>',
	                                            width : 80,
	                                            sortable : true,
	                                            formatter: function(value,row,index){
	                    							if(value){
	                    								return fnFormatDate(value, 'yyyy-MM-dd');
	                    							}
	                    						}
	                                             
	                                        }  *//* ,
	                                        {
	                                            field : 'version',
	                                            title : '<s:text name="版本号"/>',
	                                            width : 80,
	                                            sortable : true
	                                            
	                                        } */ ,
	                                        {
	                                            field : 'status',
	                                            title : '<s:text name="system.sysmng.notice.status.title"/>',
	                                            width : 80,
	                                            sortable : true,
	                                            formatter:function(value,rowData,rowIndex){
	                                            	if(value=="1"){
	                                            		return "已发布";
	                                            	}else if(value=="0"){
	                                            		return "草稿";
	                                            	}
	                                            }
	                                            
	                                        }  ,
	                                        {
	                                            field : 'type',
	                                            title : '<s:text name="system.sysmng.notice.type.title"/>',
	                                            width : 80,
	                                            sortable : true,
	                                            formatter:getTypeValue
	                                        }  ,
	                                        {
	                                            field : 'style',
	                                            title : '<s:text name="system.sysmng.notice.style.title"/>',
	                                            width : 80,
	                                            sortable : true,
	                                            formatter:function(value, rowData, rowIndex){
	                                            	if(value=="01"){
	                    								return "<s:text name='system.sysmng.notice.common.title'/>";
	                    							}else if(value=="02"){
	                    								return "<s:text name='system.sysmng.notice.warning.title'/>";
	                    							}else if(value=="03"){
	                    								return "<s:text name='system.sysmng.notice.todo.title'/>";
	                    							}else if(value=="04"){
	                    								return "<s:text name='业务确认通知'/>";
	                    							}else if(value=="05"){
	                    								return "<s:text name='提醒通知'/>";
	                    							}else if(value=="06"){
	                    								return "<s:text name='业务办理通知'/>";
	                    							}
	                                             }
	                                        }
                        ]
                    ],
                    onDblClickRow:function(rowIndex, rowData){
                    	doDblView(rowData.id);
                    	$('#queryList').datagrid('unselectRow',rowIndex);
                    },
                    
    				onRowContextMenu  : function(e, rowIndex, rowData){
    					e.preventDefault();
    					$('#mm').data("id", rowData.id);
    					$('#mm').menu('show', {
    						left: e.pageX,
    						top: e.pageY
    					});
    				},
                    pagination : true,
                    rownumbers : true,
                    toolbar :[
                        {
                            id : 'bt_add',
                            text : '<s:text name="system.button.add.title"/>',
                            iconCls : 'icon-add',
                            handler : function() {
                                doAdd();
                            }
                        },
                        '-',
                        {
                            id : 'bt_del',
                            text : '<s:text name="system.button.delete.title"/>',
                            iconCls : 'icon-remove',
                            handler : function() {
                                doDelete(null);
                            }                           
                        } ,
                        '-',
                        {
                            id : 'bt_view',
                            text : '<s:text name="system.button.view.title"/>',
                            iconCls : "icon-search",
                            handler : function() {
                                doView();
                            }
                        },
                        '-',
                        {
                            id : 'bt_copy',
                            text : '<s:text name="system.button.copy.title"/>',
                            iconCls : "icon-copy",
                            handler : function() {
                            	doCopyTbar();
                            }
                        } ,
                        {
                            id : 'bt_accessory',
                            text : '<s:text name="system.button.accessory.title"/>',
                            iconCls : "icon-attach",
                            handler : function() {
                                doAccessory();
                            }
                        } 
                    ]
                };
            }
               
         
            
            //右键复制一条记录 
            function doCopy(){
             var id = $("#mm").data("id");
             if(id==null){
            	 $.messager.alert('<s:text name="system.javascript.alertinfo.title"/>', '<s:text name="system.javascript.alertinfo.copy"/>', 'info');
                  return ;
             }else{
            	  parent.addTab('<s:text name="system.button.copy.title"/>-'+id, '${ctx}/sys/notice/notice!edit.action?id=' + id);
             }
             
          }
           //工具条复制一条记录
           function doCopyTbar(){
        	   var id=0;
             	 var rows = $('#queryList').datagrid('getSelections');
             	 if(rows.length!=1){
             		 $.messager.alert('<s:text name="system.javascript.alertinfo.title"/>', '<s:text name="system.javascript.alertinfo.copy"/>', 'info');
             	     return ;
             	 } else{
             		 id=rows[0].id;
             		 parent.addTab('<s:text name="system.sysmng.notice.copy.title"/>-'+id, '${ctx}/sys/notice/notice!edit.action?id=' + id);
             	 }
           }
           //删除一条记录
            function doDelete() {
                var ids = [];
                    var rows = $('#queryList').datagrid('getSelections');
                    for (var i = 0; i < rows.length; i++) {
                        ids.push(rows[i].id);
                    }
                if (ids != null && ids.length > 0) {
                    $.messager.confirm('<s:text name="system.javascript.alertinfo.title"/>', '<s:text name="system.javascript.alertinfo.info"/>', function(r) {
                        if (r) {
                            var options = {
                                url : '${ctx}/sys/notice/notice!delete.action',
                                data : {
                                    "ids" : ids
                                },
                                success : function(data) {
                                    if (data.msg) {
                                        $('#queryList').datagrid('clearSelections');
                                        doQuery();
                                    }
                                },
                                traditional:true
                            };
                            fnFormAjaxWithJson(options);
                        }
                    })

                } else {
                    $.messager.alert('<s:text name="system.javascript.alertinfo.title"/>', '<s:text name="system.javascript.alertinfo.question"/>', 'info');
                }
            }
            //查询
            function doQuery() {
                var param = $("#queryForm").serializeArrayToParam();
                $("#queryList").datagrid("load", param);
            }

 			//重定义尺寸
            function doResizeDataGrid() {
            	$("#queryList").datagrid("resize");
            	$("#queryList").datagrid("fixColumnSize","ck");
            }

          
           //新增
            function doAdd() {
                parent.addTab('<s:text name="system.sysmng.notice.add.title"/>', '${ctx}/sys/notice/notice!add.action');
            }
            
            function doView() {
            	 var id=0;
            	 var rows = $('#queryList').datagrid('getSelections');
            	  if(rows.length!=1){
            		 $.messager.alert('<s:text name="system.javascript.alertinfo.title"/>', '<s:text name="system.javascript.alertinfo.view"/>', 'info');
            	     return ;
            	 } else{
            		 id=rows[0].id;
            	 }
            	 doDblView(id); 
            }
           //查看tab
            function doDblView(id) {
            	parent.addTab('<s:text name="system.sysmng.notice.view.title"/>-'+id, '${ctx}/sys/notice/notice!view.action?id=' + id);
           }
            
            //右键查看
            function doCmView() {
            	var id = $("#mm").data("id");
            	doDblView(id);
            }
            
           //刷新列表
            function doRefreshDataGrid() {
    			$("#queryList").datagrid("load");
    		}
    		
          //添加附件
            function doAccessory(attachId,id) {
            	if(!attachId && !id){
           	     var rows = $('#queryList').datagrid('getSelections');
	           	 if(rows.length!=1){
	           		 $.messager.alert('<s:text name="system.javascript.alertinfo.title"/>', '<s:text name="system.javascript.alertinfo.recordInfo"/>', 'info');
	           		 return ;
	           	 } else{
	           		attachId=rows[0].atmId;
	           		id=rows[0].id;
	           	 }
            	}
	             var options ={"attachId":attachId,
           				 "id":id,
           				 "key":"",
           				 "entityName":"",
           				 "dgId":"queryList",
           				 "flag":"mainQuery",
           				 "readOnly" : false
           				 };
            	 doAttach(options);
            }

			//更新附件ID flag: mainQuery-主实体查询，subEdit-子实体编辑，mainEdit-主实体编辑
            function doUpdateAttach(opts) {
            	var options = {
            			url : '${ctx}/sys/notice/notice!updateNotice.action',
                        data : {
                        	"entityName":opts.entityName,
                            "id" : opts.id,
                            "atmId": opts.attachId
                        },
    					async: false,
    					success : function(data) {
    						 if(data.msg) {
        						if(opts.flag == "mainQuery") {
        							$("#" + opts.dgId).datagrid("reload");
            					} else if(opts.flag == "subEdit"){
            						$("#" + opts.dgId).datagrid("reload");
                				}
    						} 
    					}
    			};
    			fnFormAjaxWithJson(options);
    			
            }
            
            var typeObj = {"id":"id","pid":"pid","text":"name"};
	   		var typeJsonParam = {"type":"notice","url":"${ctx}/sys/common/common-type!queryList.action"};
	   		var typeJson = getJsonObjByParam(typeJsonParam);
            
	   		function getTypeValue(value, rowData, rowIndex) {
                return getColumnValueByIdText(typeObj, typeJson, value);
            }
            
             var doTreeQuery_type = function(ids) {
		  		var param = {"filter_INL_type":ids.join(",")};													                
	            $("#queryList").datagrid("load", param);
			  };
			  
			 var doTreeAfterSave_type = function(obj) {
				 typeJson = getJsonObjByParam(typeJsonParam);
			  };   
			  
             //增加类型刷新树列表
             function doRefreshTree(){
            	 _initCommonTypeTree_();
            	 doTreeAfterSave_type();
            	 doRefreshDataGrid();
             }
            
            $(document).ready(function() {
               //_userList=findAllUser();

               //回车查询
                 doQuseryAction("queryForm");
                $("#queryList").datagrid(getOption());
                $("#bt_query").click(doQuery);
                $("#bt_reset").click(function() {
                    $("#queryForm")[0].reset();
                    doQuery();
                });
             
                
                 $("#queryFormId").hide();
				 $("#a_exp_clp").hide();
					$("#a_switch_query").toggle(function() {
						$("#queryFormId").show();
						$("#a_exp_clp").show();
						$("#tree_type").hide();
					}, function() {
						$("#queryFormId").hide();
						$("#a_exp_clp").hide();
						$("#tree_type").show();
					});
				  $("#a_switch_query").show();
            });
            
     </script>
</head>
<body class="easyui-layout" fit="true">
	<div region="west" title="<s:text name='system.search.title'/>"
		split="true" border="false"
		style="width: 215px; padding: 0px;"
		iconCls="icon-search" tools="#pl_tt">
		<div id="tree_type" ><div style="padding-top:4px;width:100%;" class="easyui-panel" href="${ctx}/sys/common/dict-tree.action?type=notice&fname=doTreeQuery_type&fsname=doTreeAfterSave_type&isCanDrag=true" border="0"></div></div>
		<form action="" name="queryForm" id="queryForm">
			<div class="easyui-accordion" data-options="multiple:true" style="width:98%;" id="queryFormId">

				<div class="xpstyle" title="<s:text name="system.sysmng.desktop.modelTitle.title"/>" collapsible="true"
					collapsed="true">
					<input type="text" name="filter_LIKES_title" id="title" class="Itext"/>

				</div>

				<div class="xpstyle" title="<s:text name="system.sysmng.desktop.key.title"/> " collapsible="true"
					collapsed="true">
					<input type="text" name="filter_LIKES_keyworlds" id="key" class="Itext"/>
				</div>
				
			</div>
			<div style="text-align:center;padding:8px 8px;">
				<button type="button" id="bt_query" class="button_small">
					<s:text name="system.search.button.title" />
				</button>
				&nbsp;&nbsp;
				<button type="button" id="bt_reset" class="button_small">
					<s:text name="system.search.reset.title" />
				</button>
			</div> 
		</form>

	</div>
	<div region="center" title="" border="false">
		<table id="queryList" border="false"></table>
	</div>
	<div id="mm" class="easyui-menu" style="width: 120px;">
		<div onclick="doAdd()" iconCls="icon-add"><s:text name="system.button.add.title"/></div>
		<div onclick="doDelete()" iconCls="icon-remove"><s:text name="system.button.delete.title"/></div>
		<div onclick="doCmView()" iconCls="icon-search"><s:text name="system.button.view.title"/></div>
		<div onclick="doCopy()" iconCls="icon-copy"><s:text name="system.button.copy.title"/></div>
		<div onclick="doAccessory()" iconCls="icon-attach"><s:text name='system.button.accessory.title'/></div>
	</div>

	<div id="pl_tt">
		<a href="javascript:void(0);" id="a_reload" class="icon-reload"
			title="<s:text name="system.button.refresh.title"/>" onclick="doRefreshTree();"></a> 
		<a href="#"
			id="a_switch_query" class="icon-menu" title="<s:text name="system.button.switchQueryType.title"/>"
			style="display: none;"></a>
		<a href="#" id="a_exp_clp" class="accordion-expand" title="<s:text name="system.button.expand.title"/>"></a> 	
	</div>
</body>
</html>