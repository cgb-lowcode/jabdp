<%@ page contentType="text/html;charset=UTF-8"%>
<%@ include file="/common/taglibs.jsp"%>
<!DOCTYPE html>
<html>
<head>
	<title><s:text name="system.index.title"/></title>
	<%@ include file="/common/meta-gs.jsp" %>
<script type="text/javascript">
		var setting = {
			callback : {
				onClick : onTreeClick
			}
		};
		var zNodes =[
								{id:"1101", name:"业务字典", type:"dicts", key:"dicts", iconSkin:"icon-address_Book"	
                            	,children: [
	                             	{id:"110101", name:"我方公司", type:"dict", key:"WoFangGongSi", 
	                             	 iconSkin:"icon-address_Book",entityName:"wofanggongsi.WoFangGongSiZhuBiao"},
	                             	{id:"110143", name:"收款银行", type:"dict", key:"ShouKuanYinHang", 
	                             	 iconSkin:"icon-address_Book",entityName:"shoukuanyinhang.ShouKuanYinXingZhuBiao"},
	                             	{id:"110102", name:"报销项目", type:"dict", key:"BaoXiaoXiangMu", 
	                             	 iconSkin:"icon-address_Book",entityName:"baoxiaoxiangmu.BaoXiaoXiangMuZhuBiao"},
	                             	{id:"110104", name:"币种汇率", type:"dict", key:"BiZhongHuiLv", 
	                             	 iconSkin:"icon-address_Book",entityName:"bizhonghuilv.BiZhongHuiLvZhuBiao"},
	                             	{id:"110115", name:"维度", type:"dict", key:"WeiDu", 
	                             	 iconSkin:"icon-administrative_Tools",entityName:"weidu.WeiDuZhuBiao"},
	                             	{id:"110116", name:"维度字段", type:"dict", key:"WeiDuZiDuan", 
	                             	 iconSkin:"icon-administrative_Tools",entityName:"weiduziduan.WeiDuZiDuanZhuBiao"},
	                             	{id:"110105", name:"精度", type:"dict", key:"JingDu", 
	                             	 iconSkin:"icon-archive",entityName:"jingdu.JingDuZhuBiao"},
	                             	{id:"110106", name:"接口", type:"dict", key:"JieKou", 
	                             	 iconSkin:"icon-archive",entityName:"jiekou.JieKouZhuBiao"},
	                             	{id:"110108", name:"密封圈", type:"dict", key:"MiFengQuan", 
	                             	 iconSkin:"icon-archive",entityName:"mifengquan.MiFengQuanZhuBiao"},
	                             	{id:"110110", name:"直径", type:"dict", key:"ZhiJing", 
	                             	 iconSkin:"icon-archive",entityName:"zhijing.ZhiJingZhuBiao"},
	                             	{id:"110111", name:"长度", type:"dict", key:"ChangDu", 
	                             	 iconSkin:"icon-archive",entityName:"changdu.ChangDuZhuBiao"},
	                             	{id:"110112", name:"过滤器材料", type:"dict", key:"CaiLiao", 
	                             	 iconSkin:"icon-archive",entityName:"cailiao.CaiLiaoZhuBiao"},
	                             	{id:"110113", name:"滤芯数量", type:"dict", key:"LvXinShuLiang", 
	                             	 iconSkin:"icon-archive",entityName:"lvxinshuliang.LvXinShuLiangZhuBiao"},
	                             	{id:"110114", name:"进出口规格", type:"dict", key:"JinChuKouLianJie", 
	                             	 iconSkin:"icon-archive",entityName:"jinchukoulianjie.JinChuKouLianJieZhuBiao"},
	                             	{id:"110117", name:"滤膜材料", type:"dict", key:"LvMoCaiLiao", 
	                             	 iconSkin:"icon-archive",entityName:"lvmocailiao.LvMoCaiLiaoZhuBiao"},
	                             	{id:"110118", name:"滤壳材料", type:"dict", key:"LvKeCaiLiao", 
	                             	 iconSkin:"icon-archive",entityName:"lvkecailiao.LvKeCaiLiaoZhuBiao"},
	                             	{id:"110128", name:"其他维度", type:"dict", key:"QiTaWeiDu", 
	                             	 iconSkin:"icon-archive",entityName:"qitaweidu.QiTaWeiDuZhuBiao"},
	                             	{id:"110138", name:"行业代号", type:"dict", key:"HangYeDaiHao", 
	                             	 iconSkin:"icon-archive",entityName:"hangyedaihao.HangYeDaiHaoZhuBiao"},
	                             	{id:"110139", name:"桶体连接方式", type:"dict", key:"TongTiLianJieFangShi", 
	                             	 iconSkin:"icon-archive",entityName:"tongtilianjiefangshi.TongTiLianJieFangShiZhuBiao"},
	                             	{id:"110140", name:"耐压设计", type:"dict", key:"NaiYaSheJi", 
	                             	 iconSkin:"icon-archive",entityName:"naiyasheji.NaiYaSheJiZhuBiao"},
	                             	{id:"110141", name:"表面处理方式", type:"dict", key:"BiaoMianChuLiFangShi", 
	                             	 iconSkin:"icon-archive",entityName:"biaomianchulifangshi.BiaoMianChuLiFangShiZhuBiao"},
	                             	{id:"110142", name:"备注", type:"dict", key:"WeiDuBeiZhu", 
	                             	 iconSkin:"icon-archive",entityName:"weidubeizhu.WeiDuBeiZhuZhuBiao"},
	                             	{id:"110120", name:"计量单位", type:"dict", key:"JiLiangDanWei", 
	                             	 iconSkin:"icon-gnome-accessories-calculator",entityName:"jiliangdanwei.JiLiangDanWeiZhuBiao"},
	                             	{id:"110121", name:"运输方式", type:"dict", key:"YunShuFangShi", 
	                             	 iconSkin:"icon-gnome-accessories-calculator",entityName:"yunshufangshi.YunShuFangShiZhuBiao"},
	                             	{id:"110122", name:"省份", type:"dict", key:"ShengFen", 
	                             	 iconSkin:"icon-address_Book",entityName:"shengfen.ShengFenZhuBiao"},
	                             	{id:"110144", name:"城市", type:"dict", key:"ChengShi", 
	                             	 iconSkin:"icon-address_Book",entityName:"chengshi.ChengShiZhuBiao"},
	                             	{id:"110145", name:"县区", type:"dict", key:"XianQu", 
	                             	 iconSkin:"icon-address_Book",entityName:"xianqu.XianQuZhuBiao"},
	                             	{id:"110123", name:"客户类别", type:"dict", key:"KeHuLeiBie", 
	                             	 iconSkin:"icon-address_Book",entityName:"kehuleibie.KeHuLeiBieZhuBiao"},
	                             	{id:"110131", name:"客户等级", type:"dict", key:"KeHuDengJi", 
	                             	 iconSkin:"icon-address_Book",entityName:"kehudengji.KeHuDengJiZhuBiao"},
	                             	{id:"110132", name:"客户来源", type:"dict", key:"KeHuLaiYuan", 
	                             	 iconSkin:"icon-address_Book",entityName:"kehulaiyuan.KeHuLaiYuanZhuBiao"},
	                             	{id:"110125", name:"入库交易单类别", type:"dict", key:"RuKuDanLeiBie", 
	                             	 iconSkin:"icon-address_Book",entityName:"rukudanleibie.RuKuDanLeiBieZhuBiao"},
	                             	{id:"110126", name:"出库交易单类别", type:"dict", key:"ChuKuDanLeiBie", 
	                             	 iconSkin:"icon-address_Book",entityName:"chukudanleibie.ChuKuDanLeiBieZhuBiao"},
	                             	{id:"110127", name:"工单类别", type:"dict", key:"GongDanLeiBie", 
	                             	 iconSkin:"icon-address_Book",entityName:"gongdanleibie.GongDanLeiBieZhuBiao"},
	                             	{id:"110129", name:"代号规则", type:"dict", key:"DaiHaoGuiZe", 
	                             	 iconSkin:"icon-address_Book",entityName:"daihaoguize.DaiHaoGuiZeZhuBiao"},
	                             	{id:"110130", name:"打标内容", type:"dict", key:"DaBiaoNaRong", 
	                             	 iconSkin:"icon-address_Book",entityName:"dabiaonarong.DaBiaoNaRongZhuBiao"},
	                             	{id:"110133", name:"包装方式", type:"dict", key:"BaoZhuangFangShi", 
	                             	 iconSkin:"icon-archive",entityName:"baozhuangfangshi.BaoZhuangFangShiZhuBiao"},
	                             	{id:"110134", name:"物流品名", type:"dict", key:"WuLiuPinMing", 
	                             	 iconSkin:"icon-archive",entityName:"wuliupinming.WuLiuPinMingZhuBiao"},
	                             	{id:"110135", name:"收款方式", type:"dict", key:"ShouKuanFangShi", 
	                             	 iconSkin:"icon-archive",entityName:"shoukuanfangshi.ShouKuanFangShiZhuBiao"},
	                             	{id:"110136", name:"质保书模板", type:"dict", key:"ZhiBaoShuMoBan", 
	                             	 iconSkin:"icon-archive",entityName:"zhibaoshumoban.ZhiBaoShuMoBanZhuBiao"},
	                             	{id:"110137", name:"备用模板", type:"dict", key:"BeiYongMoBan", 
	                             	 iconSkin:"icon-archive",entityName:"beiyongmoban.BeiYongMoBanZhuBiao"},
	                             	{id:"110151", name:"随货资料", type:"dict", key:"FuZhiBaoShu", 
	                             	 iconSkin:"icon-archive",entityName:"fuzhibaoshu.FuZhiBaoShuZhuBiao"},
	                             	{id:"110152", name:"质保书属性", type:"dict", key:"ZhiBaoShuShuXing", 
	                             	 iconSkin:"icon-archive",entityName:"zhibaoshushuxing.ZhiBaoShuShuXingZhuBiao"},
	                             	{id:"110146", name:"销售订单状态", type:"dict", key:"XiaoShouDingDanZhuangTai", 
	                             	 iconSkin:"icon-address_Book",entityName:"xiaoshoudingdanzhuangtai.XiaoShouDingDanZhuangTai"},
	                             	{id:"110147", name:"生产订单状态", type:"dict", key:"ShengChanDingDanZhuangTai", 
	                             	 iconSkin:"icon-address_Book",entityName:"shengchandingdanzhuangtai.ShengChanDingDanZhuangTai"},
	                             	{id:"110148", name:"工单状态", type:"dict", key:"GongDanZhuangTai", 
	                             	 iconSkin:"icon-address_Book",entityName:"gongdanzhuangtai.GongDanZhuangTai"},
	                             	{id:"110149", name:"订单产品状态", type:"dict", key:"DingDanChanPinZhuangTai", 
	                             	 iconSkin:"icon-address_Book",entityName:"dingdanchanpinzhuangtai.DingDanChanPinZhuangTai"},
	                             	{id:"110162", name:"发货状态", type:"dict", key:"FaHuoZhuangTai", 
	                             	 iconSkin:"icon-address_Book",entityName:"fahuozhuangtai.FaHuoZhuangTaiZhuBiao"},
	                             	{id:"110150", name:"产品特殊要求", type:"dict", key:"ChanPinTeShuYaoQiu", 
	                             	 iconSkin:"icon-address_Book",entityName:"chanpinteshuyaoqiu.ChanPinTsYaoQiuZhuBiao"},
	                             	{id:"110153", name:"车辆信息", type:"dict", key:"CheLiangXinXi", 
	                             	 iconSkin:"icon-address_Book",entityName:"cheliangxinxi.CheLiangXinXiZhuBiao"},
	                             	{id:"110154", name:"改单类型", type:"dict", key:"GaiDanLeiXing", 
	                             	 iconSkin:"icon-address_Book",entityName:"gaidanleixing.GaiDanLeiXingZhuBiao"},
	                             	{id:"110155", name:"改单原因", type:"dict", key:"GaiDanYuanYin", 
	                             	 iconSkin:"icon-address_Book",entityName:"gaidanyuanyin.GaiDanYuanYinZhuBiao"},
	                             	{id:"110156", name:"发票类型", type:"dict", key:"FaPiaoLeiXing", 
	                             	 iconSkin:"icon-address_Book",entityName:"fapiaoleixing.FaPiaoLeiXingZhuBiao"},
	                             	{id:"110157", name:"用款用途", type:"dict", key:"YongKuanYongTu", 
	                             	 iconSkin:"icon-address_Book",entityName:"yongkuanyongtu.YongKuanYongTuZhuBiao"},
	                             	{id:"110158", name:"用款方式", type:"dict", key:"YongKuanFangShi", 
	                             	 iconSkin:"icon-address_Book",entityName:"yongkuanfangshi.YongKuanFangShiZhuBiao"},
	                             	{id:"110159", name:"岗位", type:"dict", key:"GangWei", 
	                             	 iconSkin:"icon-address_Book",entityName:"gangwei.GangWeiZhuBiao"},
	                             	{id:"110160", name:"人才来源", type:"dict", key:"RenCaiLaiYuan", 
	                             	 iconSkin:"icon-address_Book",entityName:"rencailaiyuan.RenCaiLaiYuanZhuBiao"},
	                             	{id:"110161", name:"交货原因", type:"dict", key:"JiaoHuoYuanYin", 
	                             	 iconSkin:"icon-address_Book",entityName:"jiaohuoyuanyin.JiaoHuoYuanYinZhuBiao"},
	                             	{id:"110163", name:"报销用款方式", type:"dict", key:"BaoXiaoYongKuanFangShi", 
	                             	 iconSkin:"icon-address_Book",entityName:"baoxiaoyongkuanfangshi.BaoXiaoYongKuanFangShiZh"},
	                             	{id:"110164", name:"报销用款方式", type:"dict", key:"BaoXiaoYongKuanFangShi", 
	                             	 iconSkin:"icon-address_Book",entityName:"baoxiaoyongkuanfangshi.BaoXiaoYongKuanFangShiZh"},
	                             	{id:"110165", name:"出入类型", type:"dict", key:"ChuRuLeiXing", 
	                             	 iconSkin:"icon-address_Book",entityName:"churuleixing.ChuRuLeiXingZhuBiao"},
	                             	{id:"110167", name:"检验方式", type:"dict", key:"JianYanFangShi", 
	                             	 iconSkin:"icon-address_Book",entityName:"jianyanfangshi.JianYanFangShiZhuBiao"},
	                             	{id:"110168", name:"进货随货资料", type:"dict", key:"SuiHuoZiLiao", 
	                             	 iconSkin:"icon-address_Book",entityName:"suihuoziliao.SuiHuoZiLiaoZhuBiao"},
	                             	{id:"110169", name:"假期类型", type:"dict", key:"JiaQiLeiXing", 
	                             	 iconSkin:"icon-address_Book",entityName:"jiaqileixing.JiaQiLeiXingZhuBiao"},
	                             	{id:"110170", name:"来电类型", type:"dict", key:"LaiDianLeiXing", 
	                             	 iconSkin:"icon-address_Book",entityName:"laidianleixing.LaiDianLeiXingZhuBiao"},
	                             	{id:"110171", name:"信息来源", type:"dict", key:"XinXiLaiYuan", 
	                             	 iconSkin:"icon-address_Book",entityName:"xinxilaiyuan.XinXiLaiYuanZhuBiao"},
	                             	{id:"110172", name:"学历", type:"dict", key:"XueLi", 
	                             	 iconSkin:"icon-address_Book",entityName:"xueli.XueLiZhuBiao"},
	                             	{id:"110173", name:"员工状态", type:"dict", key:"YuanGongZhuangTai", 
	                             	 iconSkin:"icon-address_Book",entityName:"yuangongzhuangtai.YuanGongZhuangTaiZhuBiao"},
	                             	{id:"110174", name:"户口性质", type:"dict", key:"HuKouXingZhi", 
	                             	 iconSkin:"icon-address_Book",entityName:"hukouxingzhi.HuKouXingZhiZhuBiao"},
	                             	{id:"110175", name:"收款登记类型", type:"dict", key:"ShouKuanDengJiLeiXing", 
	                             	 iconSkin:"icon-address_Book",entityName:"shoukuandengjileixing.ShouKuanDengJiLeiXingZhuBiao"},
	                             	{id:"110176", name:"采购订单状态", type:"dict", key:"CaiGouDingDanZhuangTai", 
	                             	 iconSkin:"icon-address_Book",entityName:"caigoudingdanzhuangtai.CaiGouDingDanZhuangTaiZhuBiao"},
	                             	{id:"110177", name:"车间", type:"dict", key:"CheJian", 
	                             	 iconSkin:"icon-address_Book",entityName:"chejian.CheJianZhuBiao"},
	                             	{id:"110178", name:"退货责任方", type:"dict", key:"ZeRenFang", 
	                             	 iconSkin:"icon-address_Book",entityName:"zerenfang.ZeRenFangZhuBiao"},
	                             	{id:"110179", name:"严重等级", type:"dict", key:"ZeRenDengJi", 
	                             	 iconSkin:"icon-address_Book",entityName:"zerendengji.ZeRenDengJiZhuBiao"},
	                             	{id:"110180", name:"退货原因", type:"dict", key:"TuiHuoYuanYin", 
	                             	 iconSkin:"icon-address_Book",entityName:"tuihuoyuanyin.TuiHuoYuanYinZhuBiao"},
	                             	{id:"110181", name:"订单来源", type:"dict", key:"DingDanLaiYuan", 
	                             	 iconSkin:"icon-address_Book",entityName:"dingdanlaiyuan.DingDanLaiYuanZhuBiao"},
	                             	{id:"110182", name:"单据属性管理", type:"dict", key:"DanJuShuXingGuanLi", 
	                             	 iconSkin:"icon-address_Book",entityName:"danjushuxingguanli.DanJuShuXingGuanLiZhuBiao"},
	                             	{id:"110183", name:"条码属性管理", type:"dict", key:"TiaoMaShuXingGuanLi", 
	                             	 iconSkin:"icon-address_Book",entityName:"tiaomashuxingguanli.TiaoMaShuXingGuanLiZhuBiao"},
	                             	{id:"110184", name:"问题模块", type:"dict", key:"WenTiMoKuai", 
	                             	 iconSkin:"icon-address_Book",entityName:"wentimokuai.WenTiMoKuaiZhuBiao"},
	                             	{id:"110185", name:"分类类型", type:"dict", key:"FenLeiLeiXing", 
	                             	 iconSkin:"icon-address_Book",entityName:"fenleileixing.FenLeiLeiXingZhuBiao"},
	                             	{id:"110186", name:"产品图纸设备类别", type:"dict", key:"ChanPinTuZhiSheBeiLeiBie", 
	                             	 iconSkin:"icon-address_Book",entityName:"chanpintuzhishebeileibie.ChanPinTuZhiSheBeiLeiBieZhuBiao"},
	                             	{id:"110187", name:"产品图纸压力表", type:"dict", key:"ChanPinTuZhiYaLiBiao", 
	                             	 iconSkin:"icon-address_Book",entityName:"chanpintuzhiyalibiao.ChanPinTuZhiYaLiBiaoZhuBiao"},
	                             	{id:"110188", name:"产品图纸排气液口", type:"dict", key:"ChanPinTuZhiPaiQiYeKou", 
	                             	 iconSkin:"icon-address_Book",entityName:"chanpintuzhipaiqiyekou.ChanPinTuZhiPaiQiYeKouZhuBiao"},
	                             	{id:"110189", name:"产品图纸介质毒性", type:"dict", key:"ChanPinTuZhiJieZhiDuXing", 
	                             	 iconSkin:"icon-address_Book",entityName:"chanpintuzhijiezhiduxing.ChanPinTuZhiJieZhiDuXingZhuBiao"},
	                             	{id:"110190", name:"产品图纸管口标准", type:"dict", key:"ChanPinTuZhiGuanKouBiaoZhun", 
	                             	 iconSkin:"icon-address_Book",entityName:"chanpintuzhiguankoubiaozhun.ChanPinTuZhiGuanKouBiaoZhunZhuBiao"},
	                             	{id:"110191", name:"产品图纸滤芯来源", type:"dict", key:"ChanPinTuZhiLvXinLaiYuan", 
	                             	 iconSkin:"icon-address_Book",entityName:"chanpintuzhilvxinlaiyuan.ChanPinTuZhiLvXinLaiYuanZhuBiao"},
	                             	{id:"110192", name:"客诉处理中心", type:"dict", key:"KeSuChuLiZhongXin", 
	                             	 iconSkin:"icon-address_Book",entityName:"kesuchulizhongxin.KeSuChuLiZhongXinZhuBiao"}
                                ]
					}
		];
		
		function onTreeClick(event, treeId, treeNode, clickFlag) {
			var entityName = treeNode.entityName;
			if(entityName) {
				var strUrl = "${ctx}/gs/gs-mng.action?entityName=" + entityName;
				$("#ifr_dicts").attr("src", strUrl);
			}
		}

		$(document).ready(function(){
			var ztreeObj = $.fn.zTree.init($("#dictsTree"), setting, zNodes);
			ztreeObj.expandAll(true);
		});            
</script>
</head>            
<body class="easyui-layout" fit="true">
          <div region="west" title="<s:text name='system.sysmng.dicts.title'/>" border="false"
                 split="true" style="width:215px;padding:0px;"
                 iconCls="icon-search">
               	<div><ul id="dictsTree" class="ztree"></ul></div>
          </div>
          <div region="center" title="" border="false">
                <iframe  id="ifr_dicts" scrolling="auto" frameborder="0" src=""  
                	style="width:100%;height:100%;overflow:hidden;visibility:visible;">
                </iframe>
          </div>
</body>
</html>