<%@ page contentType="text/html;charset=UTF-8" %>
<%@ include file="/common/taglibs.jsp" %>
<!DOCTYPE html>
<html>
<head>
<title><s:text name="system.index.title"/></title>
<%@ include file="/common/meta-gs.jsp" %>
<script type="text/javascript">
		function getOption() {
			return {
				title:'',
				iconCls:"icon-search",
				width:600,
				height:350,
				nowrap: false,
				striped: true,
				fit: true,
				url:'${ctx}/sys/log/change-log!queryList.action',
				sortName: 'id',
				sortOrder: 'desc',
				idField:'id',
				frozenColumns:[[
	                //{title:'<s:text name="system.search.id.title"/>',field:'id',width:50,sortable:true,align:'right'}
				]],
				columns:[[
							//{field:'moduleNameCn',title:'<s:text name="模块名"/>',width:80},
							//{field:'entityName',title:'<s:text name="实体名"/>',width:100},
							{field:'tableNameCn',title:'<s:text name="修订范围"/>',width:100},
							//{field:'entityId',title:'<s:text name="记录ID"/>',width:50},
							{field:'fieldNameCn',title:'<s:text name="修订字段"/>',width:100},
							{field:'beforeVal',title:'<s:text name="修订前值"/>',width:100},
							{field:'afterVal',title:'<s:text name="修订后值"/>',width:100},
							{field:'changeDesc',title:'<s:text name="变更详情"/>',width:150},
							//{field:'loginName',title:'<s:text name="操作账户"/>',width:80},
							//{field:'realName',title:'<s:text name="账户名称"/>',width:80},
							{field:'employeeName',title:'<s:text name="修订人"/>',width:80},
							//{field:'orgName',title:'<s:text name="所属组织"/>',width:80},
							{field:'operTime',title:'<s:text name="修订时间"/>',width:120,align:'center', sortable:true},
							{field:'operIp',title:'<s:text name="IP地址"/>',width:120}
						]],
				pagination:true,
				rownumbers:true
			};
		}
		
		function doQuery() {
			//var param = $("#queryForm").serializeArrayToParam();
			//$("#queryList").datagrid("load", param);
			var param = {
				"filter_EQS_moduleNameEn":"${param.mn}",
				"filter_EQS_entityName":"${param.en}",
				"filter_EQS_entityId":"${param.id}",
				"filter_EQS_fieldNameEn":"${param.fn}",
				"filter_EQS_subEntityId":"${param.sid}"
			};
			var opt = getOption();
			opt["queryParams"] = param;
			$("#queryList").edatagrid(opt);
		}

		$(document).ready(function() {
			<%-- focusEditor("moduleNameCn");
			doQuseryAction("queryForm");
			
			$("#queryList").datagrid(getOption());
			$("#operTimeStart").focus(function() {
				WdatePicker({maxDate:'#F{$dp.$D(\'operTimeEnd\')}'});
			});
			$("#operTimeEnd").focus(function() {
				WdatePicker({minDate:'#F{$dp.$D(\'operTimeStart\')}'});
			});
			$("#bt_query").click(doQuery);
			$("#bt_reset").click(function() {
				$("#queryForm")[0].reset();
				doQuery();
			});--%>
			doQuery();
		});
</script>
</head>
<body class="easyui-layout" fit="true">
		<%-- 
		<div region="west" border="false" title="<s:text name="system.search.title"/>" split="true" style="width:260px;padding:0px;" iconCls="icon-search">
			<form action="" name="queryForm" id="queryForm">
				<table border="0" cellpadding="0" cellspacing="1" class="table_form">
					<tbody>
						<tr><th><label for="moduleNameCn"><s:text name="模块名"/>:</label></th>
							<td><input type="text" name="filter_LIKES_moduleNameCn" id="moduleNameCn"></input></td>
						</tr>
						<tr><th><label for="loginName"><s:text name="登录账户"/>:</label></th>
							<td><input type="text" name="filter_LIKES_loginName" id="loginName"></input></td>
						</tr>
						<tr><th><label for="realName"><s:text name="账户名称"/>:</label></th>
							<td><input type="text" name="filter_LIKES_realName" id="realName"></input></td>
						</tr>
						<tr><th><label for="employeeName"><s:text name="员工姓名"/>:</label></th>
							<td><input type="text" name="filter_LIKES_employeeName" id="employeeName"></input></td>
						</tr>
						<tr><th><label for="orgName"><s:text name="所属组织"/>:</label></th>
							<td><input type="text" name="filter_LIKES_orgName" id="orgName"></input></td>
						</tr>
						<tr>	
							<th><label for="operTimeStart"><s:text name="变更时间"/>:</label></th>
							<td><input type="text" name="filter_GED_operTime" id="operTimeStart" readonly="readonly" class="Wdate" style="width:160px;"></input>
								<br/>--<br/><input type="text" name="filter_LED_operTime" id="operTimeEnd" readonly="readonly" class="Wdate" style="width:160px;"></input></td>
						</tr>
						<tr>		
							<td colspan="2" align="center">
								<button type="button" id="bt_query"><s:text name="system.search.button.title"/></button>&nbsp;&nbsp;
								<button type="button" id="bt_reset"><s:text name="system.search.reset.title"/></button>
							</td>
						</tr>
					</tbody>
				</table>
			</form>
		</div>--%>
		<div region="center" title="" border="false">
			<table id="queryList" border="false"></table>
		</div>
</body>
</html>
