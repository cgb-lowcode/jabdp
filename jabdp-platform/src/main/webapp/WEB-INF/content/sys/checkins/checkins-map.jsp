<%@ page contentType="text/html;charset=UTF-8"%>
<%@ include file="/common/taglibs.jsp"%>
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<meta name="viewport" content="initial-scale=1.0, user-scalable=no" />
	<style type="text/css">
		body, html,#allmap {width: 100%;height: 100%;overflow: hidden;margin:0;font-family:"微软雅黑";}
	</style>
	<script type="text/javascript" src="${ctx}/js/jquery/jquery-1.7.2.min.js"></script>
	<script type="text/javascript" src="http://api.map.baidu.com/api?v=2.0&ak=gOs6a1pGprSZiXz3N3OYniyO"></script>
	<script type="text/javascript" src="http://developer.baidu.com/map/jsdemo/demo/convertor.js"></script>
	<title>点击红色坐标签到</title>
</head>
<body>
	<div id="allmap"></div>
</body>
</html>
<script type="text/javascript">
var map = new BMap.Map("allmap");
var pt = {};
//向浏览器请求坐标
if(navigator.geolocation) {
	navigator.geolocation.getCurrentPosition(function(position) {
		var x = position.coords.longitude;
		var y = position.coords.latitude;
		baidu(x, y);
	});
} else {
	baidu(120.149261, 30.182236);
}
// 百度地图API功能
function baidu(x, y) {
	var gpsPoint = new BMap.Point(x, y);
	BMap.Convertor.translate(gpsPoint, 0, function(point) {
		pt = point;
		map.centerAndZoom(point, 15);
		var marker = new BMap.Marker(point);
		marker.addEventListener("click",attribute);
		map.addOverlay(marker);
	});
}
//获取覆盖物位置
function attribute(e) {
	var geoc = new BMap.Geocoder();
	geoc.getLocation(pt, function(rs){
		var addComp = rs.addressComponents;
		var address = addComp.province + ", " + addComp.city + ", " + addComp.district + ", " + addComp.street + ", " + addComp.streetNumber;
		var sContent = "<div>"
			+ "<p style='margin:0;line-height:1.5;font-size:13px;text-indent:2em'>"
			+ "签到主题:" 
			+ "<input type='text' id='qiandaozhuti'/>"
			+ "</p>"
			+ "<p style='margin:0;line-height:1.5;font-size:13px;text-indent:2em'>"
			+ "当前位置:" 
			+ "<input type='text' id='qiandaoweizhi' readonly='readonly' value='" + address + "'/>"
			+ "</p>"
			+ "<p style='margin:0;line-height:1.5;font-size:13px;text-indent:2em'>"
			+ "签到内容:" 
			+ "<input type='text' id='qiandaoneirong'/>"
			+ "</p>"
			+ "<p>"
			+ "<button type='button' onclick='doCheckins();'>确认签到</button>"
			+ "</p>"
			+ "</div>";
		var infoWindow = new BMap.InfoWindow(sContent,{
		    width : 400, // 信息窗口宽度
		    height: 200, // 信息窗口高度
		    title : "签到窗口", // 信息窗口标题
		    enableMessage : false // 设置禁止信息窗发送短息
		});  // 创建信息窗口对象
		map.openInfoWindow(infoWindow, pt);
	});
}
//提交签到
function doCheckins() {
	$.ajax({
		url : "${ctx}/sys/checkins/checkins!save.action",
		data : {
			lng : pt.lng,
			lat : pt.lat,
			title : $("#qiandaozhuti").val(),
			palce : $("#qiandaoweizhi").val(),
			content : $("#qiandaoneirong").val()
		},
		success : function(data) {
			if(confirm("签到成功，返回个人信息页面？")) {
				window.location.href = "${ctx}/index-mobile.action";
			}
		}
	});
}
</script>