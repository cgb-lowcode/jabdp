<%@ page contentType="text/html;charset=UTF-8"%>
<%@ include file="/common/taglibs.jsp"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title><s:text name="system.index.title" />
</title>
<%@ include file="/common/meta.jsp"%>
<script type="text/javascript">
$(document).ready(function() {
	
	$("#file_upload").uploadify({
				'uploader' : '${ctx }/sys/process/deploy!deploy.action',
				'swf' : '${ctx}/js/uploadify/uploadify-3.1.swf',
				'cancelImg' : '${ctx}/js/uploadify/uploadify-cancel-3.1.png',
				'queueID' : 'uploadGsQueue',
				'auto' : true,
				'multi' : true,
				'width' : 95,
				'height' : 20,
				'buttonClass' : 'uploadify-button-small',
				'fileSizeLimit' : '5MB', //限制上传的文件大小
				'fileTypeExts' : '*.bar;*.zip;*.bpmn;*.bpmn20.xml;', //允许的格式 zip、bar、bpmn、bpmn20.xml
				'onUploadSuccess' : function(file,
						data, response) {
					list();
					
				},
				'onUploadError' : function(file,errorCode, errorMsg,errorString) {
					$.messager.alert("提示信息", '文件'
							+ file.name
							+ '上传失败，错误信息如下：'
							+ errorString,
							"warning");
				}			
			});
});
function list(){
	window.location.replace("${ctx}/sys/process/deploy!list.action");
}
function doDel(id){
	var options = {
			url : '${ctx}/sys/process/deploy!delete.action',
			data : {
				"deploymentId" :id
			},
			success : function(data) {
				if (data.msg) {
					$.messager
							.alert(
									'<s:text name="system.javascript.alertinfo.title"/>',
									'删除成功',
									'info'); 
					list();
				}
			}
		};
	fnFormAjaxWithJson(options);
}
</script>
</head>
<body >

	<div class="file_upload_button">
			<input type="file" name="uploadify_gs" id="file_upload" />
			<span class="upload-button-3">[注：支持文件格式：zip、bar、bpmn、bpmn20.xml]</span>
		</div>
		<div id="uploadGsQueue" class="file_upload_queue"></div>
	<div>
			<table   border="2" width="100%" class="need-border">
		<thead>
			<tr>
				<td>名称</td>
				<td>KEY</td>
				<td>XML</td>
				<td>图片</td>
				<td>操作</td>
			</tr>
		</thead>
		<tbody>
			<c:forEach items="${processes }" var="process">
				<tr>
					<td>${process.name }</td>
					<td>${process.key }</td>
					<td>${process.resourceName }</td>
					<td>${process.diagramResourceName }</td>
					<td><a href="javascript:void(0);" onclick="doDel(${process.deploymentId})">删除</a></td>
				</tr>
			</c:forEach>
		</tbody>
	</table>
	</div>
</body>
</html>

