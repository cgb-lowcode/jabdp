<%@ page contentType="text/html;charset=UTF-8"%>
<%@ include file="/common/taglibs.jsp"%>
<!DOCTYPE html>
<html>
<head>
<title><s:text name="system.sysmng.process.processRunningMng.title"/></title>
<%@ include file="/common/meta-gs.jsp"%>

<script>
    
    var _userList={};
    var _userLoginNameList = {};
    
	$(document).ready(function() {
		//_userList = findAllUser(); 
		//_userLoginNameList = getUserLoginNameList(_userList);
		$("#queryList").datagrid(getOption());
		$("#bt_query").click(doQuery);
		$("#bt_reset").click(function() {
			$("#queryForm").form("clear");
			doQuery();
		});
	});
	
	function getUserLoginNameList(ulist) {
		var uMap = {};
		$.each(ulist, function(k,uObj) {
			var nickName = (uObj.nickName)?("-" + uObj.nickName):"";
			uMap[uObj.loginName] = uObj.realName + "[" + uObj.loginName + "]" + nickName;
		});
		return uMap;
	}
	
	function doQuery() {
		var param = $("#queryForm").serializeArrayToParam();
		$("#queryList").datagrid("load", param);
	}
	//查看运行中的流程信息
	function getOption() {
		return {
			iconCls:"icon-search",
			width:600,
			height:350,
			border:false,
			nowrap: false,
			striped: true,
			fit: true,
			sortName: 'createTime',
			sortOrder: 'desc',
			url:'${ctx}/gs/process!getAllProcessInstance.action',
			columns:[[
						{field:'moduleName',title:'<s:text name="system.sysmng.process.moduleName.title"/>',width:80},
						{field:'name',title:'<s:text name="system.sysmng.process.taskName.title"/>',width:300,
							formatter :  function(value, rowData, rowIndex) {	
								var newName = value;
								if(rowData.field) {
									newName = value+'-'+rowData.field;
								}
								return newName;
						}},
						{field:'assignee',title:'<s:text name="system.sysmng.process.dealUser.title"/>',width:100,
							formatter:function(value,rowData,rowIndex){
								/*if(value){
									return _userLoginNameList[value];
								}else{
									return "<s:text name='system.sysmng.process.notSign.title'/>";
								}*/
								return rowData["assigneeName"];
							}
						},
						{field:'processInstanceId',title:'<s:text name="system.sysmng.process.processInstanceID.title"/>',width:70},
						{field:'createTime',title:'<s:text name="system.sysmng.user.createtime.title"/>',width:130,sortable:true},
						{field:'startUser',title:'<s:text name="system.sysmng.process.startUser.title"/>',width:100,sortable:false,
							 formatter:function(value, rowData, rowIndex) {
								 /*var val = _userLoginNameList[value];
								 if(val) {
									 return val;
								 } else {
									 return value;
								 }*/
								 return rowData["startUserName"];
						}},
						{field:'suspensionState',title:'<s:text name="system.sysmng.process.suspensionState.title"/>',width:80,
							formatter:function(value,rowData,rowIndex){
								if(value=="1"){
									return "<s:text name='system.sysmng.process.running.title'/>...";
								}else if(value=="2"){
									return "<s:text name='system.sysmng.process.suspension.title'/>...";
								}
							}
						},
						{
							field : 'oper',
							title : '<s:text name="system.button.oper.title"/>',
							width : 200,
							align : 'left',
							formatter : operFormatter
						}
						]],
						pagination:true,
						rownumbers:true,
		
			onDblClickRow :function(rowIndex, rowData){
				doView(rowData.processInstanceId);
			}
		};
	}
	
	function operFormatter(value, rowData, rowIndex){
		var strArr = [];
		strArr.push('<a href="javascript:void(0);" onclick="doView(');
		strArr.push(rowData.processInstanceId);
		strArr.push(')"><s:text name="system.sysmng.process.searchProcessInfo.title"/></a>');
		//if(rowData.assignee){
			strArr.push('&nbsp;<a href="javascript:void(0);" onclick="doSetAssignee(');
			strArr.push(rowData.id);
			strArr.push(')"><s:text name="system.sysmng.process.setAssignee.title"/></a>');
		//}	
		if(rowData.suspensionState=="1"){
		strArr.push('&nbsp;<a href="javascript:void(0);" onclick="doSuspend(');
		strArr.push(rowData.processInstanceId);
		strArr.push(')"><s:text name="system.sysmng.process.suspensionProcess.title"/></a>');
		}else if(rowData.suspensionState=="2"){
			strArr.push('&nbsp;<a href="javascript:void(0);" onclick="doActivate(');
			strArr.push(rowData.processInstanceId);
			strArr.push(')"><s:text name="system.sysmng.process.activateProcess.title"/></a>');
		}
		strArr.push('&nbsp;<a href="javascript:void(0);" onclick="doDel(');
		strArr.push(rowData.processInstanceId);
		strArr.push(')"><s:text name="结束流程"/></a>');
		return strArr.join("");
	}
	function doDel(processInstanceId){
		var options = {
				url : '${ctx}/gs/process!endProcessInstance.action',
				data : {
					"processInstanceId" :processInstanceId
				},
				success : function(data) {
					if (data.msg) {
						$("#queryList").datagrid(getOption());
					}
				}
			};
		fnFormAjaxWithJson(options);
	}
	function doView(pId){
		top.addTab('<s:text name="system.sysmng.process.searchProcessInfo.title"/>-'+pId, 
				'${ctx}/gs/process!getProcess.action?processInstanceId=' + pId + '&isRuningMng=true');
		<%--
		var content = [];
		content.push("<iframe src='${ctx}/gs/process!getProcess.action?processInstanceId=");
		content.push(pId);
		content.push("&isRuningMng=");
		content.push(true);
		content.push("' frameborder='0' style='width:100%;height:100%'></iframe>");
		$("#tt").tabs("add", {
			title : '<s:text name="流程详情"/>-' + pId+ '',
			content : content.join(""),
			closable : true,
			cache : false
		});--%>
	}
	function doSetAssignee(taskId){
	var content ='<iframe  src="${ctx}/gs/process!passOn.action?taskId='+taskId+'" frameborder="0" style="width:100%;height:100%"></iframe>';
		var p =	$("<div/>").appendTo("body");
		p.window({
			id:'pass_win',
			title:'<s:text name="system.sysmng.process.setPassOnUser.title"/>',
			content:content,
			width:400,
			height:120,
			closed:true,
			modal:true,
			onClose:function() {
				$("#pass_win").window("destroy");
			}
		});
		 $("#pass_win").window("open");
	}
	function doSuspend(pId){
		var options = {
				url : '${ctx}/gs/process!suspendProcess.action',
				data : {
					"processInstanceId" :pId
				},
				success : function(data) {
					if (data.msg) {
						$("#queryList").datagrid(getOption());
					}
				}
			};
		fnFormAjaxWithJson(options);
	}
	function doActivate(pId){
		var options = {
				url : '${ctx}/gs/process!activateProcess.action',
				data : {
					"processInstanceId" :pId
				},
				success : function(data) {
					if (data.msg) {
						$("#queryList").datagrid(getOption());
					}
				}
			};
		fnFormAjaxWithJson(options);
	}
</script>
</head>
<body class="easyui-layout">
	<div region="west" border="false" title="<s:text name="system.search.title"/>" split="true" style="width:260px;padding:0px;" iconCls="icon-search">
		<form action="" name="queryForm" id="queryForm">
				<table border="0" cellpadding="0" cellspacing="1" class="table_form">
					<tbody>
						<tr><th><label for="assignee"><s:text name="system.sysmng.process.dealUser.title"/>:</label></th>
							<td><input type="text" name="filter_LIKES_assignee" id="assignee" class="Itext" placeholder="用户登录名模糊查询"></input></td>
						</tr>
						<tr><th><label for="name"><s:text name="任务名"/>:</label></th>
							<td><input type="text" name="filter_LIKES_name" id="name" class="Itext" placeholder="任务名模糊查询"></input></td>
						</tr>
						<tr><th><label for="processInstanceId"><s:text name="流程实例ID"/>:</label></th>
							<td><input type="text" name="filter_EQS_processInstanceId" id="processInstanceId" class="Itext" placeholder="流程实例ID精确查询"></input></td>
						</tr>
						<tr>		
							<td colspan="2" align="center">
								<button type="button" id="bt_query"><s:text name="system.search.button.title"/></button>&nbsp;&nbsp;
								<button type="button" id="bt_reset"><s:text name="system.search.reset.title"/></button>
							</td>
						</tr>
					</tbody>
				</table>
			</form>
	</div>
	<div region="center" title="<s:text name=""/>" border="false">		
	<%--<div id="tt" class="easyui-tabs" fit="true" border="false">
		<div  title="<s:text name="运行中流程信息"/>" >--%>
		<table id="queryList"></table>
	<%-- </div>
		</div>--%>
	</div>	
</body>
</html>