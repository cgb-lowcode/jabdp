<%@ page contentType="text/html;charset=UTF-8"%>
<%@ include file="/common/taglibs.jsp"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title><s:text name="system.index.title" /></title>
<%@ include file="/common/meta-min.jsp"%>
<link rel="stylesheet" type="text/css" href="${ctx}/js/easyui/blue/easyui.css"/>
<script type="text/javascript" src="${ctx}/js/easyui/scripts/jquery.easyui.min.js"></script>
<script type="text/javascript" src="${ctx}/js/easyui/scripts/locale/easyui-lang-${locale}.js"></script>
<script type="text/javascript" src="${ctx}/js/common/scripts/common.js"></script>
<link href="${ctx}/js/loadmask/jquery.loadmask.css" rel="stylesheet" type="text/css"/>
<script type="text/javascript" src="${ctx}/js/loadmask/jquery.loadmask.min.js"></script>
<script src="${ctx}/js/qtip/jquery.qtip.min.js" type="text/javascript"></script>
<link href="${ctx}/js/qtip/jquery.qtip.min.css" rel="stylesheet" type="text/css"/>
<script src="${ctx}/js/html/jquery.outerhtml.js" type="text/javascript"></script>
<link href="${ctx}/js/html/style.css" rel="stylesheet" type="text/css"/>
<style type="text/css">
body,th{text-align: center;}
.ui-tooltip, .qtip{
	max-width: 800px;
}
</style>
<script type="text/javascript">
	$(function() {
		<c:if test="${not empty param.processInstanceId}">
		function loadProcessInfo(infos) {
	        var positionHtml = [];
			var minX = 2147483647;
			var minY = 2147483647;
	        $.each(infos, function(i, v) {
				if(v.x <= minX) {
					minX = v.x;
				}
				if(v.y <= minY) {
					minY = v.y;
				}
	        });
	        // 生成图片
	        var varsArray = new Array();
	        $.each(infos, function(i, v) {
	            var $positionDiv = $('<div/>', {
	                'class': 'activiyAttr'
	            }).css({
	                position: 'absolute',
	                left: (v.x - minX + 4),
	                top: (v.y - minY + 4),
	                width: (v.width -1),
	                height: (v.height -1)
	            });
	            if (v.currentActiviti) {
	                $positionDiv.addClass('ui-corner-all-12').css({
	                    border: '3px solid red'
	                });
	            } else {
					if(v.vars && v.vars["hisInfo"] && v.vars["hisInfo"].length > 0) {
						$positionDiv.addClass('ui-corner-all-12').css({
		                    border: '3px solid #7fff00'
		                });
					}
		        }
	            positionHtml.push($positionDiv.outerHTML());
	            varsArray[varsArray.length] = v.vars;
	        });
	        
	        $('#processImageBorder').html(positionHtml.join(""));
	        
	        // 设置每个节点的data
	        $('.activiyAttr').each(function(i, v) {
	            $(this).data('vars', varsArray[i]);
	        });

	     // 此处用于显示每个节点的信息，如果不需要可以删除
            $('.activiyAttr').qtip({
                content: function() {
                    var vars = $(this).data('vars');
                    var tipContent = [];
                    tipContent.push("<table class='table_form'><tbody>");
                    $.each(vars, function(varKey, varValue) {
                        if (varKey && varKey!="hisInfo" && varValue) {
                        	tipContent.push("<tr><th width='30%'>");
                        	tipContent.push(varKey);
                        	tipContent.push("</th><td>");
                        	tipContent.push(varValue);
                        	tipContent.push("</td></tr>");
                        }
                    });
                    tipContent.push("</tbody></table>");
                    var hisInfo = vars["hisInfo"];
                    if(hisInfo && hisInfo.length > 0) {
                    	tipContent.push("<table class='table_form'>");
                    	tipContent.push("<thead><tr>");
                    	tipContent.push("<th style='text-align:center;'><s:text name='system.sysmng.process.dealUser.title'/></th>");
                    	tipContent.push("<th style='text-align:center;'><s:text name='system.sysmng.process.dealTime.title'/></th>");
                    	tipContent.push("<th style='text-align:center;'><s:text name='system.sysmng.process.isApprove.title'/></th>");
                    	tipContent.push("<th style='text-align:center;'><s:text name='system.sysmng.process.reason.title'/></th>");
                    	tipContent.push("<th style='text-align:center;'><s:text name='system.sysmng.process.applyTimes.title'/></th>");
                    	tipContent.push("</tr></thead><tbody>");
						$.each(hisInfo, function(i, v) {
							tipContent.push("<tr><td>");
                            tipContent.push(v.dealName);
                            tipContent.push("</td><td>");
                            tipContent.push(v.dealTime);
                            tipContent.push("</td><td>");
							switch(v.isApprove) {
								case "0":tipContent.push("不同意");break;
								case "1":tipContent.push("同意");break;
								default: tipContent.push("");
							}
                            tipContent.push("</td><td>");
                            tipContent.push(v.reason);
                            tipContent.push("</td><td>第");
                            tipContent.push(v.countVal);
                            tipContent.push("次</td></tr>");
                        });
						tipContent.push("</tbody><table>");
                    }
                    return tipContent.join("");
                },
                position: {
                    my: 'top left',
                    at: 'bottom center'
                },
                style: {
                    width: 400,
                    classes: 'ui-tooltip-light ui-tooltip-rounded ui-tooltip-shadow'
                }
            });
	        
		}

		var options = {
				url : '${ctx}/gs/process!traceProcess.action',
				data : {
					"processInstanceId":"${param.processInstanceId}"
				},
				success : function(data) {
					if (data.msg) {
						loadProcessInfo(data.msg);
					}
				}
		};
		fnFormAjaxWithJson(options);
		</c:if>
	});
</script>
</head>  
<body>
	<div class="ui-corner-all-12" style="border:1px solid #BCF5A9;text-align:center;margin:auto;font-size:14px;">
	<div style="height:25px;padding:10px 20px;">
		 <div class="ui-corner-all-12" style="width:20px;height:20px;border:3px solid red;float:left;"></div><div style="float:left;height:20px;padding:0px 50px 0px 10px"><s:text name="system.sysmng.process.executing.title"/></div>     
		 <div class="ui-corner-all-12" style="width:20px;height:20px;border:3px solid #7fff00;float:left;"></div><div style="float:left;height:20px;padding:0px 10px 0px 10px"><s:text name="system.sysmng.process.executed.title"/></div>
		 <div style="float:left;padding-left:20px;color:red;">（请将鼠标移到对应环节查看详情）</div>
		 <div style="clear:both;"></div>
	</div>
	</div>
	<div style="position:relative;margin:12px 12px;">
		<img src="${ctx}/gs/process!loadProcessDefinition.action?processInstanceId=${param.processInstanceId}&entityName=${param.entityName}" style="position:absolute; left:0px; top:0px;"/>
		<div id="processImageBorder"></div>
	</div>
</body>
</html>