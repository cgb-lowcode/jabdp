<%@page import="java.util.Random"%>
<%@ page language="java" contentType="text/html; charset=utf-8"
    pageEncoding="utf-8"%>
<%@ include file="/common/taglibs.jsp"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>iframe</title>
</head>
<body>

<%
    Random rad=new Random();
    int radParam=rad.nextInt();
    String id=request.getParameter("id");
    request.setAttribute("id",id);
%>

<script type="text/javascript">
	$(function(){
		 var id=${param.id};
		initContent("iframe",id,"ifm_<%=radParam%>");
		 
	});
</script>
    <iframe id="ifm_<%=radParam%>"  src="" 
                   width="100%" 
                   height="100%"
                   frameborder="0"
				   scrolling='auto'>
    </iframe> 
</body>
</html>