<%@ page contentType="text/html;charset=UTF-8"%>
<%@ include file="/common/taglibs.jsp"%>
<!DOCTYPE html>
<html>
<head>
 <title><s:text name="system.index.title"/></title>
 <%@ include file="/common/meta-css.jsp"%>
</head>
<body>
<div class="easyui-layout" style="width:100%;height:500px;" data-options="fit:true">
        <div data-options="region:'west',border:false" style="width:48%;padding:4px;">
           <div class="easyui-layout" data-options="fit:true">
			 <div data-options="region:'center',border:false">
			  <table id="_datalist_" ></table>
			 </div>
			</div>
        </div>
        <div data-options="region:'center',border:false,split:true" style="width:48%;padding:4px;">
        	<div class="easyui-layout" data-options="fit:true">
			 <div data-options="region:'center',border:false">
			  <table id="_datalist_sel_" ></table>
			 </div>
			</div>
        </div>
</div>
<div id="tb" style="padding:2px 5px;">
        <input class="easyui-searchbox" data-options="prompt:'请输入登录名、登录账户、姓名、组织名称查询',searcher:doSearch" style="width:100%;"></input>
        <a href="#" class="easyui-linkbutton" iconCls="icon-add" title="移至右边" plain="true" onclick="doAddAllSel();">移至选中列表</a>
        <span style="color:red;font-size:10px;">【双击数据可以移至选中列表】</span>
</div>
<div id="tb1" style="padding:2px 5px;">
        <input class="easyui-searchbox" data-options="prompt:'请输入登录名、登录账户、姓名、组织名称查询',searcher:doSearchSel" style="width:100%;"></input>
        <a href="#" class="easyui-linkbutton" iconCls="icon-remove" title="全部清空" plain="true" onclick="doClearAllSel();">全部清空</a>
        <span style="color:red;font-size:10px;">【双击移除选中数据】</span>
</div>
<div style="display:none;">
<textarea id="_query_params_">${param.queryParams}</textarea>
</div>
<%@ include file="/common/meta-js.jsp"%>
<script type="text/javascript">
var filter = {
	"filter_EQS_status":"1"
};
//获取值
function getValue() {
	var rows = $("#_datalist_sel_").datagrid("getRows");
	if(rows && rows.length) {
		var keys = [], captions = [];
		for(var i in rows) {
			var row = rows[i];
			keys.push(row["id"]);
			captions.push(row["nickName"]);
		}
		return {
			key : keys.join(","),
			caption : captions.join(",")
		};
	} else {
		return null;
	}
}
//初始化结果窗
function initDatagrid(items) {
	var filters = $.extend({}, filter);
	var queryParams = $("#_query_params_").val();
	if(queryParams) {
		$.extend(filters, JSON.parse(queryParams));
	}
	var fields = [{
		field : "ck",
		checkbox : true
	}];
	for(var i in items) {
		var field = {
			field : items[i].key,
			title : items[i].caption,
			width : items[i].width ? items[i].width : 100
		};
		if(items[i].order == 'hidden') field.hidden = true;
		fields.push(field);
	}
	$("#_datalist_").datagrid({
		title:"请选择数据",
		fit:true,
		url : "${ctx}/sys/account/user!queryUserPage.action?entityName=User",
		queryParams : filters,
		pagination : true,
		pageList:[10,20,30,40,50,100,200,500],
		pageSize:10,
		rownumbers :true,
		striped : true,	
		nowrap : false, // 截取
		idField : 'id',
		columns : [fields],
		toolbar:'#tb',
		onDblClickRow:function(index, row) {
			doAddSelToSelList([row]);
			$('#_datalist_').datagrid('unselectRow',rowIndex);
		}
	});
}

function doSearch(value) {
	$("#_datalist_").edatagrid("load", {
		"filter_LIKES_loginName_OR_realName_OR_nickname_OR_com.qyxx.platform.sysmng.accountmng.entity.Organization$$$organization.organizationName":value
	});
}
//搜索选择数据
function doSearchSel(value) {
	if(value) {
		var selRows = $("#_datalist_sel_").datagrid("getRows");
		var fields = $("#_datalist_sel_").datagrid("getColumnFields");
		if(selRows && selRows.length) {
			$.each(selRows, function(i, row) {
				for(var j in fields) {
					var field = fields[j];
					if(field != "id") {
						var fieldVal = String(row[field]);
						if(fieldVal.indexOf(value) >= 0) {
							$("#_datalist_sel_").datagrid("selectRow", i);
							break;
						}
					}
				}
			});
		}
	}
}

function doAddAllSel() {
	var rows = $("#_datalist_").datagrid("getSelections");
	if(rows.length) {
		doAddSelToSelList(rows);
	} else {
		$.messager.confirm('提醒', '您当前没有选中任何数据，确认要默认选中当页所有数据么？', function(r){
            if (r){
            	var rows = $("#_datalist_").datagrid("getRows");
            	doAddSelToSelList(rows);
            }
        });
	}
}

function doAddSelToSelList(rows) {
	var selRows = $("#_datalist_sel_").datagrid("getRows");
	var selRowIds = $.map(selRows, function(row) {
		return row["id"];
	});
	for(var i in rows) {
		var row = rows[i];
		if($.inArray(row["id"], selRowIds) == -1) {
			$("#_datalist_sel_").datagrid("appendRow", row);
		}
	}
}

function doClearAllSel() {
	$("#_datalist_sel_").datagrid("loadData", []);
}

function initSelDatagrid(items) {
	var filters = {"filter_INL_id":"${param.key}" || "0"};
	var fields = [{
		field : "ck",
		checkbox : true
	}];
	for(var i in items) {
		var field = {
			field : items[i].key,
			title : items[i].caption,
			width : items[i].width ? items[i].width : 100
		};
		if(items[i].order == 'hidden') field.hidden = true;
		fields.push(field);
	}
	$("#_datalist_sel_").datagrid({
		title:"选中数据",
		fit:true,
		url : "${ctx}/sys/account/user!queryUser.action?entityName=SelUser",
		queryParams : filters,
		pagination : false,
		rownumbers :true,
		striped : true,	
		nowrap : false, // 截取
		idField : 'id',
		columns : [fields],
		toolbar:'#tb1',
		onDblClickRow:function(index, row) {
			$("#_datalist_sel_").datagrid("deleteRow", index);
		}
	});
}

$(function(){
	var items = [{"key":"id","caption":"编号","order":"hidden"}, 
	             {"key":"loginName","caption":"登录名"},
	             {"key":"nickName","caption":"姓名"},
	             {"key":"orgName","caption":"组织"},
	             {"key":"realName","caption":"登录账户"}
				];
	initDatagrid(items);
	initSelDatagrid(items);
});
</script>
</body>
</html>