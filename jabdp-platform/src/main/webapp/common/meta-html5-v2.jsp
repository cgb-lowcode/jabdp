<!--[if lt IE 9]>
  <script>
    (function(){
      var ef = function(){};
      window.console = window.console || {log:ef,warn:ef,error:ef,dir:ef};
    }());
  </script>
  <script src="${ctx}/js/html5/html5shiv.min.js"></script>
  <script src="${ctx}/js/html5/html5shiv-printshiv.min.js"></script>
  <script src="${ctx}/js/html5/es5-shim.min.js"></script>
  <script src="${ctx}/js/html5/es5-sham.min.js"></script>
<![endif]-->
<%-- 
<script>
    if (typeof Promise !== "function")
        document.write('<script src="${ctx}/jslib/html5/es6-promise.min.js"><\/script>');
</script>--%>
<script>
	var __ctx='${ctx}';
	var $curUserId$ = "${sessionScope.USER.id}";
	var $curUserLoginName$ = "${sessionScope.USER.loginName}";
	var $curOrgId$ = "${sessionScope.USER.organization.id}";
	var $curOrgName$ = "${sessionScope.USER.organization.organizationName}";
	var $curOrgCode$ = "${sessionScope.USER.orgCode}";
	var $curUserEmployeeId$ = "${sessionScope.USER.employeeId}";
	var $curUserName$ = "${sessionScope.USER.nickname}";
	var $curUserAccount$ = "${sessionScope.USER.realName}";
  	var $today$ = "${today}";
</script>