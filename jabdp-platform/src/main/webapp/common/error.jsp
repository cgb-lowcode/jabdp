<%@ page contentType="text/html;charset=UTF-8"%>
<%@ include file="/common/taglibs.jsp" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<title>系统错误</title>
	<script src="${ctx}/js/jquery/jquery-1.4.4.min.js" type="text/javascript"></script>
	<script type="text/javascript">
		$(document).ready(function() {
			$("#a_view_error").click(function() {
				$('#dv_error_msg').show();
			});
		});
	</script>
</head>
<body>
<div><h1><s:property value="exception.message" /></h1></div>
<div><a href="javascript:history.go(-1);">返回</a>&nbsp;&nbsp;<a id="a_view_error" href="javascript:void(0);">查看详细错误信息</a></div>
<div id="dv_error_msg" style="display:none;">
	<pre>
		<s:property value="exceptionStack" />
	</pre>
</div>
</body>
</html>
