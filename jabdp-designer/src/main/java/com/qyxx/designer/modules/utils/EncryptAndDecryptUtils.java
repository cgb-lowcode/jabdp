package com.qyxx.designer.modules.utils;

import java.io.File;
import java.io.IOException;

import org.apache.commons.io.FileUtils;
import org.apache.commons.lang.StringUtils;
import org.springframework.context.support.StaticApplicationContext;

import com.qyxx.designer.miniweb.service.ServiceException;

/**
 * 加解密工具类<br>
 *
 * @Author:xw
 * 
 *
 */
public class EncryptAndDecryptUtils {
	//默认密码
	private static final String DEFAULT_KEY="1234";
	//加密标识
	private static final String XML_ENCRYPT_SUFFIX = "jabdp";
	
	/**
	 * AES加密，并转化为16进制
	 *
	 * 
	 *
	 * @param value
	 * 					待加密内容
	 * @param key
	 * 					秘钥，如果为空则使用默认密钥
	 * @return          
	 *                  密文，并且加上加密标识
	 */
	public static String aesEncrypt(String value,String key ){
		if(StringUtils.isBlank(key)){
			return  value;
		}else{
			byte[] encrypt;
			String result = null;
			try {
				if (value != null && !"".equals(value.trim())) {             //value is not null
					encrypt = AESUtils.encrypt(value, key);               //加密
					result = Encodes.parseByte2HexStr(encrypt);          //2进制转换为16进制
					result += XML_ENCRYPT_SUFFIX;                        //给密文加上已经加密过的标识
				}
			} catch (Exception e) {
				e.printStackTrace();
			}

			return result;
		}


	}
	
	/**
	 * 先转换为2进制，然后AES解密
	 * 
	 * 
	 *
	 * @param value
	 * 				待解密内容
	 * @param key
	 * 				秘钥，如果为空则使用默认密钥
	 * @return
	 *              解密后内容 ，如果没有加密标识，则返回原文
	 */
	public static String aesDecrypt(String value , String key){
		if(value.endsWith(XML_ENCRYPT_SUFFIX) == true){                    //判断有没有加密标识
			String envalue =  value.split("jabdp")[0];                     //去掉加密标识
			key = key == null ? DEFAULT_KEY: key;                          
			byte[] twoStrValue= null;
			byte[] decrypt = null;
			String result = null;
			try {
				if(value != null && !"".equals(value.trim())){	
					twoStrValue      = Encodes.parseHexStr2Byte(envalue);         //16进制转换成2进制
					decrypt = AESUtils.decrypt(twoStrValue,key);                  //解密
					 result = new String(decrypt,"UTF-8");	      //转字符串
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
				
			return result;
		}else{
			
			return value;
		}
	}
	
	public final static String ENCODING = "UTF-8";
	public static void main(String[] args) throws IOException {

      
//        /*读xml文件*/
//        File file = new File("E:/menu.xml");
//        String xml = FileUtils.readFileToString(file, "UTF-8");
//        System.out.println("加密之前XML：" + xml);
//
//        // 加密
//        String encrypt = aesEncrypt(xml, "");
//        System.out.println("加密后的内容：" + encrypt);
//        
//        /*写进xml文件*/
//        File enfile = new File("E:/ENmenu.xml");        
//        FileUtils.writeStringToFile(enfile, encrypt, "UTF-8");
        
        
        /*读取加密xml文件*/
		//String filePath = "H:/tools/jabdp-v1.2.1/webapps/iDesigner/local/20180707140053642/config/";
      
		//String xml = null;
		
			//File enFile = new File(filePath+ "enmenu.xml");

			
				//String enxml = FileUtils.readFileToString(enFile, ENCODING);
				//System.out.println("解密前"+enxml);
				

			
       
		
}
}
