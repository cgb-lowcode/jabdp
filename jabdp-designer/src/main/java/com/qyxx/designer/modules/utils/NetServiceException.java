package com.qyxx.designer.modules.utils;

/**
 *  网络处理异常类
 *  
 *  @author gxj
 *  @version 1.0 2012-6-28 下午04:12:37
 *  @since jdk1.6
 */
public class NetServiceException extends RuntimeException {

	private static final long serialVersionUID = 3583566093089790852L;

	public NetServiceException() {
		super();
	}

	public NetServiceException(String message) {
		super(message);
	}

	public NetServiceException(Throwable cause) {
		super(cause);
	}

	public NetServiceException(String message, Throwable cause) {
		super(message, cause);
	}
}
