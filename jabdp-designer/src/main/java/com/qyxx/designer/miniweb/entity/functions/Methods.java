package com.qyxx.designer.miniweb.entity.functions;

import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 *  树形一级子节点
 *  
 *  @author thy
 *  @version 1.0 
 *  @since jdk1.6
 */

@XmlAccessorType(XmlAccessType.FIELD)
public class Methods {
	@XmlAttribute
	private String key;
	
	@XmlAttribute
	private String caption;
	
	@JsonProperty("children")
	@XmlElement
	private List<Method> method = new ArrayList<Method>();
	
	
	@XmlElement
	private String description ;

	public String getKey() {
		return key;
	}

	public void setKey(String key) {
		this.key = key;
	}

	public String getCaption() {
		return caption;
	}

	public void setCaption(String caption) {
		this.caption = caption;
	}

	public List<Method> getMethod() {
		return method;
	}

	public void setMethod(List<Method> method) {
		this.method = method;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	

	
	
}
