package com.qyxx.designer.miniweb.cache;

import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;

import org.apache.commons.lang.StringUtils;

import com.google.common.collect.Lists;
import com.qyxx.jwp.bean.Field;
import com.qyxx.jwp.bean.Form;
import com.qyxx.jwp.bean.Item;
import com.qyxx.jwp.bean.Module;
import com.qyxx.jwp.datasource.TeAttr;

/**
 * 模块缓存
 * 
 * @author gxj
 * @version 1.0 2013-3-15 上午10:51:40
 * @since jdk1.6
 */
public class ModuleEntityCache {

	private static final String NAME_SPLIT_SYMBOL = "--";
	private static final String KEY_SPLIT_SYMBOL = ".";
	
	private static ModuleEntityCache mec;
	private static String[] SYSTEM_COLUMN = { "ID", "VERSION",
			"CREATE_USER", "CREATE_TIME", "LAST_UPDATE_USER",
			"LAST_UPDATE_TIME", "STATUS", "SORT_ORDER_NO", "ATM_ID",
			"FLOW_INS_ID" };
	private static String[] SYSTEM_KEY = {"id","version","createUser","createTime",
			"lastUpdateUser","lastUpdateTime","status","sortOrderNo", "atmId", "flowInsId"};
	private static String[] SYSTEM_CAPTION = { "编号", "版本号", "创建者",
			"创建时间", "最后修改者", "最后修改时间", "状态", "排序号", "附件编号", "流程实例编号" };

	private ConcurrentMap<String, ConcurrentMap<String, Module>> meMap;

	private ModuleEntityCache() {
		meMap = new ConcurrentHashMap<String, ConcurrentMap<String, Module>>();
	}

	public static synchronized ModuleEntityCache getInstance() {
		if (null == mec) {
			mec = new ModuleEntityCache();
		}
		return mec;
	}

	public void put(String instanceId, ConcurrentMap<String, Module> fe) {
		this.meMap.put(instanceId, fe);
	}

	public ConcurrentMap<String, Module> get(String instanceId) {
		return this.meMap.get(instanceId);
	}
	
	/**
	 * 获取form对象
	 * 
	 * @param instanceId
	 * @param formKey
	 * @return
	 */
	public Form getForm(String instanceId, String tableName) {
		ConcurrentMap<String, Module> modules = this.get(instanceId);
		if(null!=modules) {
			for(Module module : modules.values()) {
				List<Form> formList = module.getDataSource().getFormList();
				if(null!=formList && !formList.isEmpty()) {
					for(Form form : formList) {
						if(tableName.equals(form.getTableName())) {
							String entityName = module.getModuleProperties().getKey().toLowerCase()
											+ KEY_SPLIT_SYMBOL + form.getKey();
							form.setEntityName(entityName);
							return form;
						}
					}
				}
			}
		}
		return null;
	}
	
	/**
	 * 获取form对象
	 * 
	 * @param instanceId
	 * @param moduleKey
	 * @param formKey
	 * @return
	 */
	public Form getForm(String instanceId, String moduleKey, String formKey) {
		ConcurrentMap<String, Module> modules = this.get(instanceId);
		if(null!=modules) {
			Module module = modules.get(moduleKey.toLowerCase());
			if(null!=module) {
				List<Form> formList = module.getDataSource().getFormList();
				if(null!=formList && !formList.isEmpty()) {
					for(Form form : formList) {
						if(formKey.equals(form.getKey())) {
							String entityName = module.getModuleProperties().getKey().toLowerCase()
											+ KEY_SPLIT_SYMBOL + form.getKey();
							form.setEntityName(entityName);
							return form;
						}
					}
				}
			}
		}
		return null;
	}

	/**
	 * 获得所有的表名
	 * 
	 * @param instanceId 实例ID
	 *           
	 */
	public Map<String, String> getAllTable(String instanceId) {
		ConcurrentMap<String, Module> modules = this.get(instanceId);
		Map<String, String> tabName = new HashMap<String, String>();
		if(null!=modules) {
			for(Module module : modules.values()) {
				List<Form> formList = module.getDataSource().getFormList();
				if(null!=formList && !formList.isEmpty()) {
					for(Form form : formList) {
						if(form.getIsVirtual()==null || !form.getIsVirtual()) {
							//实体表
							if(StringUtils.isNotBlank(form.getTableName())) {
								tabName.put(form.getTableName(), form.getCaption());
							}
						}
					}
				}
			}
		}
		return tabName;
	}

	/**
	 * 获取所有字段
	 * 
	 * @param instanceId
	 * @param formKey
	 * @return
	 */
	public Map<String, String> getFieldName(String instanceId,
			String formKey) {
		Map<String, String> fieldsName = new HashMap<String, String>();
		Form fm = this.getForm(instanceId, formKey);
		List<Field> fields = fm.getFieldList();
		Iterator<Field> ite = fields.iterator();
		while (ite.hasNext()) {
			Field fl = (Field) ite.next();
			if (StringUtils.isNotBlank(fl.getKey())
					&& (fl.getIsVirtual() == null || !fl
							.getIsVirtual())) {
				fieldsName.put(fl.getDataProperties().getColName(),
						fl.getCaption());
			}

		}
		List<TeAttr> sf = fm.getSystemFields();
		for(TeAttr ta :sf){
				fieldsName.put(ta.getColumnName(),ta.getCaption());
			}
			if (!fm.getIsMaster()) {			
				fieldsName.put("MASTER_ID", "主表编号");
			}

		return fieldsName;
	}
	
	/**
	 * 获取所有模块表
	 * 
	 * @param instanceId
	 * @return
	 */
	public List<Item> getModuleTables(String instanceId) {
		List<Item> itemList = Lists.newArrayList();
		ConcurrentMap<String, Module> modules = this.get(instanceId);
		if(null!=modules) {
			for(Module module : modules.values()) {
				String moduleName = module.getModuleProperties().getCaption();
				String key = module.getModuleProperties().getKey().toLowerCase();
				List<Form> formList = module.getDataSource().getFormList();
				if(null!=formList && !formList.isEmpty()) {
					for(Form form : formList) {
						if(form.getIsMaster()) {
							if(form.getIsVirtual()==null || !form.getIsVirtual()) {
								//实体表
								if(StringUtils.isNotBlank(form.getTableName())) {
									Item item = new Item();
									item.setKey(key + KEY_SPLIT_SYMBOL + form.getKey());
									item.setColumn(form.getTableName());
									item.setCaption(moduleName + NAME_SPLIT_SYMBOL + form.getCaption());						
									itemList.add(item);
								}
							}
						}
					}
					/*for(Form form : formList) {
						if(form.getIsMaster()) {
							if(form.getIsVirtual()==null || !form.getIsVirtual()) {
								//实体表
								if(StringUtils.isNotBlank(form.getTableName())) {
									Item item = new Item();
									item.setKey(key + KEY_SPLIT_SYMBOL + form.getKey());
									item.setColumn(form.getTableName());
									item.setCaption(moduleName + NAME_SPLIT_SYMBOL + form.getCaption());						
									itemList.add(item);
								}
							}
						}
					}*/
				}
			}
		}
		return itemList;
	}
	/**
	 * 获取所有模块公用表
	 * 
	 * @param instanceId
	 * @return
	 */
	public List<Item> getModuleCommonTables(String instanceId) {
		List<Item> itemList = Lists.newArrayList();
		ConcurrentMap<String, Module> modules = this.get(instanceId);
		if(null!=modules) {
			for(Module module : modules.values()) {
				String moduleName = module.getModuleProperties().getCaption();
				String key = module.getModuleProperties().getKey().toLowerCase();
				List<Form> formList = module.getDataSource().getFormList();
				if(null!=formList && !formList.isEmpty()) {
					for(Form form : formList) {
						if(form.getIsVirtual()==null || !form.getIsVirtual()) {
							//实体表
							if(StringUtils.isNotBlank(form.getTableName())&& form.getIsCommon()) {
								Item item = new Item();
								item.setKey(key + KEY_SPLIT_SYMBOL + form.getKey());
								item.setColumn(form.getTableName());
								item.setCaption(moduleName + NAME_SPLIT_SYMBOL + form.getCaption());						
								itemList.add(item);
							}
						}
					}
				}
			}
		}
		return itemList;
	}
	/**
	 * 获取所有模块表单的表
	 * 
	 * @param instanceId
	 * @return
	 */
	public List<Item> getModuleFormsTables(String instanceId) {
		List<Item> itemList = Lists.newArrayList();
		ConcurrentMap<String, Module> modules = this.get(instanceId);
		if (null != modules) {
			for (Module module : modules.values()) {
				if(!"form".equals(module.getModuleProperties().getType())){
					String moduleName = module.getModuleProperties()
							.getCaption();
					String key = module.getModuleProperties()
							.getKey().toLowerCase();
					List<Form> formList = module.getDataSource()
							.getFormList();
					if (null != formList && !formList.isEmpty()) {
						for (Form form : formList) {
							if (StringUtils.isNotBlank(form
									.getTableName())) {
								Item item = new Item();
								item.setKey(key + KEY_SPLIT_SYMBOL + form.getKey());
								item.setColumn(form.getTableName());
								item.setCaption(moduleName
										+ NAME_SPLIT_SYMBOL
										+ form.getCaption());
								itemList.add(item);
							}
						}
					}
				}
			}
		}
		return itemList;
	}
	/**
	 * 根据实体名获取字段信息
	 * 
	 * @param instanceId
	 * @param entityName
	 * @return
	 */
	public List<Item> getTableFields(String instanceId, String entityName) {
		List<Item> itemList = Lists.newArrayList();
		String[] str = entityName.split("\\"+KEY_SPLIT_SYMBOL);
		if(null!=str && str.length > 1) {
			Form form = this.getForm(instanceId, str[0], str[1]);
			if(null!=form) {
				List<Field> fieldList = form.getFieldList();
				if(null!=fieldList && !fieldList.isEmpty()) {
					for(Field fl:fieldList) {
						if (StringUtils.isNotBlank(fl.getKey())
								&& (fl.getIsVirtual() == null || !fl
										.getIsVirtual())) {
							Item item = new Item();
							item.setKey(fl.getKey());
							item.setColumn(fl.getDataProperties().getColName());
							item.setCaption(fl.getCaption());
							itemList.add(item);
						}
					}
				}
			}
			addSystemField(form, itemList);
		}
		return itemList;
	}
	/**
	 * 根据实体名获取公用表字段信息
	 * 
	 * @param instanceId
	 * @param entityName
	 * @return
	 */
	public List<Field> getCommonTableFields(String instanceId, String entityName) {
		List<Field> fieldList = Lists.newArrayList();
		String[] str = entityName.split("\\"+KEY_SPLIT_SYMBOL);
		if(null!=str && str.length > 1) {
			Form form = this.getForm(instanceId, str[0], str[1]);
			if(null!=form) {
				fieldList = form.getFieldList();
			}
		}
		return fieldList;
	}
	/**
	 * 添加系统字段
	 * 
	 * @param form
	 * @param itemList
	 */
	private void addSystemField(Form form, List<Item> itemList) {
		//添加系统字段
		for (int i = 0; i < SYSTEM_KEY.length; i++) {
			Item item = new Item();
			item.setKey(SYSTEM_KEY[i]);
			item.setColumn(SYSTEM_COLUMN[i]);
			item.setCaption(SYSTEM_CAPTION[i]);
			itemList.add(item);
		}
		if(!form.getIsMaster()) {
			//子表字段
			Item item = new Item();
			item.setKey("masterId");
			item.setColumn("MASTER_ID");
			item.setCaption("主表编号");
			itemList.add(item);
		}
		if(Form.LIST_TYPE_TREE_GRID.equals(form.getListType())) {
			//树形列表字段
			Item item = new Item();
			item.setKey("autoCode");
			item.setColumn("AUTO_CODE");
			item.setCaption("自动编码");
			itemList.add(item);
			item = new Item();
			item.setKey("autoParentCode");
			item.setColumn("AUTO_PARENT_CODE");
			item.setCaption("父编码");
			itemList.add(item);
		}
	}

	public void destroy() {
		this.meMap.clear();

	}
}
