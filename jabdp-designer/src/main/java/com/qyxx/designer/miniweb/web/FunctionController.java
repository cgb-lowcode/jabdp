package com.qyxx.designer.miniweb.web;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.qyxx.designer.miniweb.entity.functions.Function;
import com.qyxx.designer.miniweb.service.FunctionManager;
import com.qyxx.designer.modules.mapper.JsonMapper;
import com.qyxx.designer.modules.web.FormatMessage;



@Controller
@RequestMapping(value = "/design")
public class FunctionController {
		@Autowired
		private FunctionManager functionManager;
		private JsonMapper jm = new JsonMapper();
		@RequestMapping(value = "getFunction",produces="text/plain;charset=UTF-8")
		@ResponseBody
		/**
		 * 获得平台函数模块保存行数
		 * @param request 
		 * return rtnMsg*/
		public String getFunction(HttpServletRequest request){
			String path = DesignController.getRealPath(request)+"/js/common/";
			// System.out.println(path);
			 Function function = functionManager.getFunction(path);
			 
			  FormatMessage rtnMsg = new FormatMessage();
			  rtnMsg.setMsg(function);
			return jm.toJson(rtnMsg);
			
		}
}
